from django import template
register = template.Library()

class JsTmp(template.Node):
	def __init__(self, nodelist, key, var):
		self.key = key
		self.var = var
		self.nodelist = nodelist

	def render(self, context):
		out = '<script type="text/javascript">' + self.var + "[" + self.key + "]=\"" + \
		       self.nodelist.render(context).replace('\'', '\\\'')\
				.replace('\n', '').replace('</', '<\/').replace('"', '\\\"') + '\";</script>'

		return out

@register.tag(name="jstmp")
def do_jstmp(parser, token):
	list = token.split_contents()
	key = list[1]
	var = list[2] if len(list) == 3 else "Templates"

	nodelist = parser.parse(('endjstmp',))
	parser.delete_first_token()

	return JsTmp(nodelist, key, var)

