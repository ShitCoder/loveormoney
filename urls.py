from django.conf.urls.defaults import patterns, include, url
import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
      url(r'^qiwi/', include('qiwi.urls')),
     url(r'^auth/', include('auth.urls')),
     url(r'^admin/', include('admin.urls')),
     url(r'^lots/', include('lots.urls')),
     url(r'^purse/', include('qiwi.urls')),
	  url(r'^uloginxd/', 'views.uloginxd'),

	  #url(r'^test/', 'sms_service.test.send'),
	  url(r'^sms/', include('sms_service.urls'))
)

if settings.DEBUG:
	urlpatterns += patterns('', url(r'^test/', include('test.urls')))

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += patterns('',
   url(r'^.*$', 'views.index'),
)