from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from admin import admin as my_admin

urlpatterns = patterns('',
	url(r'admin_tools', include('admin_tools.urls')),
	url(r'', include(admin.site.urls)),
)