# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from qiwi.models import Enrollment, Purse
from django.db.models.aggregates import Sum

from qsstats import QuerySetStats
from admin_tools.dashboard import modules


class BRegistrationSum(modules.DashboardModule):
	"""
	   Dashboard module with user registration charts.

	   With default values it is suited best for 2-column dashboard layouts.
	   """
	title = _('Registration chart')
	template = 'admin/accounting.html'

	def is_empty(self):
		return False

	def __init__(self, *args, **kwargs):
		super(BRegistrationSum, self).__init__(*args, **kwargs)
		self.corp = Enrollment.objects.filter(type=Enrollment.CORPORATE).aggregate(Sum('sum'))['sum__sum'] or 0
		self.char = Enrollment.objects.filter(type=Enrollment.CHARITY).aggregate(Sum('sum'))['sum__sum'] or 0
		self.full = -1#Purse.objects.all().aggregate(Sum('sum'))['sum__sum'] or 0

class RegistrationSum(modules.Group):
	""" Group module with 3 default registration charts """
	title = _('Бухгалтерия')

	def __init__(self, *args, **kwargs):
		kwargs.setdefault('children', [BRegistrationSum()])
		super(RegistrationSum, self).__init__(*args, **kwargs)
