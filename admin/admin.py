from auth.models import LMUser
from social_auth.models import UserSocialAuth
from django.contrib import admin
from django.contrib.auth import admin as auth_admin

class UserFilter(admin.StackedInline):
	pass

class UserAdmin(admin.ModelAdmin):
	list_select_related = True
	fields = ('username', )

admin.site.register(LMUser, UserAdmin)

