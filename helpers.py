from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.utils import simplejson
from django.contrib.auth.models import get_hexdigest
from django.core.serializers.json import Serializer as JSONSerializer, DjangoJSONEncoder
from auth.models import LMUser
from PIL import Image
import settings
from django.utils.encoding import is_protected_type
import datetime
import os
from datetime import datetime
from StringIO import StringIO
from django.conf import settings
from django.utils.importlib import import_module

JSON_RESPONSE_BACKENDS = getattr(settings, 'JSON_RESPONSE_BACKENDS', ())


class JSONEncoder(DjangoJSONEncoder):
	DATE_FORMAT = "%Y/%m/%d"

def datetime_from_milisec(milisec):
	return datetime.datetime.fromtimestamp(milisec // 1000) + datetime.timedelta(microseconds=(milisec % 1000)*1000)

def to_json(obj):
	return simplejson.dumps(obj, cls=JSONEncoder)

def base_jsonResponse(request, json_data, ascii=False):
	d = {}
	for backend in JSON_RESPONSE_BACKENDS:
		d = get_backend(backend)(request, d)

	json_data['___hash'] = d

	return HttpResponse(to_json(json_data), mimetype="application/javascript")

def jsonResponse(request, json_data={}, ascii=False):
	data = json_data
	data.update({'___status': True})
	return base_jsonResponse(request, data, ascii)

def jsonResponseError(request, text="you are hacker", json_data={}):
	data = json_data
	data.update({'___status': False, 'error': text})
	return base_jsonResponse(request, data)


def dict_exclude(dict, *kwargs):
	result = {}
	for attr in kwargs:
		if dict[attr]:
			result.update({attr: dict[attr]})
	return result

def dict_extend(*kwargs):
	d = {}
	for i in kwargs:
		d = dict(d.items() + i.items())
	return d


def user_authenticated(func):
	def wrapper(request, *args, **kwarg):
		if not request.user.is_authenticated():
			if settings.DEBUG:
				print 'ERROR User is not authenticated.'
			return HttpResponseNotFound()
		return func(request, *args, **kwarg)
	return wrapper

def profile_filled(func):
	def wrapper(request, *arg, **kwarg):
		if request.user.is_authenticated() and not request.user.profile_filled:
			return HttpResponseRedirect('/profile/')
		return func(request, *arg, **kwarg)
	return wrapper

def send_post(social_user, text):
	import twitter
	import facebook
	import vkontakte
	import re
	from settings import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, VKONTAKTE_API_SECRET, VKONTAKTE_APP_ID

	try:
		if social_user.provider == 'twitter':
			access_token_secret = re.search('^oauth_token_secret=([^&]+)', social_user.extra_data['access_token']).group(1)
			access_token_key = re.search('oauth_token=([^&]+)', social_user.extra_data['access_token']).group(1)
			twitter.Api(
				consumer_key=TWITTER_CONSUMER_KEY,
				consumer_secret=TWITTER_CONSUMER_SECRET,
			   access_token_key=access_token_key,
				access_token_secret=access_token_secret
			).PostUpdate(text)
		elif social_user.provider == 'facebook':
			facebook.GraphAPI(
				social_user.extra_data['access_token']
			).put_wall_post(text)
		else:
			raise Exception('not send msg for provider: %s' % social_user.provider)
		return True
	except Exception:
		return False

def send_email(users, title, text):
	return True

from types import StringType
def resizeImage(fp, size, dividing=False):
	res = StringIO()
	image = Image.open(fp)
	width, height = image.size
	box = (0, 0, height, width)
	w_k = width/float(size[0])
	h_k = height/float(size[1])

	if w_k > h_k:
		if dividing:
			new_size = (int(width / h_k), int(height / h_k))
			border = (new_size[0] - size[0]) // 2
			b_k = new_size[0] - size[0] - 2 * border
			box = (border, 0, new_size[0] - border - b_k, new_size[1])
		else:
			new_size = (size[0], int(height / w_k))
	else:
		if dividing:
			new_size = (int(width / w_k), int(height / w_k))
			border = (new_size[1] - size[1]) // 2
			b_k = new_size[1] - size[1] - 2 * border
			box = (0, border, new_size[0], new_size[1] - border - b_k)
		else:
			new_size = (int(width/h_k), size[1])

	image = image.resize(new_size, Image.ANTIALIAS)
	if dividing:
		image = image.crop(box)

	image.save(res, 'JPEG')
	return res



import re
from django.db.models.base import Model as DjangoDBModel

class Mapper:
	_REG_FIELD = re.compile('([^\.]+)')
	_REG_CHECK_FIELD = re.compile('^([^\.]+)((\.[^\.]+)+)?$')

	def mapping(self, obj, fields=None):
		if obj == None:
			return None

		self._objects = []
		is_list = bool(getattr(obj, '__iter__', False))
		list = obj if is_list else [obj]

		if is_list and len(list) == 0:
			return []

		for obj in list:
			self._current = {}

			fields = getattr(obj, '_mapping_fields', ()) if not fields else fields
			self._mapping_fields = fields
			mapping_fields_set = set(self.get_mapping_fields(fields))

			for model_field in obj._meta.fields:
				if not model_field.name in mapping_fields_set: #not model_field.serialize or
					continue

				if model_field.rel is None:
					self.handle_field(obj, model_field)
				else:
					self.handle_fk_field(obj, model_field)

			for model_fields in obj._meta.many_to_many:
				pass

			self._objects.append(self._current)

		return self._objects if is_list else self._objects[0]

	def get_mapping_fields(self, fields):
		self._mapping_fields_map = {}
		res = []
		for f in fields:
			if type(f) == type({}):
				key = f.keys()[0]
				self._mapping_fields_map[key] = f[key]
				res.append(key)
			else:
				self._mapping_fields_map[f] = f
				res.append(f)
		return res

	def _get_inner_mapper(self):
		if not getattr(self, '_inner_mapper', False):
			self._inner_mapper = Mapper()
		return self._inner_mapper

	def assign_value(self, field, value):

		def build_value(fieldname, val, cur):
			if type(fieldname) == type([]):
				for f in fieldname:
					build_value(f, getattr(val, f, None), cur)
			elif type(fieldname) == type(()):
				cur = self._get_inner_mapper().mapping(val, fieldname)
			else:
				cur[fieldname] = val

		if type(self._mapping_fields_map[field]) == type(()):
			self._current[field] = self._get_inner_mapper().mapping(value, self._mapping_fields_map[field])
			return

		build_value(self._mapping_fields_map[field], value, self._current)


	def handle_field(self, obj, field):
		value = field._get_val_from_obj(obj)
		if is_protected_type(value):
			self.assign_value(field.name, value)
		else:
			self.assign_value(field.name, field.value_to_string(obj))

	def handle_fk_field(self, obj, field):
		self.assign_value(field.name, getattr(obj, field.name))

def get_backend(path):
	i = path.rfind('.')
	module, attr = path[:i], path[i+1:]
	mod = import_module(module)
	return getattr(mod, attr)


def str_to_list(s):
	if s == '':
		return []
	elif re.match('^\d+$', s) != None:
		return [s]
	else:
		return s.split(',')



from django.core import mail

def send_email_admin(data):
	'''Current function need by send message to user mail'''
	data = dict_extend({
		'author': '',
		'msg': '',
		'theme': '',
		'addr': [],
		'type': ''
	}, data)
	if type(data['addr']) != list:
		data['addr'] = [data['addr']]

	for admin in settings.ADMINS:
		msg = mail.EmailMessage("PANIC", data['msg'], "LOVEORMONEY PANIC", admin)
		msg.content_subtype = data['type'] or 'html'
		msg.send()
