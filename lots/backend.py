from lots.models import Lot

def check_win(request, data):
	if request.user.is_authenticated():
		data['is_win'] = bool(len(Lot.objects.filter(winner=request.user, active=True)))
	return data
