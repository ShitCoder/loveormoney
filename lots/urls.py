from django.conf.urls.defaults import patterns, include, url


urlpatterns = patterns('lots.views',
	url(r'^create/$', 'create'),
	url(r'^set-rate/$', 'add_rate'),
	url(r'^get-rates/$', 'get_rates'),
	url(r'^get-lots/$', 'get_lots'),
	url(r'^search/$', 'get_more_lots'),
	url(r'^update_list/$', 'update_list'),
	url(r'^select-winner/$', 'set_winner'),
	url(r'^get-my-rates/$', 'get_my_rates'),
	url(r'^get-state/$', 'lot_finish'),

	url(r'^message-post/$', 'chat_msg_add'),
	url(r'^messages-get/$', 'chat_msgs_get'),
	url(r'^set-complete/$', 'complete'),
)
