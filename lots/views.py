#encoding=utf-8
#coding=utf-8
from lots.models import Lot, LoveInterest, Interest, RateInterest, ChatMessage, ToMeetTimeLeft
from auth.models import LMUser
from helpers import *
from django.views.decorators.http import require_POST, require_GET
import datetime
import simplejson
from django.db.models import *
import re
from consts import MAX_COUNT_DAY_TO_FINISH_RATE, MIN_COUNT_DAY_TO_FINISH_RATE
from qiwi.models import Enrollment
from settings import DEBUG
from qiwi.views import compute_percent, WITHDRAW_PERCENT


COUNT_LOTS_ON_PAGE = 18

def search(count, ids=[], city='', gender=''):
	users = LMUser.objects.filter(profile_filled=True)
	if bool(city):
		users = users.filter(city=city)
	if bool(gender):
		users = users.filter(gender=gender)

	return Lot.objects.select_related('user').filter(
		id__in=Lot.objects.order_by('date').filter(active=True, user__in=users)[:count].values('id')
	).exclude(id__in=ids)

	#users = LMUser.objects.filter(profile_filled=True)
	#if bool(city):
	#	users = users.filter(city=city)
	#
	#if bool(gender):
	#	users = users.filter(gender=gender)
	#
	#last_date = Lot.objects.get(id=last_id).date if last_id != -1 else datetime.datetime.fromtimestamp(0)
	#
	#return Lot.objects.select_related('user').order_by('date').filter(user__in=users, date__gt=last_date)[:count]

def update(ids=[], city='', gender=''):
	users = LMUser.objects.filter(profile_filled=True)
	if bool(city):
		users = users.filter(city=city)
	if bool(gender):
		users = users.filter(gender=gender)

	return Lot.objects.select_related('user').order_by('date').filter(user__in=users, active=True).exclude(pk__in=ids)

@require_GET
def get_more_lots(request):
	if not ('ids' in request.GET and 'gender' in request.GET and 'city' in request.GET and 'count' in request.GET):
		return jsonResponseError(request, 'you are hacker')

	ids = str_to_list(request.GET['ids']);

	if request.user.is_authenticated():
		request.user.set_save_state({
			'search': {
				'gender': request.GET.get('gender', ''),
				'city': request.GET.get('city', '')}
		})
		request.user.save()

	return jsonResponse(request, {
		'lots': Mapper().mapping(search(int(request.GET['count']), ids, request.GET['city'], request.GET['gender']))
	})

@require_GET
def update_list(request):
	'''
		���������� ������ ����� �����, ����� ������� ���������� � ��������� �������� �������
	'''

	if not 'city' in request.GET and 'gender' in request.GET and 'ids' in request.GET:
		return jsonResponseError(request, 'you are hacker')

	ids = str_to_list(request.GET['ids'])

	result = update(ids, request.GET['city'], request.GET['gender'])

	if request.user.is_authenticated():
		request.user.set_save_state({
			'search': {
				'gender': request.GET.get('gender', ''),
				'city': request.GET.get('city', '')}
		})
		request.user.save()

	return jsonResponse(request, {
		'lots': Mapper().mapping(result)
	})


@user_authenticated
@profile_filled
@require_POST
def create(request):
	try:
		#('percent' in request.POST or int(request.POST['percent']) < 0 or int(request.POST['percent']) > 100)) or\
		if request.user.get_lot() or not('days' in request.POST and 'hours' in request.POST) or\
			int(request.POST['days']) < MIN_COUNT_DAY_TO_FINISH_RATE or int(request.POST['days']) >  MAX_COUNT_DAY_TO_FINISH_RATE:
			raise Exception()
	except:
		jsonResponseError(request, 'you are hacker')

	mins = 0
	if DEBUG and 'mins' in request.POST:
		mins = int(request.POST.get('mins', 0))
	if mins < 0 or mins > 59:
		mins = 0

	date = datetime.datetime.now() + datetime.timedelta(days=int(request.POST['days']), hours=int(request.POST['hours']), minutes=int(mins))
	try:
		Lot.objects.get(user=request.user, active=True)
	except Lot.DoesNotExist:
		lot = Lot.objects.create(user=request.user, date=date, percent=0)#int(request.POST['percent']))
		return jsonResponse(request, {
			'state': True,
			'lot': Mapper().mapping(lot)
		})
	return jsonResponseError(request)

def add_rates(after_love_index, after_rate_index, lot):
	mapper = Mapper()
	if type(lot) == int:
		lot = Lot.objects.get(id=lot)
	return mapper.mapping(list(LoveInterest.objects.filter(id__gt=after_love_index, lot=lot))) +\
			 mapper.mapping(list(RateInterest.objects.filter(id__gt=after_rate_index, lot=lot)))

#@user_authenticated
@require_GET
def get_rates(request):
	last_love_id = request.GET.get('last_love_id')
	last_rate_id = request.GET.get('last_rate_id')
	lid = request.GET.get('lot_id')
	return jsonResponse(request, {
		'upload': add_rates(last_love_id, last_rate_id, int(lid))
	})


@user_authenticated
@profile_filled
@require_POST
def add_rate(request):
	NOT_MONEY = 1
	NEED_UP_RATE = 2
	HAS_LOVE_RATE__NOT_POST_LOVE = 3
	HAS_LOVE_RATE__NOT_POST_MONEY = 4
	HAS_MONEY_RATE__NOT_POST_LOVE = 5
	SUCCESS = 0

	type = request.POST.get('type')
	val = request.POST.get('value')
	lid = request.POST.get('lot_id') #lid = Lot ID
	last_love_id = request.POST.get('last_love_id')
	last_rate_id = request.POST.get('last_rate_id')

	result = dict(upload=[], state=SUCCESS)

	try:
		lot = Lot.objects.get(id=lid)
		if lot.date < datetime.datetime.now() or not lot.active:
			return jsonResponseError(request)

		if len(LoveInterest.objects.filter(lot=lot, user=request.user)) > 0: #если было признание
			if type == 'love':
				result['state'] = HAS_LOVE_RATE__NOT_POST_LOVE
			elif type == 'money':
				result['state'] = HAS_LOVE_RATE__NOT_POST_MONEY
		else:
			if type == 'money' and re.match('^\d+$', val) != None and (int(val) >= 100 if not DEBUG else 1):
				if not DEBUG and int(val) % 100 != 0:
					return jsonResponseError(request)
				max_rate = RateInterest.objects.filter(lot=lot).order_by('-id')
				max_rate = max_rate[0] if len(max_rate) > 0 else None
				if (max_rate == None) or (max_rate != None and int(val) > max_rate.sum):
					my_max_rate = 0
					if request.user.purse.sum - request.user.purse.involved >= int(val) - my_max_rate:
						if max_rate != None: #Возврат бабок
							my_max_rate = RateInterest.objects.filter(lot=lot, user=request.user).aggregate(max_sum=Max('sum'))['max_sum']
							my_max_rate = my_max_rate or 0
							user = request.user if request.user == max_rate.user else max_rate.user
							user.purse.involved -= max_rate.sum
							user.purse.save()

						RateInterest.objects.create(lot=lot, user=request.user, sum=int(val))
						request.user.purse.involved += int(val)# - (my_max_rate if max_rate != None and max_rate.user != request.user else 0)
						request.user.purse.save()
					else:
						result['state'] = NOT_MONEY
				else:
					result['state'] = NEED_UP_RATE


			elif type == 'love':
				if len(RateInterest.objects.filter(lot=lot, user=request.user)) == 0:
					LoveInterest.objects.create(lot=lot, user=request.user, text=val)
				else: #если ставил деньги и пытается отправить признание
					result['state'] = HAS_MONEY_RATE__NOT_POST_LOVE

		result['upload'] = add_rates(last_love_id, last_rate_id, lot)
		return jsonResponse(request, result)
	except Lot.DoesNotExist:
		pass

	return jsonResponseError(request)


#@user_authenticated
@require_GET
def get_lots(request):
	method = request.GET.get('method', 'include').lower()
	count = request.GET.get('count', COUNT_LOTS_ON_PAGE)
	ids = request.GET.get('ids', None)
	if not ids is None:
		ids = ids.split(',')
	id = request.GET.get('id', None)
	if id is None and ids is None:
		return jsonResponseError(request)
	if method == 'include':
		lots = Lot.objects.filter(id__in=ids, active=True) if ids else Lot.objects.filter(id=id, active=True)
	elif method == 'exclude':
		lots = Lot.objects.exclude(id__in=ids, active=True)[:count] if ids else Lot.objects.exclude(id=id, active=True)
	else:
			return jsonResponseError(request)

	return jsonResponse(request, {
		'upload': Mapper().mapping(lots)
	})


@user_authenticated
@require_GET
def lot_finish(request):
	lid = request.GET.get('lot_id', None)
	if lid != None:
		try:
			lot = Lot.objects.get(id=lid)
			rates = 	list(LoveInterest.objects.filter(lot=lot)) + \
						list(RateInterest.objects.filter(lot=lot))
			if len(rates) == 0:
				lot.active = False
				lot.save()
				return jsonResponse(request, {
					'complete_status': 'no-rates'
				})
			else:
				return jsonResponse(request, {
					'complete_status': 'select-winner'
				})
		except Lot.DoesNotExist:
			pass
	return jsonResponseError(request)


@user_authenticated
@require_POST
@profile_filled
def set_winner(request):
	if not 'lot_id' in request.POST or not 'rate_id' in request.POST or not 'type' in request.POST:
		return jsonResponseError(request)

	rate_type = request.POST.get('type')
	if not rate_type in ['love', 'money']:
		return jsonResponseError(request)
	
	rid = request.POST.get('rate_id')
	lid = request.POST.get('lot_id')

	try:
		lot = Lot.objects.get(id=lid)
		if lot.date > datetime.datetime.now():
			return jsonResponseError(request)
		if request.user == lot.user:
			rate = None
			if rate_type == 'money':
				rate = RateInterest.objects.get(id=rid)
			elif rate_type == 'love':
				rate = LoveInterest.objects.get(id=rid)
				max_rate = RateInterest.objects.filter(lot=lot)
				if len(max_rate) > 0:
					max_rate = max_rate[len(max_rate) - 1]
					max_rate.user.purse.involved -= max_rate.sum
					max_rate.user.purse.save()

			try:
				ToMeetTimeLeft.objects.get(lot=lot)
			except ToMeetTimeLeft.DoesNotExist:
				ToMeetTimeLeft.objects.create(lot=lot, date=datetime.datetime.now() + datetime.timedelta(days=7))

			lot.winner = rate.user
			lot.save()

			return jsonResponse(request, {
				'winner': Mapper().mapping(lot.winner)
			})
	except Lot.DoesNotExist:
		pass

	return jsonResponseError(request)

@user_authenticated
@require_GET
@profile_filled
def get_my_rates(request):
	rates = list(LoveInterest.objects.filter(user=request.user)) + list(RateInterest.objects.filter(user=request.user))
	result = []
	for i in rates:
		if not (i.lot.id in result) and i.lot.active == True:
			result += [i.lot.id]
	return jsonResponse(request, {
		'lots': result
	})



#Chat messages
@user_authenticated
@profile_filled
@require_POST
def chat_msg_add(request):
	lid = request.POST.get('lot_id', '')
	text = request.POST.get('text', '')
	last_msg_id = request.POST.get('last_msg_id', '')

	if len(lid) == 0 or len(text) == 0:
		return jsonResponseError(request)

	if len(last_msg_id) == 0:
		last_msg_id = 0
	else:
		last_msg_id = int(last_msg_id)

	try:
		lot = Lot.objects.get(id=int(lid))
		if lot.date > datetime.datetime.now() or not request.user in [lot.user, lot.winner] or not lot.active:
			return jsonResponseError(request)

		chat = None
		try:
			chat = ToMeetTimeLeft.objects.get(lot=lot)
		except ToMeetTimeLeft.DoesNotExist:
			chat = ToMeetTimeLeft.objects.create(lot=lot, date=datetime.datetime.now() + datetime.timedelta(days7))

		if chat.date >= datetime.datetime.now() + datetime.timedelta(days=7):
			raise Lot.DoesNotExist()
			#warning! нужна функция закрытия лоты и понижения рейтинга. а также возврата денег

		ChatMessage.objects.create(lot=lot, author=request.user, text=text, date=datetime.datetime.now())
		msgs = ChatMessage.objects.filter(lot=lot, id__gt=last_msg_id)
		return jsonResponse(request, {
			'upload': Mapper().mapping(msgs),
		   'active': lot.active
		})
	except Lot.DoesNotExist:
		pass

	return jsonResponseError(request)

@user_authenticated
@profile_filled
def chat_msgs_get(request):
	lid = request.GET.get('lot_id', '')
	last_msg_id = request.GET.get('last_message_id', '')

	try:
		lot = Lot.objects.get(id=int(lid))
		if request.user in [lot.user, lot.winner]:
			chat = ToMeetTimeLeft.objects.get(lot=lot)
			msgs = ChatMessage.objects.filter(lot=lot, id__gt=last_msg_id)

			return jsonResponse(request, {
				'upload': Mapper().mapping(msgs),
				'chat': Mapper().mapping(chat),
			   'active': lot.active
			})
	except Lot.DoesNotExist, ToMeetTimeLeft.DoesNotExist:
		pass

	return jsonResponseError(request)


from qiwi.views import compute_percent
@user_authenticated
@profile_filled
@require_POST
def complete(request):
	lid = request.POST.get('lot_id', '')
	if len(lid) > 0:
		try:
			lot = Lot.objects.get(id=int(lid), active=True)
			if lot.user == request.user and lot.winner != None:
				lot.active = False
				lot.save()

				rates = list(LoveInterest.objects.filter(lot=lot, user=lot.winner)) + \
							list(RateInterest.objects.filter(lot=lot, user=lot.winner))
				winner_rate = rates[len(rates) - 1]
				if winner_rate.type == Interest.MONEY:
					winner_rate.user.purse.involved -= winner_rate.sum
					winner_rate.user.purse.save()

				return jsonResponse(request)
			elif lot.winner == request.user:
				lot.active = False
				lot.save()

				rates = list(LoveInterest.objects.filter(lot=lot, user=lot.winner)) + \
							list(RateInterest.objects.filter(lot=lot, user=lot.winner))
				winner_rate = rates[len(rates) - 1]
				if winner_rate.type == Interest.MONEY:
					winner_rate.user.purse.involved -= winner_rate.sum
					winner_rate.user.purse.sum -= winner_rate.sum
					winner_rate.user.purse.save()
					profit = compute_percent(winner_rate.sum, lot.percent)
					profit -= 0 if (profit - int(profit)) == 0 else 1
					lot.user.purse.sum += int(profit)
					lot.user.purse.save()
					chr_sum = winner_rate.sum - profit
					chr_sum -= compute_percent(chr_sum, WITHDRAW_PERCENT)
					Enrollment.objects.create(user=lot.user, sum=chr_sum, type=Enrollment.CHARITY)

				return jsonResponse(request)
		except Lot.DoesNotExist:
			pass

	return jsonResponseError(request)
