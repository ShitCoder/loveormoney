from lots.models import Lot, Interest
from auth.models import LMUser
import random
from consts import CITIES

class Functions:
	@staticmethod
	def create_user(self, first_name=None):
		if not first_name:
			first_name = "user" + str(LMUser.objects.all().count())

		return LMUser.objects.create(
			username= str(LMUser.objects.count()),
			first_name=first_name,
			default_user_login=False,
			profile_filled=True,
		   telephone_number="".join([str(random.randint(0, 9)) for i in range(10)]),
		   city=CITIES[random.randint(0, len(CITIES) - 1)],
		   gender= 'male' if random.random() else 'female'
		)

	@staticmethod
	def create_lot(self, user, date):
		return Lot.objects.create(user=user, date=date)

	@staticmethod
	def create_rate(self, user, lot, type, text):
		Interest.objects.create(lot=lot, user=user, value=text, type=type)



