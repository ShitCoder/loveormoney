from auth.models import LMUser
from django.db import models
from datetime import datetime

class Lot(models.Model):
	user = models.ForeignKey(LMUser, related_name='user', unique=False)
	winner = models.ForeignKey(LMUser, null=True, related_name='winner', unique=False)
	date = models.DateTimeField()
	my_confirm = models.BooleanField(default=False)
	winner_confirm = models.BooleanField(default=False)
	active = models.BooleanField(default=True)
	percent = models.IntegerField(default=100)

	_mapping_fields = ('id', {'user':('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender', 'city')}, {'winner': ('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender')}, 'date', 'my_confirm', 'winner_confirm', 'percent', 'active')

class Interest(models.Model):
	LOVE = 0
	MONEY = 1

	user = models.ForeignKey(LMUser)
	lot = models.ForeignKey(Lot)
	date = models.DateTimeField()

	def save(self, force_insert=False, force_update=False, using=None):
		if not self.pk:
			self.date = datetime.now()
		super(Interest, self).save(force_insert, force_update, using)


class LoveInterest(Interest):
	text = models.CharField(max_length=150)
	type = models.IntegerField(default=0)

	_mapping_fields = ('id', {'user': ('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender', 'city')}, {'lot': ('id', 'date')}, 'text', 'type', 'date')

class RateInterest(Interest):
	sum = models.IntegerField()
	type = models.IntegerField(default=1)

	_mapping_fields = ('id', {'user': ('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender', 'city')}, {'lot': ('id', 'date')}, 'sum', 'type', 'date')



class ToMeetTimeLeft(models.Model):
	lot = models.ForeignKey(Lot)
	date = models.DateTimeField() #close date

	_mapping_fields = ('date', {'lot': ('id', 'date') })

class ChatMessage(models.Model):
	lot = models.ForeignKey(Lot)
	author = models.ForeignKey(LMUser)
	text = models.CharField(max_length=255) #maybe will be must
	date = models.DateTimeField(default=datetime.now())

	_mapping_fields = ('id', {'author': ('id', 'first_name', 'photo_middle', 'photo_mini', 'photo', 'about_me', 'gender', 'city')}, 'text', 'date')

