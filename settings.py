import os
PROJECT_ROOT = os.path.dirname( __file__ )

PRODUCTION = False
DEBUG = True
TEMPLATE_DEBUG = DEBUG
SOAP_DEBUG = True
INVITE = True


def _prod(val, valpr):
	return val if not PRODUCTION else valpr


if not PRODUCTION:
	SITE_URL = 'http://127.0.0.1:8000/'#'http://127.0.0.1:8000/'
else:
	SITE_URL = 'http://www.loveormoney.me/'

ADMINS = (

)

ADMINS_EMAIL = (
	'glasscube@mail.ru',
)

ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
MANAGERS = ADMINS

ADMIN_TOOLS_MEDIA_URL = '/static/'


if not PRODUCTION:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
			'NAME': 'db.db',                      # Or path to database file if using sqlite3.
			'USER': '',                      # Not used with sqlite3.
			'PASSWORD': '',                  # Not used with sqlite3.
			'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
			'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
		}
	}
else:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.postgresql_psycopg2',
			'NAME': 'loveormoney',
			'USER': 'postgres',
			'PASSWORD': 'answerbyloveormoney',
			'HOST': '127.0.0.1',
			'PORT': '5432'
		}
	}

TIME_ZONE = 'America/Chicago'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True



MEDIA_ROOT = _prod(os.path.join(PROJECT_ROOT, 'media'), '/srv/www/media')

TEMP_MEDIA_ROOT = os.path.join(MEDIA_ROOT, 'temp')

MEDIA_URL = '/media/'




STATIC_ROOT = os.path.join(PROJECT_ROOT, _prod('gstatic', 'static'))

STATIC_URL = '/static/'

STATICFILES_DIRS = [
	os.path.join(PROJECT_ROOT, 'static'),
	os.path.join(PROJECT_ROOT, 'media'),
]


STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = '+lyf@nnr8fq#g3al_(-9vs9%o5ob55dyf3e601znrn&5lu2-%l'

TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
	os.path.join(PROJECT_ROOT, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
	'template_context_processor.consts',

	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.core.context_processors.static',
	'django.contrib.auth.context_processors.auth',
	'django.contrib.messages.context_processors.messages',

	'social_auth.context_processors.social_auth_by_name_backends',
	'social_auth.context_processors.social_auth_backends',
	'social_auth.context_processors.social_auth_by_type_backends',

	'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
)

ROOT_URLCONF = 'urls'


INSTALLED_APPS = (
	'admin_tools',
	'admin_tools.theming',
	'admin_tools.menu',
	'admin_tools.dashboard',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.admin',
	'templatetags',
	'admin',
	'auth.social_backends',
	'social_auth',
	'auth',
	'chart_tools',
	'lots',
	'django_qiwi',
	'grab',
	'qiwi',
	'django_ulogin',
	'test',
	'sms_service'
)

AUTHENTICATION_BACKENDS = (
	#'social_auth.backends.facebook.FacebookBackend',
	#'social_auth.backends.twitter.TwitterBackend',
	#'auth.social_backends.mailru.MailRuBackend',
	#'auth.social_backends.vkontakte.VkontakteBackend',
	'auth.backends.DefaultUser',
	'django.contrib.auth.backends.ModelBackend',
)

JSON_RESPONSE_BACKENDS = (
	'qiwi.backends.update_suer_purse',
   'lots.backend.check_win',
)


LOGIN_URL = '/'
SOCIAL_AUTH_ERROR_KEY = 'nfksdbfksjkldfn'
SOCIAL_AUTH_COMPLETE_URL_NAME  = 'socialauth_complete'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_USER_MODEL = 'auth.LMUser'
SOCIAL_AUTH_PIPELINE = (
	'social_auth.backends.pipeline.social.social_auth_user',
	'auth.social_pipeline.social_auth_controller',
)

ULOGIN_FIELDS = []
ULOGIN_OPTIONAL = ['photo_big', 'sex', 'nickname', 'city', 'photo', 'first_name', 'last_name']
ULOGIN_DISPLAY = 'panel'
ULOGIN_HIDDEN = ''
ULOGIN_PROVIDERS = ['vkontakte', 'odnoklassniki', 'mailru', 'facebook', 'twitter', 'google']

TWITTER_CONSUMER_KEY         = _prod('hrGg9DrB80Pz7ZFzQsPTw', 'ydukDGN2xvPT64G2Jdl3oQ')
TWITTER_CONSUMER_SECRET      = _prod('BGiWjHqqu75h7kqaCe9T7S3gJZSSJ5F991UsUchA98', 'JgsBa2xQvZmMbjGF5C0Y4JwnRN8o0SE4WQdvPSWMgM')

FACEBOOK_APP_ID              = _prod('344762675576677', '344762675576677')
FACEBOOK_API_SECRET          = _prod('082f3d233f9552c1ff7bd2e154c593c5', '082f3d233f9552c1ff7bd2e154c593c5')

VKONTAKTE_APP_ID             = _prod('2875522', '2877857')
VKONTAKTE_API_SECRET         = _prod('XXS1eEAfCFt9dExt0UV4', 'lvV0q6bIV6B6pyhbzt4n')

MAILRU_APP_ID                = _prod('670337', '670533')
MAILRU_API_SECRET            = _prod('b69ed57fad11f8abb7ea86a17e190316', '2323b4a2bd3d4486f122d52c00be9624')

SOCIAL_AUTH_BACKEND_ERROR_URL = '/auth/error/'

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'handlers': {
		'mail_admins': {
			'level': 'ERROR',
			'class': 'django.utils.log.AdminEmailHandler'
		}
	},
	'loggers': {
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': True,
			},
		}
}


#qiwi soap
QIWI_APP = 'qiwi'
QIWI_LOGIN = '11764'
QIWI_PASSWORD = 'answerofloveormoneyis42answerofloveormoneyis42'
QIWI_FILES_ROOT = os.path.join(MEDIA_ROOT, 'qiwi')
QIWI_FILES_URL = MEDIA_URL + 'qiwi/'
QIWI_SOAP_SERVER = ('0.0.0.0', 8017)
QIWI_SOAP_ISHOP_URL = '127.0.0.1:8007' if SOAP_DEBUG else 'https://ishop.qiwi.ru/services/ishop'


#sms servise rest
REST_HOST = 'rest.devinotele.com'
REST_PORT = None
REST_LOGIN = 'loveormoney'
REST_PASSWORD = 'loveormoneydevino'
REST_CONTENT_TYPE = 'application/x-www-form-urlencoded'
REST_VALIDITY = 5

REST_AUTHOR = 'LoveOrMoney'

REST_LOGIN_SRC = '/User/SessionId?login=%s&password=%s'
REST_LOGIN_SRC2 = '/User/SessionId'

REST_BALANCE_SRC = '/User/Balance?sessionId=%s'
REST_BALANCE_SRC2 = '/User/Balance'
REST_SEND_SMS_SRC = '/Sms/Send'
REST_STATUS_SRC = '/Sms/State?sessionId=%s&messageId=%s'
REST_STATUS_SRC2 = '/Sms/State'

REST_ADMIN = '79025556603' if PRODUCTION else '79146503025'