import settings
from django.http import HttpRequest
from consts import *
from auth.models import DEFAULT_PHOTO
def consts(context):
	return {
		'STATIC_URL': settings.STATIC_URL,
		'SITE_URL':settings.SITE_URL,
		'AUTH_REDIRECT_URI': HttpRequest().build_absolute_uri(settings.SITE_URL + 'auth/postback/'),
		'DEBUG': settings.DEBUG,
		'INVITE': settings.INVITE,
	   'PHOTO_URL': '/media/photo/',
	   'MIN_COUNT_DAY_TO_FINISH_RATE': MIN_COUNT_DAY_TO_FINISH_RATE,
	   'MAX_COUNT_DAY_TO_FINISH_RATE': MAX_COUNT_DAY_TO_FINISH_RATE,
	   'DEFAULT_USER_PHOTO': settings.MEDIA_URL + DEFAULT_PHOTO
	}
