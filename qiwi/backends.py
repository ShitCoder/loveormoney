from django_qiwi.soap.client import Client
from qiwi.models import Bill, Purse, Enrollment
from qiwi.qiwi_status_code import *
from helpers import Mapper
from qiwi.views import WITHDRAW_PERCENT, compute_percent


def update_bill(txn, status):
	try:
		bill = Bill.objects.get(pk=txn)
		bill.state = status
		bill.save()
	except Bill.DoesNotExist:
		return 1

	if status in [QIWI_STATUS_CANCEL, QIWI_STATUS_TERMINAL_ERROR, QIWI_STUTUS_AUTH_FAIL, QIWI_STATUS_TIMEOUT]:
		bill.delete()

	if status == QIWI_STATUS_PAID:
		purse = Purse.objects.get(user=bill.user)
		bill.delete()
		purse.sum += bill.sum
		purse.save()
		Enrollment.objects.create(user=purse.user, sum=compute_percent(bill.sum, WITHDRAW_PERCENT), type=Enrollment.CORPORATE)

	return 0


def update_suer_purse(request, data):
	if request.user.is_authenticated():
		data['purse'] = Mapper().mapping(request.user.purse)
	return data