from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('qiwi.views',
	url(r'^bring/$', 'bring'),
   url(r'^capthca/$', 'get_captha'),
   url(r'^withdraw/$', 'withdraw'),
)