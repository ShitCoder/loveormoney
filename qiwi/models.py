#coding=utf-8
from django.db import models
from settings import QIWI_FILES_ROOT, QIWI_LOGIN, QIWI_PASSWORD, QIWI_FILES_URL
import os
import simplejson
import re
from grab import Grab
from auth.models import LMUser
from django_qiwi.soap.client import Client
from django_qiwi.soap.utils import STATUS_CODE_TEXT as QIWI_STATUS_CODE
import datetime
import urllib
from qiwi_status_code import QIWI_STATUS_SUCCESS, QIWI_STATUS_EXPOSED, QIWI_STATUS_BILL_EXIST

QIWI_PERCENT = 1
def compute_qiwi_difference(sum):
	return (100 + QIWI_PERCENT) * float(sum) / 100 - sum

class Purse(models.Model):
	user = models.OneToOneField(LMUser, unique=True)
	sum = models.IntegerField(default=0)
	involved = models.IntegerField(default=0)

	_mapping_fields = ('id', 'sum', 'involved')

	def available_money(self):
		return self.sum - self.involved


class Enrollment(models.Model):
	CHARITY = 0
	CORPORATE = 1

	sum = models.FloatField(default=0)
	user = models.ForeignKey(LMUser) #lot
	type = models.IntegerField()
	date = models.DateTimeField()

	def save(self, force_insert=False, force_update=False, using=None):
		if self.pk == None:
			self.date = datetime.datetime.now()

		super(Enrollment, self).save(force_insert, force_update, using)


class Bill(models.Model):
	LIFETIME = datetime.timedelta(weeks=2)

	user = models.ForeignKey(LMUser)
	sum = models.IntegerField(default=0)
	date = models.DateTimeField(default=0)
	state = models.IntegerField(default=-1)

	def save(self, force_insert=False, force_update=False, using=None):
		is_create = self.pk == None
		if not self.pk:
			self.date = datetime.datetime.now()

		super(Bill, self).save(force_insert, force_update, using)

		if is_create:
			qiwi = Client()
			self.state = qiwi.createBill(
					phone=str(self.user.telephone_number),
					amount=self.sum + compute_qiwi_difference(self.sum),
					comment="",
					txn=self.pk,
					lifetime=datetime.datetime.now() + Bill.LIFETIME
			)

			if self.state == QIWI_STATUS_SUCCESS:
				self.state = qiwi.checkBill(self.pk)['status']
				if self.state != QIWI_STATUS_EXPOSED:
					self.state = QIWI_STATUS_BILL_EXIST

			self.save()

	def get_status_text(self):
		for t in QIWI_STATUS_CODE:
			if t[0] == self.state:
				return t[1]
		return None


class QIWIAuth(models.Model):
	cookies = models.CharField(max_length=256, default="{}")
	captha = models.CharField(default="", max_length=256)
	is_auth = models.BooleanField(default=False)
	is_upload = models.BooleanField(default=False)


class QIWI:
	CAPTCHA_FILE_NAME = 'captcha.jpg'

	def __init__(self):
		if QIWIAuth.objects.all().count() == 0:
			self.qiwi_auth = QIWIAuth.objects.create()
		else:
			self.qiwi_auth = QIWIAuth.objects.all()[0]
		self.PATH_CAPTCHA = os.path.join(QIWI_FILES_ROOT, self.CAPTCHA_FILE_NAME)

		self.grab = Grab(cookies=self.prepare_cookie(self.qiwi_auth.cookies))

	def is_auth(self):
		self.grab.go('https://ishop.qiwi.ru/paymentsReport.action')
		self.qiwi_auth.is_auth = (self.grab.response.url.find('https://ishop.qiwi.ru/plogin.action') == -1)
		self.qiwi_auth.save()
		return self.qiwi_auth.is_auth

	def upload_captcha(self):
		self.grab.download('https://ishop.qiwi.ru/ari.action?s=0', self.PATH_CAPTCHA)
		self.qiwi_auth.cookies = simplejson.dumps(self.grab.response.cookies)
		self.qiwi_auth.is_upload = True
		self.qiwi_auth.save()

	def send_answer_captcha(self, capthca_answer):
		self.grab.go('https://ishop.qiwi.ru/login.action?login='+QIWI_LOGIN+'&password='+QIWI_PASSWORD+'&aricode='+capthca_answer)
		auth_cookies = self.qiwi_auth.cookies
		self.qiwi_auth.cookies = simplejson.dumps(self.grab.response.cookies)
		if self.is_auth():
			self.grab.setup(cookies=self.prepare_cookie(self.qiwi_auth.cookies))
			self.qiwi_auth.is_auth = True
			self.qiwi_auth.is_upload = False
			self.qiwi_auth.save()
			return True
		else:
			self.qiwi_auth.cookies = '{}'
			self.qiwi_auth.save()

		return False

	def is_upload_captcha(self):
		return self.qiwi_auth.is_upload

	def get_captcha_url(self):
		return QIWI_FILES_URL + self.CAPTCHA_FILE_NAME

	def transaction(self, phone, sum, comment=""):
		params = {
			'provider': '99',
			'value': str(int(sum)),
			'change': str(int((sum - int(sum)) * 100)),
			'extra[\'account\']': '7' + phone,
			'extra[\'comment\']': comment,
			'mode': 'QIWI'
		}

		try:
			self.grab.go('https://ishop.qiwi.ru/user/transaction.action?' + urllib.urlencode(params))
			response = self.load_json(self.grab.response.body)
			if int(response['error']) > 0:
				raise Exception

			return True
		except:
			return False

	def prepare_cookie(self, cookies):
		return simplejson.loads(cookies)


	def load_json(self, json):
		try:
			return simplejson.loads(json)
		except Exception:
			return simplejson.loads(re.sub('([{,])([^{:\s"]*):', lambda m: '%s"%s":'%(m.group(1),m.group(2)),json))