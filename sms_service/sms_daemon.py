#encoding=utf-8
import threading
from time import sleep
from sms_service.restlib import SMS
from sms_service.models import BalanceState
from datetime import datetime
from settings import REST_ADMIN

TIME_INTERVAL_SECS = 0
TIME_INTERVAL_MINS = 10
TIME_INTERVAL_HOURS = 0

LOW_BALANCE_UNDER = 101.5
SENT__IF_BALANCE_UNDER = [LOW_BALANCE_UNDER, 51.5, 26.5]

def lowBalance(rest=None):
	__rest = rest
	if rest is None:
		__rest = SMS()
		__rest.connect()

	last_state = BalanceState.objects.order_by('-id').filter(sent=True)
	last_state = last_state[0] if len(last_state) > 0 else None

	balance = float(__rest.balance())

	need_send = False
	min_val__index = len(SENT__IF_BALANCE_UNDER)
	while min_val__index > 0:
		min_val__index -= 1
		if balance < SENT__IF_BALANCE_UNDER[min_val__index] and (
			last_state is None or (
				not last_state is None and last_state.balance > SENT__IF_BALANCE_UNDER[min_val__index]
			)
		):
			need_send = True
			break

	#print balance
	BalanceState.objects.create(balance=balance, is_low=(balance <= LOW_BALANCE_UNDER), date=datetime.now(), sent=need_send)
	if need_send:
		__rest.send(REST_ADMIN, u'Низкий баланс ( < %s)' % balance, 0)

	if rest is None:
		__rest.close()


class BalanceWatcher:
	def __init__(self):
		pass

	def run(self):
		#print 'run...'
		#rest = SMS()
		#rest.connect()
		while True:
			print 'watching...'
			lowBalance()
			sleep(float(TIME_INTERVAL_HOURS * 3600 + TIME_INTERVAL_MINS * 60 + TIME_INTERVAL_SECS))
		#rest.close()
	




