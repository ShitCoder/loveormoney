from django.conf.urls.defaults import patterns, include, url


urlpatterns = patterns('sms_service.views',
	url(r'^get-code/$', 'send_confirm_telephone'),
	url(r'^try-code/$', 'confirm_telephone'),
	url(r'^get-data/$', 'getTelephoneConfirmData'),
	url(r'^pre--change-number/', 'can_change_number_')
)
