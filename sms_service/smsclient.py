#encoding=utf-8
from settings import REST_ADMIN
from restlib import *
from models import *
import time
import random

def codegen_hex4():
	return str(hex(int(str(int(time.time()*100))[-6:-1])))[2:6]
def codegen_dec4():
	return str(random.randint(1000, 9999))


###################
EDATA_NUMBER = Exception('Incorrect data :: number must be of type string or integer')
EDATA_TEXT = Exception('Incorrect data :: text of sms must be of type string')

#number is 9xx-xxx-xx-xx, without 7 in beginning
def sms(number, text, livetime=0, rest=None):
	if not type(number) in [int, str, unicode]:
		raise EDATA_NUMBER
	if not type(text) in [str, unicode]:
		raise EDATA_TEXT
	__rest = rest
	if __rest is None:
		__rest = SMS()
		__rest.connect()

	__rest.send(number, text, livetime)

	if rest is None:
		__rest.close()

