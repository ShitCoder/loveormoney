#encoding=utf-8

from settings import *
import httplib
import urllib
import json

eDict = Exception('Incorrect data :: input attribute must be of type dictionary')
eStr = Exception('Incorrect data :: input attribute must be of type string')
eDictOrStr = Exception('Incorrect data :: input attribute must be of type dictionary or string')
eMethod = Exception('Incorrect method :: method must be POST or GET')

stOk = 'Operation complete'
stNull = 'Argument can not be Null or Empty'
stArg = 'Invalid argument'
stSID = 'Invalid session ID'
stAuth = 'Unauthorised access'
stCredits = 'Not enough credits'
stForbidden = 'Forbidden'
stGateway = 'Gateway error'
stIServer = 'Interval server error'
stRequest = 'Bad request'

class Rest(object):
	__headers = {}
	__body = None
	__url = None
	__method = 'GET'
	rest = None

	__not_cnt = None

	def __init__(self, url=__url, headers=__headers, body=__body, method=__method):
		self.url(url)
		self.header(headers)
		self.body(body)
		self.method(method)
		if self.rest is None:
			self.rest = httplib.HTTPConnection(REST_HOST, REST_PORT or None)

	def header(self, data):
		if data is None:
			return self.__headers
		if type(data) != dict:
			raise eDict
		headers = {
			'Content-Type': REST_CONTENT_TYPE or 'application/x-www-form-urlencoded'
		}
		headers.update(data)
		self.__headers = headers
		return self.__headers

	def body(self, data):
		if data is None:
			return self.__body
		if not type(data) in [str, unicode, dict]:
			raise eDictOrStr
		self.__body = urllib.urlencode(data) if type(data) != str else data
		return self.__body

	def url(self, data):
		if data is None:
			return self.__url
		if type(data) != str:
			raise eStr
		self.__url = data
		return self.__url

	def method(self, data):
		if data is None:
			return self.__method
		if type(data) != str or not data in ['GET', 'POST']:
			raise eMethod
		self.__method = data
		return self.__method

	def data(self):
		return {
			'method': self.__method,
			'url': self.__url,
			'headers': self.__headers,
			'body': self.__body
		}

	def connect(self):
		self.rest.connect()

	def close(self):
		self.rest.close()

	def request(self, *args, **kwargs):
		self.__init__(*args, **kwargs)
		self.__response = None
		try:
			self.rest.request(**self.data())
		except Exception, e:
			raise Exception(e, 'Error :: Maybe you not connected')

	def response(self, buff=False):
		self.__response = self.rest.getresponse(buff)
		return self.__response

	def cnt(self, buff=False):
		if self.__response is None:
			self.__response = self.rest.getresponse(buff)
		return self.__response.read() if self.__response.status == httplib.OK else self.__not_cnt

	def status(self, buff=False):
		if self.__response is None:
			self.__response = self.rest.getresponse(buff)
		return self.__response.status



rus = 0
eng = 1
msSendToMobileService = -1
msInQueue = -2
msDeleted = -97
msStopped = -98
msComplete = 0
msSourceAddr = 10
msDestAddr = 11
msErrorDestAddr = 41
msNoEcho = 42
msDead = 46
msCancel = 69
msUndef = 99
msMIDDead = 255

class SMS(Rest):
	__sid = None
	__mid = None

	statuses = {
		msStopped: [u'Остановлено', u'Stopped'],
		msDeleted: [u'Удалено', u'Deleted'],
		msInQueue: [u'В очереди', u'In queue'],
		msSendToMobileService: [u'Передано в мобильную сеть', u'Send to mobile net'],
		msComplete: [u'Доставлено абоненту', u'Send to subscriber'],
		msSourceAddr: [u'Неверно введён адрес отправителя', u'Incorrect source address'],
		msDestAddr: [u'Неверно введён адрес получателя', u'Incorrect destination address'],
		msErrorDestAddr: [u'Недопустимый адрес получателя', u'Illegal destination address'],
		msNoEcho: [u'Отключено смс центром', u'Disconnect of SMS center'],
		msDead: [u'Просрочено (истёк срок жизни сообщения)', u'Lifetime has expired message'],
		msCancel: [u'Отклонено', u'Disconnect'],
		msUndef: [u'Неизвестный', u'Unknown'],
		msMIDDead: [u'Неизвестный (срок получения статуса истёк)', u'Unknown (Deadline for receipt status has expired)']
	}

	def sessionID(self):
		if not self.__sid is None:
			return self.__sid
		self.request(url=REST_LOGIN_SRC2, body={
			'login': REST_LOGIN,
			'password': REST_PASSWORD
		})
		self.__sid = self.cnt()[1:-1]
		return self.__sid

	def balance(self):
		self.sessionID()
		self.request(url=REST_BALANCE_SRC, body={'sessionId': self.__sid})
		self.__balance = self.cnt()
		return self.__balance

	def send(self, number, text, livetime=None):
		self.sessionID()
		self.__mid = None
		text = str(text) if type(text) != str and type(text) != unicode else text
		if type(text) == unicode:
			text = text.encode('utf-8')
		body = {
			'sessionId': self.__sid,
			'sourceAddress': str(REST_AUTHOR),
			'destinationAddress': str(number),
			'data': text,
		}
		if livetime > 0 or (livetime is None and REST_VALIDITY > 0):
			body.update({'validity': livetime  or REST_VALIDITY or 10})
		self.request(REST_SEND_SMS_SRC, {'Expect': '100-continue'}, body , 'POST')
		self.__mid = self.cnt()[2:-2]
		return self.__mid

	def msgstatus(self, mid=None):
		self.sessionID()
		if mid is None:
			if self.__mid is None:
				raise Exception('Error :: No message ID')
			mid = self.__mid
		self.request(url=REST_STATUS_SRC2, body={
			'sessionId': self.__sid,
			'messageId': mid
		})
		self.__response = json.loads(self.cnt())
		return self.__response['State']

	def msgstatus_text(self, lang=rus, mid=None):
		status = self.msgstatus(mid)
		if status in self.statuses:
			return self.statuses[status][lang]
		else:
			return msUndef
