from django.db import models
from datetime import datetime
from auth.models import LMUser

class BalanceState(models.Model):
	balance = models.FloatField()
	is_low = models.BooleanField(default=False)
	date = models.DateTimeField()
	sent = models.BooleanField(default=False)


class NumberConfirmNote(models.Model):
	user = models.OneToOneField(LMUser, unique=True)
	number_confirm = models.BooleanField(default=False)
	number_accept_date = models.DateTimeField(null=True)

class NumberConfirmation(models.Model):
	user = models.ForeignKey(LMUser, unique=False)
	number = models.CharField(max_length=11)
	code = models.CharField(max_length=4) #activation code
	confirm = models.BooleanField(default=False) #confirm state
	#insert tries
	try_index = models.IntegerField(default=0)
	last_try_date = models.DateTimeField(null=True)
	#senders
	send_index = models.IntegerField(default=0)
	last_send_date = models.DateTimeField(null=True)

	def next_try(self):
		self.try_index += 1
		self.last_try_date = datetime.now()
		self.save()

	def next_send(self):
		self.send_index += 1
		self.last_send_date = datetime.now()
		self.save()
