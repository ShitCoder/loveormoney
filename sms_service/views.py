#coding=utf-8
from django.views.decorators.http import require_POST, require_GET
import smsclient
from models import NumberConfirmation, NumberConfirmNote
from helpers import jsonResponse, jsonResponseError
from datetime import timedelta, datetime


#DOMAIN_DIAPASONE = ['914', '924', '950', '908']
FIRST_RESEND = 10
NEXT_RESEND = 60

def number_confirm_note(fun):
	def decorator(request):
		if not hasattr(request.user, 'numberconfirmnote'):
			request.user.numberconfirmnote = NumberConfirmNote.objects.create(user=request.user)
		return fun(request)
	return decorator


@require_GET
def can_change_number_(request, dec_mode=False):
	last_accept_date = request.user.numberconfirmnote.number_accept_date
	if not last_accept_date is None and last_accept_date + timedelta(days=1) > datetime.now():
		next_accept_date = last_accept_date + timedelta(days=1)
		return jsonResponse(request, {
			'change_number__time': next_accept_date.strftime('%Y %m %d %H:%M:%S')
		}) if not dec_mode else False
	return jsonResponse(request) if not dec_mode else True

@require_GET
@number_confirm_note
def send_confirm_telephone(request):
	#if not 'number' in request.GET and request.user.number == '':
	#	return jsonResponseError(request)
	if (not 'number' in request.GET or request.GET['number'] == '') and (not hasattr(request.user, 'number') or request.user.number == ''):
		return jsonResponse(request, {
			'data_error': True
		})
	number =  request.GET['number'] if request.GET['number'] != '' else request.user.number
	if len(number) < 10 or number == request.user.telephone_number:
		jsonResponseError(request)
	number = '7'+number

	if not can_change_number_(request, True):
		return jsonResponseError(request)

	try:
		data = NumberConfirmation.objects.get(user=request.user, number=number, confirm=False)
	except NumberConfirmation.DoesNotExist:
		data = NumberConfirmation.objects.create(
			user = request.user,
			number = number,
			code = smsclient.codegen_dec4()
		)

	if request.user.telephone_number != number[1:]:
		request.user.numberconfirmnote.number_confirm = False
		request.user.numberconfirmnote.save()

	mins = FIRST_RESEND if data.send_index == 1 else NEXT_RESEND
	if (
			data.send_index > 0 and
			data.last_send_date + timedelta(minutes=mins) < datetime.now()
		) or (
			data.send_index == 0
		):
		#String of sms sending (see down)
		#smsclient.sms(data.number, u'Код подтверждения: %s' % data.code)
		data.next_send()

	next_send = data.last_send_date + timedelta(minutes=FIRST_RESEND if data.send_index == 1 else NEXT_RESEND)
	return jsonResponse(request, {
		'next_send': next_send.strftime('%Y %m %d %H:%M:%S')
	})

@require_POST
def confirm_telephone(request):
	if not 'code' in request.POST:
		return jsonResponseError(request)

	try:
		data = NumberConfirmation.objects.get(user=request.user, confirm=False)
		date = None
		if data.try_index % 3 != 0 or \
			data.last_try_date is None or \
			data.last_try_date + timedelta(minutes=60) < datetime.now():
			if data.code == request.POST['code']:
				data.confirm = True
				data.save()
				if request.user.telephone_number != data.number:
					request.user.number_is_confirmed(data.number)
				return jsonResponse(request, {
					'state': 'complete',
					'number': data.number[1:]
				})
			else:
				if data.try_index == 0 or  data.try_index % 3 != 0:
					data.next_try()
				date = 0 if data.try_index % 3 != 0 else None
		else:
			pass

		if not data.last_try_date is None and date is None:
			date = data.last_try_date + timedelta(minutes=60)

		return jsonResponse(request, {
			'next_try': 0 if date == 0 else date.strftime('%Y %m %d %H:%M:%S')
		})
	except NumberConfirmation.DoesNotExist:
		pass
	return jsonResponseError(request)

@require_GET
def getTelephoneConfirmData(request):
	try:
		fdata = NumberConfirmation.objects.filter(user=request.user, confirm=False).order_by('-id')
		response = {
			'__confirm_state__': request.user.numberconfirmnote.number_confirm
		}
		if len(fdata) > 0:
			data = fdata[0]
			response['number'] = data.number
		
		return jsonResponse(request, response)
	except Exception:
		return jsonResponse(request, {
			'__confirm_state__': False
		})