import os.path
import sys

extensions = ['.py', '.js', '.css', '.html']

dirpath = '.'
if len(sys.argv) == 2:
	dirpath = sys.argv[1]

if not os.path.exists(dirpath):
	print "Directory '%s' not found." % dirpath
	exit(1)

def scandir(dirpath):
	files = []
	def visit(arg, dirname, names) :
		for filename in names:
			ext = os.path.splitext(filename)[1].lower()
			if ext in extensions:
				files.append(dirname + os.sep + filename)
	os.path.walk(dirpath, visit, {})
	return files


total = 0
files = scandir(dirpath)
for path in files:
	with open(path) as f:
		cnt = len(f.readlines())
		total += cnt
		print "%s: %s" % (path, cnt)


print "total files: %s" % len(files)
print "total lines: %s" % total