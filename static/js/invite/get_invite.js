var CGetInvite = Backbone.View.extend({
	initialize: function() {
		this.constructor.__super__.initialize.apply(this, arguments);
		this.login = new CAuthorisationPanel();
		this.template('get_invite', this);

		//Combo box
		var self = this;
		var onSelect = function() {
			self.change_field(this.$val);
		};
		this.gender = new combo({
			holder: 'Пол',
			render: false,
			list: ['Мужской', 'Женский'],
			classes: {
				'parent': ['combo'],
				'input': ['editor']
			},
			ids: {
				'input': ['gender']
			},
			lines: 2,
			input: false,
			free: true
		});
		this.city = new combo({
			holder: 'Город',
			render: false,
			list: InitializeData.CITIES,
			classes: {
				'parent': ['combo'],
				'input': ['editor']
			},
			ids: {
				'input': 'city'
			},
			free: false,
			search: true,
			scroll: true,
			lines: 10,
			trim: true,
			show_all: true
		});
		this.gender.onSelect = onSelect;
		this.city.onSelect = onSelect;

		if (App.user.gender != '')
			this.gender.select(['male', 'female'].indexOf(App.user.gender));
		if (App.user.city != '')
			this.city.select(App.user.city);
		this.gender.$list.bind('click', function() {
			self.check_field(self.gender.$val, self.check_viewer);
		});
		this.city.$list.bind('click', function() {
			self.check_field(self.city.$val, self.check_viewer);
		});
		this.$('#gender-note > label').after(this.gender.render());
		this.$('#city-note > label').after(this.city.render());
		this.$('#save_profile').addClass('disabled');

		this.change_photo_button();

		this.confirm = new CNumberConfirmUpgrade(this.$('#telephone_number'));
		this.confirm.render();

		$('#page_content').append(this.$el);
		$('#body').addClass('get-invite-body');
	},

	events: {
		'keyup #telephone_number': 'change_field',
		'keyup   #first_name': 'duplicate_value',
		'keyup #first_name': 'change_field',
		'change #first_name': 'change_field',
		'change #city': 'change_field',
		'keyup #gender': 'change_field',
		'change #gender': 'change_field',
		'mousedown #gender': 'change_field',
		'click #update_photo': 'change_photo',
		'change #profile_main_note #UserPhotoToUpload': 'update_photo',
		'click #save_profile' : 'save_change'
	},

	change_photo_button: function() {
		var photo = App.user.photo;
		photo = photo.substring(photo.lastIndexOf('/') + 1, photo.lastIndexOf('.')).toLowerCase();
		if (photo == 'default') {
			this.$('#update_photo')
				.removeClass('no-default-photo')
				.addClass('default-photo')
				.find('span')
					.text('Загрузить фотографию');
		} else {
			this.$('#update_photo')
				.removeClass('default-photo')
				.addClass('no-default-photo')
				.find('span')
					.text('Заменить фотографию');
		}
	},

	change_photo: function() {
		this.$('input[type=file]').click();
		this.$('#profile_main_note #UserPhotoToUpload').blur();
	},

	update_photo: function(e) {
		var MAX_SIZE_IMG = 15;
		var file = e.target.files[0];
		var self = this;
		var $input = $(e.target);

		if (file.size >= 1024*1024*MAX_SIZE_IMG || !/\.(png|jpg|jpeg|bmp|ico|gif)$/.test(file.name)) {
			var el = this.$('#update_photo').css('border', 'solid 1px red');

			setTimeout(function(){el.css('border', 'none')}, 5000);
			return;
		}

		$.ajaxFileUpload({
			url: '/auth/update/photo/',
			secureuri: false,
			fileElementId: 'UserPhotoToUpload',
			dataType: 'json',
			type: 'POST',
			data: { csrfmiddlewaretoken: CSRF_TOKEN },
			success: function (data, status){
				if (data.status) {
					self.$('#user_photo').attr('src', '/media/photo/original/default.jpg');
					self.$('#user_photo').attr('src', data.photo_uri);
					App.user.photo = data.photo_uri;
					App.user.photo_mini = data.photo_mini_uri;
					App.user.photo_middle = data.photo_middle_uri;
					self.change_photo_button();
					self.handler_profile_filled(data);
				} else {
					App.msg.show('Файл слишком большой или его формат не верен');
				}
			}
		})
	},

	duplicate_value: function(e) {
		var $f = $(e.target);
		var selector = '#' + ($f.attr('id') == 'about_me' ? 'about_me-duplicate': 'first_name-duplicate');
		this.$(selector).text($f.val());
	},

	check_viewer: function(state, $field) {
		var method = state? "removeClass": "addClass";
		if ($field.attr('id') == 'user_confirmation') {
			$field = $field.next();
		}

		$field[method]('bad_fill_field');
	},

	change_field: function(e) {
		var $field = $(e.target);
		var all_fill_filled = true;
		var fields = ['city', 'gender', 'telephone_number', 'first_name'];
		for (var i = 0; i < fields.length; i++) {
			var val = $('#' + fields[i]).val();
			if (val.length == 0 || (fields[i] == 'telephone_number' && val.length != 10)) {
				all_fill_filled = false;
				break;
			}
		}

		if(all_fill_filled) {
			this.$('#save_profile').removeClass('disabled');
		}

		if (['first_name', 'telephone_number', 'gender', 'city', 'user_confirmation'].indexOf($field.attr('id')) != -1) {
			this.check_field($field, this.check_viewer);
		}
	},

	check_username: function(username, success) {
		success(username.length != 0 && username.length <= 32)
	},

	check_telephone_number: function(telephone_number, success) {
		if (/^\d{10}$/.test(telephone_number)) {
			var self = this;
			jsonRequest({
				data: {telephone_number: telephone_number},
				type: 'GET',
				url: '/auth/check_telephone_number/',
				success: function(data) {
					if (success) {
						success(!data.is_exist);
						if(data.is_exist) {
							App.msg.show('Данный номер уже используется.', 4);
						}
						if (telephone_number != App.user.telephone_number)
							self.confirm.render();
					}
				}
			});
		} else {
			success(false);
		}
	},

	check_field: function($field, success) {
		var state = false;

		switch($field.attr('id')) {
			case 'first_name':
				this.check_username($field.val(), function(state) {success(state, $field);});
				return;
			case 'telephone_number':
				this.check_telephone_number($field.val(), function(state){success(state, $field)});
				return;
			case 'gender':
				state = this.gender.valid();
				break;
			case 'city':
				state = this.city.valid();
				break;
		}

		success(state, $field);
	},

	save_change: function($field, success){
		var self = this;
		var state = true;
		var field_ids = ['first_name', 'gender', 'city', 'telephone_number'];

		if (self.$('#save_profile').hasClass('disabled')) {
			if (success) {
				success();
			}
			return;
		}

		(function(ids, itr) {
			var ff = arguments.callee;

			self.check_field(self.$('#' + ids[itr]), function(st, $field){
				if (!st) {
					self.check_viewer(st, $field);
					state = st;
				}

				if (itr < ids.length - 1) {
					ff(ids, itr + 1, state);
					return;
				}

				if (state) {
					var data = {};
					var fields = [];
					for (var i = 0; i < ids.length; i++) {
						data[ids[i]] = self.$('#' + ids[i]).val();
						fields[i] = ids[i];
					}
					data.city = self.city.val();
					data.gender = ['male', 'female'][self.gender.index()];

					self.$('#save_profile').attr('disabled', 'disabled');

					jsonRequest({
						type: 'POST',
						url:  '/auth/update/profile/',
						data: data,
						success: function(__data) {
							if (success)
								success();

							self.$('#save_profile').removeAttr('disabled').addClass('disabled');
							if (App.user.profile_filled && App.user.number_confirm) {
								App.page.remove();
								App.page = new CCompletePage();
								$('#body').removeClass('get-invite-body');
							}
						},
						error: function() {
							self.$('#save_profile').removeAttr('disabled').addClass('disabled');
						}
					})
				}
			})
		})(field_ids, 0);
	}
});