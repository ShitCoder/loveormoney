var CInvitePage = Backbone.View.extend({
	events: {
		'click #login': 'open_login',
		'click global #shadow': 'close_login',
		'click global #authorisation_panel_close': 'close_login'
	},

	initialize: function() {
		this.login = new CAuthorisationPanel();
		$('#page_content').append(this.render());
		this.login.$el.centered();
	},

	open_login: function() {
		$('body').append('<div id="shadow"></div>');
		this.login.$el.show();
		if (uLogin) {try{uLogin.init();}catch(e){}}
	},

	close_login: function() {
		$('#shadow').remove();
		this.login.$el.hide();
	},

	render: function() {
		return this.template('invite', this);
	}
});
