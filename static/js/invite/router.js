
var CInviteRouter = Backbone.Router.extend({
	routes: {
		'invite/': 'open_pp',
		'*path': 'index'
	},

	index: function() {
		if (App.router.uri != '/') {
			App.router.redirect('/');
			return;
		}
		if (App.page)
			App.page.remove();

		if ([undefined, null].indexOf(App.page) != -1)
			if (App.user.is_authenticated() && (!App.user.profile_filled || !App.user.number_confirm)) {
				App.page = new CGetInvite();
			} else {
				App.page = new CInvitePage();
			}
	},

	open_pp: function() {
		this.index();
		if (App.page instanceof CInvitePage)
			App.page.open_login();
	}
});
