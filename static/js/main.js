var CSRF_TOKEN;
var Templates = null;
var SETTINGS = {
	DEBUG: false,
	LOT_RATES_AUTOUPDATE_TIMEOUT: 15000,
	LOTS_IN_THE_PAGE: 18
};

var App = {
	user: null,
	page: null,
	socket: null
};
var Router = null;
var InitializeData = {};


var sync_server_time = function(itr){
	if (itr == 3)
		return;

	sync_server_time._stime = new Date().getTime();
	
	$.ajax({
		url: '/syc_time/',
		type: 'GET',
		dataType: 'json',
		success: function(data) {
			var overtime = new Date().getTime() - sync_server_time._stime;
			DEFFERENCE_TIME -= overtime;
		},
		error: function() {
			sync_server_time(itr + 1);
		}
	})
};

function init(current_user, social_auth) {
	_.templateSettings = {
		evaluate    : /<%([\s\S]+?)%>/g,
		interpolate : /<<([\s\S]+?)>>/g,
		escape      : /<#([\s\S]+?)#>/g
	};

	RESPONSE_BACKENDS = ['update_purse_sum', 'update_user_win'];

	CSRF_TOKEN = document.cookie.match('csrftoken=([^;]+)');
	CSRF_TOKEN = CSRF_TOKEN? CSRF_TOKEN[1]: "";
	if (!CSRF_TOKEN.length)
		CSRF_TOKEN = $('#csrf_token > input').val();

	if (!SETTINGS.INVITE) {
		if (current_user instanceof CUser)
			current_user.state.set('user', current_user);


		App.user = current_user;
		App.router = new CRouter();
		App.msg = new CMsg();
		App.lots = new CLots(InitializeData.lots);
		App.users = new CUserCollection(App.lots.get('user'));
		Backbone.history.start({pushState: true});

		$('#navi_panel button').click(function() {
			$('#navi_panel img').show();
		});
	} else {
		App.user = current_user;
		App.router = new CInviteRouter();
		App.msg = new CMsg();
		Backbone.history.start({pushState: true});
		//App.page = new CInvitePage();
	}
}

function update_user_win(data) {
	if(data.is_win === undefined) {
		return;
	}
	App.user.set('is_win', data.is_win);
}

function update_purse_sum(data) {
	if (!App.user.is_authenticated()) {
		return;
	}

	if (data.purse && data.purse.incomes) {
		var sum = 0;
		for (var i = 0; i < data.purse.incomes.length; i++) {
			sum += data.purse.incomes[i];
		}
		App.msg.show("На ваш счет поступило " + sum + " chr")
	}

	with (App.user.purse)
		if (!data.purse || (sum == data.purse.sum && involved == data.purse.involved)) {
			return;
		}

	App.user.purse.sum = data.purse.sum;
	App.user.purse.involved = data.purse.involved;
	App.user.purse.on_change_sum();
}