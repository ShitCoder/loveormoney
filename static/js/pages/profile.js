/* эта функция вызывается при получения токена от ulogin */
function __ulogin_add_social_account__(token, provider) {
	App.page.add_social_account.apply(App.page, arguments);
}


var CProfile = CBaseUserPage.extend({
	views: new Array(),
	initialize: function() {
		this.constructor.__super__.initialize.apply(this, arguments);
		this.template('profile', this);

		//Combo box
		var self = this;
		var onSelect = function() {
			self.change_field(this.$val);
		};
		this.gender = new combo({
			holder: 'Пол',
			render: false,
			list: ['Мужской', 'Женский'],
			classes: {
				'parent': ['combo'],
				'input': ['editor']
			},
			ids: {
				'input': ['gender']
			},
			lines: 2,
			input: false,
			free: true
		});
		this.city = new combo({
			holder: 'Город',
			render: false,
			list: InitializeData.CITIES,
			classes: {
				'parent': ['combo'],
				'input': ['editor']
			},
			ids: {
				'input': 'city'
			},
			free: false,
			search: true,
			scroll: true,
			lines: 10,
			trim: true,
			show_all: true
		});
		this.gender.onSelect = onSelect;
		this.city.onSelect = onSelect;
		if (App.user.gender != '')
			this.gender.select(['male', 'female'].indexOf(App.user.gender));
		if (App.user.city != '')
			this.city.select(App.user.city);
		this.gender.$list.bind('click', function() {
			self.check_field(self.gender.$val, self.check_viewer);
		});
		this.city.$list.bind('click', function() {
			self.check_field(self.city.$val, self.check_viewer);
		});
		this.$('#gender-note > label').after(this.gender.render());
		this.$('#city-note > label').after(this.city.render());
		//end
		this.$('#save_profile').addClass('disabled');

		if (App.user.lot && App.user.lot.rates) {
			var tl = timeleft(App.user.lot);
			if (tl !== undefined) {
				this.$('#time-left > span').text(tl);
				this.$('#put_lot_button').addClass('disabled');
				this.date_timer(jQuery.extend({over: true}, App.user.lot, {
					parent: '#time-left > span',
					end_function: this.end_function
				}));
			}

			App.user.lot.rates.display = this;
			App.user.lot.rates.show();
			function auto_update(interval) {
				var t = setTimeout(function() {
					if (App.router.uri == '/profile/') {
						clearTimeout(t);
						if (App.user.lot) {
							App.user.lot.rates.update();
							auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
						}
					}
				}, interval);
			}
			auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
			App.user.lot.rates.update(false);

			this.$('#rates > #title').show();

			if (!tl)
				this.end_function();

			var t = setTimeout(function() {
				clearTimeout(t);
				$('#hide_profile, #profile_form, #show_profile').toggle();
				App.router.page_build();
			}, 300);
		} else {
			this.$('#time-left').text('Вы себя ещё не поставили на торга');
			this.$('#rates > #title').hide();
		}
		$('#page_content').append(this.$el);
		uLogin.init();

		if (this.chat !== undefined)
			$('#page_content').append(this.chat.$el);
		this.$('#auctionmeter').slider({
			create: function() {
				$(this).slider("option", "change").call(this);
			},
			value: App.user.lot ? App.user.lot.percent : 60,
			disabled: App.user.lot && 1,
			step: 5,
			change: function() {
				var $obj = $(this);
				var $parent = $obj.parents('#auctonmeter-note');
				var value = $obj.slider("value");
				$parent.find('#to_self').text(value);
				$parent.find("#to_children").text(100 - value);
			}
		});
	},

	events: {
		'keyup #telephone_number': 'change_field',
		'keyup   #first_name': 'duplicate_value',
		'keyup #first_name': 'change_field',
		'change #first_name': 'change_field',
		'keyup #about_me': 'change_field',
		'keyup  #about_me': 'duplicate_value',
		'change #about_me': 'change_field',
		'change #city': 'change_field',
		'keyup #gender': 'change_field',
		'change #gender': 'change_field',
		'mousedown #gender': 'change_field',
		'change #user_confirmation': 'change_field',
		'click #update_photo': 'change_photo',
		'change #profile_main_note #UserPhotoToUpload': 'update_photo',

		'click #add_social_account': 'open_add_social_account',


		'click #put_lot_button': 'put_lot',
		'click #save_profile' : 'save_change',

		'click #hide_profile, #show_profile': 'tumbler_show_profile',

		'click .rate_note': 'select_winner'
	},

	date_timer: date_timer,


	end_function: function() {
		if (App.user.lot !== undefined && App.user.lot.winner instanceof CUser) {
			this.$('#rates').children().not('#title').remove();
			this.$('#rates>#title>div').html('');
			this.$('#time-left').html('Победитель "'+App.user.lot.winner.first_name+'"');
			this.__open_chat();
		} else {
			var self = this;
			function select_winner() {
				self.$('#time-left').html('Выберете победителя');
				self.$('#rates').children().not('#title').remove();
				App.user.lot.rates.show(App.user.lot.rates.query('type="money"::max(value as int)'));
				App.user.lot.rates.show(App.user.lot.rates.query('type="love":last-first'));
				self.$('.rate_note').addClass('select-rate-mode');
			}

			if (App.user.lot.rates.length == 0)
				App.user.lot.rates.update(false);
			if (App.user.lot.rates.length == 0) {
				var self = this;
				jsonRequest({
					url: '/lots/get-state/',
					type: 'GET',
					data: {
						lot_id: App.user.lot.id
					},
					success: function(data) {
						switch (data.complete_status.toLowerCase()) {
							case 'select-winner':
								select_winner();
								break;
							case 'no-rates':
								var lot = App.lots.query('id='+App.user.lot.id)[0];
								if (lot !== undefined)
									App.lots.remove(lot);
								if (App.user.lot !== undefined)
									App.user.lot = undefined;
								self.reinitialize();
								break;
						}
					}
				});
			} else
				select_winner();
		}
	},

	select_winner: function(e) {
		if (e.id === 'user-profile-link' || $(e.target).hasClass('user_photo')) {
			if (e.id === 'user-profile-link' || $(e.target).hasClass('user_photo'))
				return;
		}
		if (App.user.lot.date.getTime() + DEFFERENCE_TIME <= new Date().getTime()) {
			/*if ($('body').find('#confirm-select-content').length > 0)
				return;*/
			var self = this;
			$('.selected_rate').removeClass('selected_rate');
			var $e = $(e.target);
			if ($e.attr('class').indexOf('rate_note') == -1)
				$e = $e.parents().filter('.rate_note');
			$e.addClass('selected_rate');
			//далее будет вызов окна подтверждения и соответствующие выбору действия
			var __yes = function() {
				var rate = App.user.lot.rates.query('id='+$e[0].id+':first')[0];
				$confirm.remove();
				jsonRequest({
					url: '/lots/select-winner/',
					data: {
						lot_id: App.user.lot.id,
						rate_id: rate.id,
						type: rate.type
					},
					type: 'POST',
					success: function(data) {
						if (data.___status) {
							App.user.lot.winner = new CUser(data.winner);
							self.$('#time-left').html('Победитель пользователь "'+App.user.lot.winner.first_name+'"');
							self.__open_chat(true);
						}
					}
				})
			};
			var __not = function() {
				$confirm.remove();
				$e.removeClass('selected_rate');
			};
			var $confirm = this.template('confirm-select', this, true);
			$confirm.find('#title').text('Вы уверены в своём выборе?');
			$confirm.find('#confirm').bind('click', function() {__yes()});
			$confirm.find('#unconfirm').bind('click', function() {__not()});
			$('body').append($confirm);
			$confirm.centered();
		}
	},

	__open_chat: function(render) {
		//функция открытия чата
		this.$('#rates').remove();
		this.chat = new CChat(this, App.user.lot);
		if (render)
			$('#page_content').append(this.chat.$el);
		this.$('#rates > #title > div').text(App.user.lot.winner.first_name);
	},

	update_photo: function(e) {
		var MAX_SIZE_IMG = 5;
		var file = e.target.files[0];
		var self = this;
		var $input = $(e.target);

		if (file.size >= 1024*1024*MAX_SIZE_IMG || !/\.(png|jpg|jpeg|bmp|ico|gif)$/.test(file.name)) {
			var el = this.$('#update_photo').css('border', 'solid 1px red');

			setTimeout(function(){el.css('border', 'none')}, 5000);
			return;
		}

		$.ajaxFileUpload({
			url: '/auth/update/photo/',
			secureuri: false,
			fileElementId: 'UserPhotoToUpload',
			dataType: 'json',
			type: 'POST',
			data: { csrfmiddlewaretoken: CSRF_TOKEN },
			success: function (data, status){
				if (data.status) {
					/* Activate this code, if browser not update static
						var $pic = self.$('#user_photo');
						var $parent = $pic.parent();
						$pic.remove();
						$parent.append($pic);
					 */
					self.$('#user_photo').attr('src', '/media/photo/original/default.jpg');
					self.$('#user_photo').attr('src', data.photo_uri);
					App.user.photo = data.photo_uri;
					App.user.photo_mini = data.photo_mini_uri;
					App.user.photo_middle = data.photo_middle_uri;
					self.handler_profile_filled(data);
				} else {
					App.msg.show('Файл слишком большой или его формат не верен');
				}
			}
		})
	},

	duplicate_value: function(e) {
		var $f = $(e.target);
		var selector = '#' + ($f.attr('id') == 'about_me' ? 'about_me-duplicate': 'first_name-duplicate');
		this.$(selector).text($f.val());
	},

	handler_profile_filled: function(data) {
		if(!data.profile_filled) {
			return;
		}

		if(!App.user.profile_filled && data.profile_filled) {
			App.user.profile_filled = true;

			this.$('#put_lot_button').removeClass('disabled');
		}
	},

	check_viewer: function(state, $field) {
		var method = state? "removeClass": "addClass";
		if ($field.attr('id') == 'user_confirmation') {
			$field = $field.next();
		}

		$field[method]('bad_fill_field');
	},

	change_field: function(e) {
		var $field = $(e.target);

		this.$('#save_profile').removeClass('disabled');

		if (['first_name', 'telephone_number', 'gender', 'city', 'user_confirmation'].indexOf($field.attr('id')) != -1) {
			this.check_field($field, this.check_viewer);
		}
	},

	check_username: function(username, success) {
		/*if (username.length == 0 || username.length > 32) {
			success(false);
			return;
		}

		jsonRequest({
			data: {first_name: username},
			type: 'GET',
			url: '/auth/check_username/',
			success: function (data) {
				success(!data.is_exist);
			}
		})*/
		success(username.length != 0 && username.length <= 32)
	},

	check_telephone_number: function(telephone_number, success) {
		if (/^\d{10}$/.test(telephone_number)) {
			jsonRequest({
				data: {telephone_number: telephone_number},
				type: 'GET',
				url: '/auth/check_telephone_number/',
				success: function(data) {
					if (success) {
						success(!data.is_exist);
						if(data.is_exist) {
							App.msg.show('Данный номер уже используется.', 4);
						}
					}
				}
			});
		} else {
			success(false);
		}
	},

	check_field: function($field, success) {
		var state = false;

		switch($field.attr('id')) {
			case 'first_name':
				this.check_username($field.val(), function(state) {success(state, $field);});
				return;
			case 'telephone_number':
				this.check_telephone_number($field.val(), function(state){success(state, $field)});
				return;
			case 'about_me':
				state = $field.val().length <= 200;
				break;
			case 'gender':
				state = this.gender.valid();
				break;
			case 'city':
				state = this.city.valid();
				//state = $field.val().length != 0;
				break;
			case 'user_confirmation':
				state = $field[0].checked;
				break;
		}

		success(state, $field);
	},

	save_change: function($field, success){
		var self = this;
		var state = true;
		var field_ids = ['about_me', 'first_name', 'gender', 'city', 'user_confirmation'];
		if (!App.user.telephone_number.length) {
			field_ids.push('telephone_number');
		}

		if (self.$('#save_profile').hasClass('disabled')) {
			if (success) {
				success();
			}
			return;
		}

		(function(ids, itr) {
			var ff = arguments.callee;

			self.check_field(self.$('#' + ids[itr]), function(st, $field){
				if (!st) {
					self.check_viewer(st, $field);
					state = st;
				}

				if (itr < ids.length - 1) {
					ff(ids, itr + 1, state);
					return;
				}

				if (state) {
					var data = {};
					var fields = [];
					for (var i = 0; i < ids.length; i++) {
						data[ids[i]] = self.$('#' + ids[i]).val();
						fields[i] = ids[i];
					}
					data.city = self.city.val();
					data.gender = ['male', 'female'][self.gender.index()];

					self.$('#save_profile').attr('disabled', 'disabled');

					jsonRequest({
					type: 'POST',
					url:  '/auth/update/profile/',
					data: data,
		            success: function(__data) {
			            for (var i = 0; i < fields.length; i++) {
									App.user[fields[i]] = data[fields[i]];
			            }
			            self.handler_profile_filled(__data);
			            if (success)
								success();

							App.lots.update_search_filter('city', App.user.city);
							App.lots.update_search_filter('gender', App.user.gender == 'male' ? 'female' : 'male');

			            self.$('#save_profile').removeAttr('disabled').addClass('disabled');
		            },
						error: function() {
							self.$('#save_profile').removeAttr('disabled').addClass('disabled');
						}
	            })
				}
			})
		})(field_ids, 0);
	},

	open_add_social_account: function(e) {
		var $b = $(e.target);

		if (!this.$('#add_social_account_menu').is(':hidden')) {
			return;
		}

		var social_login_widget = new UloginWidget({
			exproviders: App.user.social_accounts,
			redirect_url: SETTINGS.SITE_URL + 'auth/ulogin_xd.html',
			display: "small",
			callback: '__ulogin_add_social_account__'
		});
		this.$('#add_social_account_menu').append(social_login_widget.render());
		this.$('#add_social_account_menu').show();
	},

	add_social_account: function(token) {
		var self = this;
		jsonRequest({
			url: '/auth/add_social_account',
			data: {token: token},
			success: function(data) {
				if (!data.state) {
					this.error(data);
					return;
				}

				var pr = data.social_account;
				self.$('#add_social_account_menu').hide().empty();
				App.user.social_accounts.push(pr);
				self.$('#social_accounts>img:last').after(
					'<img src="'+SETTINGS.STATIC_URL+ 'img/social_icon/'+ pr + '.png" alt="'+pr+'" />'
				);

				App.msg.show('Учетная запись ' + pr + ' успешно добавленна.');
			},
			error: function() {
				this.$('#add_social_account_menu').hide();
				App.msg.show('Сервис временно недоступен.');
			}
		});
	},

	put_lot: function() {
		if (this.$('#put_lot_button').hasClass('disabled')) {
			var text = "";
			if (App.user.lot) {
				text = "Вы уже выставили лот.";
			} else if (!App.user.profile_filled) {
				if (App.user.photo == SETTINGS.USER.DEFAULT_PHOTO_URL) {
					text = "Необходимо загрузить фотографию.";
				} else {
					text = "Заполните профиль.";
				}
			}

			App.msg.show(text);
			return;
		}

		var state = true;
		var $day = this.$('#close_lot_day'), $hour = this.$('#close_lot_hour');

		$day.css('border', 'none');
		if ($day.val() == $(this.$('#close_lot_day>option')[0]).val()) {
			$day.css('border', 'solid 1px red');
			state = false;
		}

		$hour.css('border', 'none');
		if ($hour.val() == $(this.$('#close_lot_hour>option')[0]).val()) {
			$hour.css('border', 'solid 1px red');
			state = false;
		}

		if (SETTINGS.DEBUG) {
			var $mins = this.$('#close_lot_minutes');
			if ($mins.val() == $(this.$('#close_lot_hour>option')[0]).val()) {
				$mins.css('border', 'solid 1px red');
				state = false;
			}
		}

		if(!state) {
			return;
		}

		var self = this;

		jsonRequest({
			url: '/lots/create/',
			type: 'POST',
			data:{
				days: $day.val(),
				hours: $hour.val(),
				mins: $mins ? $mins.val(): 0
				//percent: this.$('#to_self').text() #NOT_LOVE
			},
			success: function(data) {
				App.lots.add(data.lot);
				App.user.lot = App.lots.query("id=" + data.lot.id + ":last-first:first")[0];
				self.reinitialize();
			}
		})
	},

	tumbler_show_profile: function(e) {
		var $e = $(e.target);
		var tumbler = $e.attr('id') == 'hide_profile';

		this.$('#profile_form, #hide_profile').toggle();
		this.$('#show_profile').toggle();
	},

	add: function(rates) {
		if (!rates)
			return;

		if (this.views === undefined || this.views === null || !(this.views instanceof Array))
			this.views = new Array();

		for (var i = 0; i < rates.length; i++)
			this.views.push(new CRateView(rates[i], this.$('#rates')));
		App.router.page_build();
	},
	change_photo: function() {
		this.$('input[type=file]').click();
		this.$('#profile_main_note #UserPhotoToUpload').blur();
	}
});

var CFormPutLot = Backbone.View.extend({
	render: function() {
		return this.template("put_lot_form",this);
	}
});

var CMyRatesPage = CBaseUserPage.extend({
	views: null,

	initialize: function() {
		if (!App.user.profile_filled) {
			App.router.redirect('/profile/');
			App.msg.show('Заполните профиль', 5);
			return undefined;
		}
		this.constructor.__super__.initialize.call(this);
		this.render();
		this.views = new Array();
		App.lots.display = null;

		$('#page_content').append(this.$el);

		//if (App.user.lot) {
			jsonRequest({
				url: '/lots/get-my-rates/',
				type: 'GET',
				async: false,
				success: function(data) {
					var q = '';
					for (var i = 0; i < data.lots.length; i++)
						q += data.lots[i] + ',';
					q = '['+q.substr(0, q.length - 1) + ']';
					App.lots.upload(eval(q));
					var lots = App.lots.query('@'+q+'.indexOf(%me%.id) != -1');
					for (i = 0; i < lots.length; i++)
						while (lots[i].rates.length == 0)
							lots[i].rates.update(false);
				}
			});
		//}
		if (!App.lots.display)
			App.lots.display = this;
		App.lots.show(App.lots.query('rates.query("user.id=App.user.id:[0]").length > 0'));
		if (this.views === null || this.views === undefined || this.views.length == 0)
			this.$('#my_rates').text('Вы не участвуете в торгах').css('text-align', 'center');
		else
			this.$('#my_rates').css('text-align', 'left');
	},

	add: function(lots) {
		if (!lots)
			return;
		for (var i = 0; i < lots.length; i++)
			if (lots[i].winner === undefined || lots[i].winner === null || lots[i].winner.id == App.user.id)
				this.views.push(new CMyRateView(lots[i], this.$('#my_rates')));
		App.router.page_build();
	},

	render: function() {
		return this.template('my_rates_page', this);
	}
});

var CMyRateView = Backbone.View.extend({
	lot: null,
	timeleft: 10,

	events: {
		'click *': 'open_lot'
	},

	initialize: function(get_lot, $parent) {
		this.lot = get_lot;
		this.render();
		if (!$parent instanceof jQuery)
			$parent = $($parent);

		//this.lot.rates.update(false);

		this.update(false);
		this.autoupdate();

		$parent.append(this.$el);
	},

	open_lot: function(e) {
		App.router.redirect('/lot/'+this.lot.id+'/');
	},

	autoupdate: function() {
		var self = this;
		var f =function() {
			var t = setTimeout(function() {
				if (App.router.uri == '/my_rates/') {
					clearTimeout(t);
					self.update();
					f();
				}
			}, self.timeleft * 1000);
		};
		f();
	},

	update: function(update) { //default true
		if (update === undefined || update)
			this.lot.rates.update(false);
		var max_rate = this.lot.rates.query('type="money":last-first:max(value as int)')[0];
		var my_rate = this.lot.rates.query('user.id=App.user.id:[last]')[0];
		var rate_type = this.$('#my-rate-info');
		var rate_val = this.$('#max_rate_value');

		if (this.lot.winner !== undefined && this.lot.winner !== null) {
			if (my_rate.type == 'money') {
				rate_type.attr('class', 'money_win');
			} else if (my_rate.type == 'love') {
				rate_type.attr('class', 'love_win');
			}
			rate_val.text('Победа');
		} else if (my_rate.type == 'money') {
			if (max_rate !== undefined && max_rate.user.id == App.user.id){
				rate_type.attr('class', 'money');
				rate_val.text(max_rate.value);
			} else {
				rate_type.attr('class', 'up_value');
				rate_val.text(max_rate.value);
			}
			/*if (max_rate !== undefined && +my_rate.value < +max_rate.value) {
				rate_type.attr('class', 'up_value');
				rate_val.text(max_rate.value);
			} else {
				rate_type.attr('class', 'money');
				rate_val.text(my_rate.value);
			}*/
		} else if (my_rate.type == 'love') {
			rate_type.attr('class', 'love');
			if (max_rate !== undefined)
				rate_val.text(max_rate.value);
		}
	},

	render: function() {
		return this.template('my_rate', this);
	}
});

var CUserPage = CBaseUserPage.extend({
	initialize: function(user_id) {
		this.lot = App.lots.query('user.id='+user_id+':first')[0];
		this.user = App.users.query('id='+user_id+':first')[0];

		if (this.lot !== undefined) {
			this.user = this.lot.user;
			App.users.add(this.user);
		}

		if (this.user === undefined || this.lot === undefined) {
			CUser.upload(user_id);
			if (!this.user)
				this.user = App.users.query('id='+user_id+':first')[0];
			if (!this.lot)
				this.lot = App.lots.query('user.id='+user_id+':first')[0];
		}
		/*this.data = App.lots.query('user.id='+user_id)[0];

		if (this.data === undefined) {
			this.data = App.lots.query('user.id='+CUser.upload(user_id))[0];
			if (this.data === undefined) {
				var user = App.users.query('id='+user_id+':[0]')[0];
				if (user !== undefined)
					this.data = {
						date: 0,
						user: user
					};
			}
		}*/

		if (this.user !== undefined || this.lot !== undefined) {
			this.constructor.__super__.initialize.apply(this, arguments);

			this.render();

			if (this.lot !== undefined && this.lot.id !== undefined && this.lot.id != -1) {
				var tl = timeleft(this.lot);
				if (tl !== undefined) {
					this.$('#time-left > span').text(tl);
					this.date_timer(jQuery.extend({}, this.lot, {
						parent: '#time-left > span',
						end_function: this.end_function
					}));
				}

				this.lot.rates.display = this;
				this.lot.rates.show();
				var self = this;
				function auto_update(interval) {
					var t = setTimeout(function() {
						if (/^\/user\/\d+\/$/.test(App.router.uri) && App.router.uri.match(/\d+/)[0] == self.user.id) {
							clearTimeout(t);
							self.lot.rates.update();
							auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
						}
					}, interval);
				}
				auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
				this.lot.rates.update(false);
			} else {
				this.$('#time-left').html('Лот не продаётся');
			}

			$('#page_content').append(this.$el);
		} else {
			App.router.error404();
		}
	},

	date_timer: date_timer,
	
	end_function: function() {
		if (this.data.winner instanceof CUser) {
			this.$('#rates').empty();
			this.$('#time-left').html('Победитель "'+this.data.winner.first_name+'"');
			this.__open_chat();
		} else {
			this.$('#time-left').html('Выберете победителя');
			this.$('#rates').empty();
			this.data.rates.show(this.data.rates.query('type="money"::max(value as int)'));
			this.data.rates.show(this.data.rates.query('type="love":last-first'));
		}
	},

	add: function(rates) {
		if (!rates)
			return;

		if (this.views === undefined || this.views === null || !(this.views instanceof Array))
			this.views = new Array();

		for (var i = 0; i < rates.length; i++)
			this.views.push(new CRateView(rates[i], this.$('#rates')));
		App.router.page_build();
	},

	render: function() {
		return this.template('user_profile', this);
	}
});