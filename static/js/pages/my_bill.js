var CMyBillPage = CBaseUserPage.extend({
	events: {
		'change #sum': 'sumWrongSym',
		'keyup #sum': 'sumWrongSym',
		'click #bring': 'bring_money',
		'click #withdraw': 'withdraw_money',
		'click #show_rules': 'show_rules'
	},

	consts: {
		MAX_MONEY: 14000,
		MIN_MONEY: 100
	},

	initialize: function() {
		if (!App.user.profile_filled) {
			App.router.redirect('/profile/');
			App.msg.show('Заполните профиль', 5);
			return undefined;
		}

		var self = this;
		this.constructor.__super__.initialize(arguments);
		$('#page_content').append(this.render());

		jsonRequest({
			url: '/purse/capthca/',
			success: function(data) {
				if (data.state) {
					if (!data.is_auth) {
						self.$('img#captcha').attr('src', data.capthca_url);
						self.$('#captcha_form').show();
					}
					///if (App.user.purse.available() != 0) {
						self.$('#withdraw')
							.removeClass('disabled')
							.removeAttr('disabled');
					//}

				} else {
					App.msg.show(data.error);
				}
				App.router.page_build();
			}
		});

		App.user.purse.bind('change_sum', this.on_change_sum, this);
	},

	on_change_sum: function(purse) {
		this.$("#user_money").text(purse.available());
		this.$('#user_money_involved').text(purse.involved);
	},

	sumWrongSym: function(e){
		$('#field').css('border', '1px solid #8f8f8f');
		$(e.target).val($(e.target).val().replace(/[^\d]/g, ''));
	},

	withdraw_money: function(e) {
		if(this.check_sum()){
			var $e = $(e.target);
			var $sum = this.$('#sum>input');
			var answer = this.$("#captcha_form").attr('display') == 'none'? undefined: this.$('#captcha_form>input').val();
			var self = this;

			$e.attr('disabled', 'disabled');

			var $form_cp = self.$('#captcha_form');
			App.user.purse.withdraw($sum.val(), answer, function(data) {
				if (!data.state) {
					self.$('img#captcha').attr('src', '');
					self.$('img#captcha').attr('src', data.new_captcha_url);
					self.$('#captcha_form>input').val('');
					
					if (!data.is_auth && $form_cp.is(':hidden')) {
						App.msg.show("Введите код и повторите попытку.");
					} else {
						App.msg.show(data.error);
					}
				}

				$form_cp[data.is_auth ? 'hide' : 'show']();
				$e.removeAttr('disabled');
				if (data.state) {
					App.msg.show('На ваш QIWI кошелек переведено ' + data.translated_money + ' рублей.');
				}
				App.router.page_build();
			});
		} else {
			this.show_msg_bad_money_value();
		}
	},

	check_sum: function() {
		var $sum = this.$('#sum>input');
		if($sum.val() < this.consts.MIN_MONEY || $sum.val() > this.consts.MAX_MONEY || !$sum.val().length){
			$sum.css('border', '2px solid #ff0000');

			if (this.consts.MIN_MONEY > $sum.val()){
				$sum.val(this.consts.MIN_MONEY);
			}
			return false;
		}

		return true;
	},


	bring_money: function(e) {
		if(this.check_sum()){
			var $e = $(e.target);
			$e.attr('disabled', 'disabled');
			var self = this;
			App.user.purse.bring(this.$('#sum>input').val(), function(data) {
				if (!data.state) {
					App.msg.show(data.error);
				} else {
					App.msg.show('На ваш QIWI кошелек выставлен счет.');
					self.$('#sum>input').val('');
				}
				$e.removeAttr('disabled');
			});
		} else {
			this.show_msg_bad_money_value();
		}
	},

	show_msg_bad_money_value: function(){
		App.msg.show('Введенная вами сумма не корректна. Введите не менее '+this.consts.MIN_MONEY+' и не более '+this.consts.MAX_MONEY+' единиц.', 7);
	},

	show_rules: function(){
		App.router.redirect("/");
		if (App.page.rules) {
			App.page.rules.rules_toggle(function() {
				App.page.rules.goto('#sub-1');
				App.page.rules.subject_toggle('#sub-1', function() {
					App.page.rules.goto(766);
				});
			});
		}
	},


	render: function() {
		return this.template('my_bill', this);
	}
});

var CPurse = Backbone.Model.extend({
	initialize: function(data) {
		if (data.id === undefined)
			return;
		
		data = data || {};

		this.sum = data.sum || 0;
		this.involved = data.involved || 0;
	},

	bring: function (sum, callback) {
		jsonRequest({
			url: '/purse/bring/',
			data: {sum: sum},
			success: function (data) {
				if (callback) {
					callback(data);
				}
			}
		})
	},

	withdraw: function(sum, captcha, callback) {
		var self = this;
		var data = {sum: sum};
		if (captcha != undefined) {
			data.captcha = captcha;
		}

		jsonRequest({
			url: '/purse/withdraw/',
			data: data,
			success: function(data) {
				if (callback) {
					callback(data);
					if (!data.state) {
						return;
					}
				}

				self.sum -= sum;
				self.on_change_sum();
			}
		})
	},

	on_change_sum: function() {
		this.trigger('change_sum', this);
	},

	available: function() {
		return this.sum - this.involved;
	}
});
