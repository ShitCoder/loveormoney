
var CLoginPage = CBaseMainPage.extend({
	initialize: function(preview) {
		this.constructor.__super__.initialize.apply(this, arguments);

		this.login_panel = new CLoginPanel();

		this.$el = $('#page_content');
		$('#body')
			.prepend(this.login_panel.$el)
			.append(this.login_panel.login_popup.render());
		this.login_panel.login_popup.$el.centered();
		if (preview === undefined || preview === true) {
			this.template('lots_page_unlog', this);
			$('#page_content').append(this.$el);
			this.display = new CBaseDisplay($('#lots'), CGeneralLotView);
			App.lots.display = this.display;
			App.lots.search_filter.gender = '';
			App.lots.search_filter.city = '';
			App.lots.search_filter.count += SETTINGS.LOTS_IN_THE_PAGE;
			App.lots.page_index = 1;
			App.lots.update();
		}
		this.rules = new CRules();
		$('#page_content').prepend(this.rules.render());
		$('#padding').remove();
		this.delegateEvents();
	},

	events: {
		'click #lvl1_show': 'show_lvl2',
		'click .instructions_link': 'show_instructions',
		'click #close_rules': 'close_rules'
	},

	close_rules: function(e){
		var $e = $(e.target);
		this.$('#lvl2_rules').slideUp(0);
		this.$('#lvl1_show').show();
		this.$('#close_rules').hide();
	},

	show_lvl2: function(e){
		var $e = $(e.target);
		this.$('#lvl2_rules').slideDown('normal');
		this.$('#lvl1_show').hide();
		this.$('#close_rules').show();
		return false;
	},

	show_instructions: function(e){
		var $e = $(e.target);
		$e.next().toggle('normal');
		return false;
	}
});

var CMainPage = CBaseUserPage.extend({
	last_count: 0,

	events: {
		'click #search_form_gender button': 'set_search_filter',
		'keyup global #city': 'set_search_filter'
	},

	initialize: function() {
		this.constructor.__super__.initialize.apply(this, arguments);

		this.city = new combo({
			render: false,
			list: InitializeData.CITIES,
			empty: 'Все города',
			empty_val: '',
			classes: {
				'parent': 'city'
			},
			ids: {
				'parent': 'city-filter',
				'input': 'city',
				'list': 'cities-list'
			},
			free: false,
			lines: 10,
			line_size: 14,
			scroll: true,
			valid: true,
			trim: true,
			show_all: true
		});

		if (App.user.state.get('search').city)
			this.city.select(App.user.state.get('search').city);
		else
			this.city.select_default();
		var self = this;
		this.city.onSelect = function(e) {
			self.set_search_filter(e);
		};
		this.userPanel.$('#lot-place').html('').append(this.city.render());
		this.userPanel.$('#lot-place').show().css('z-index', '3');


		this.rules = new CRules();
		$('#page_content').prepend(this.rules.render());
		this.template('lots_page', this);

		this.display = new CBaseDisplay(this.$('#lots'), CGeneralLotView);
		App.lots.display = this.display;
		
		$('#page_content')
			.append(this.$el);

		$(window).scroll(this.on_scroll);

		App.lots.page_index = 0;
		$('#padding').hide();

		this.$('#search_form_gender button[value='+App.user.state.get('search').gender+']').addClass('select_gender');
		App.lots.update_search_filter('city', App.user.state.get('search').city);
		App.lots.update_search_filter('gender', App.user.state.get('search').gender);

		//$('#lots').append('<div id="no-lots">Загружаю...</div>');
		App.lots.search();
		/*if (App.lots.length > 0) {
			$('#lots > #no-lots').remove();
		} else {
			this.no_lots();
		}*/
		share42();
	},

	on_scroll: function() {
		var self = this;
		if(document.body.scrollTop < (document.body.scrollHeight - window.innerHeight - 10)) {
			return;
		}
		//$('#lots').append('<div id="no-lots">Загружаю...</div>');
		App.lots.get_more();
	},

	set_search_filter: function(e) {
		var $e = e instanceof jQuery ? e : $(e.target);
		var field = $e.attr('id');
		var val = $e.val();
		if (field == 'city') {
			if (!this.city.valid())
				return;
			val = this.city.val();
		}
		if (App.lots.search_filter[field] == val)
			return;
		if ($e.closest('#search_form_gender').length) {
			field = 'gender';
			$('#search_form_gender button').removeClass('select_gender');
			$e.addClass('select_gender');
		}

		App.lots.update_search_filter(field, val);
		App.lots.page_index = 0;
		App.lots.search();
	},

	show_some_lots: function() {
		App.lots.show(App.lots.query('user.city="'+this.$('#city').val()+'" & user.gender="'+this.$('#gender').val()+'":['+this.last_count+', '+this.last_count+18+']' ));
		this.last_count += 18;
	},

	reset: function() {
		for(var i = 0; i < this.collection.length; i++) {
			this.collection[i].remove();
		}

		this.collection = new Array();
	},

	close_rules: function(e){
		var $e = $(e.target);
		this.$rules.find('#lvl2_rules').slideUp(0);
		this.$rules.find('#lvl1_show').show();
		this.$rules.find('#close_rules').hide();
	},

	no_lots: function() {
		var city = this.city.val();
		var gender = ['', 'парней', 'девушек'][['', 'male', 'female'].indexOf(App.lots.search_filter.gender)];
		var text = '';
		if (city == this.city.get('empty_val')) {
			gender += ' ', city += ' ';
			text = 'Активные лоты '+gender+'отсутствуют';
		} else {
			gender += ' ', city += ' ';
			text = 'В городе '+city+'активные лоты '+gender+'отсутствуют';
		}
		$('#lots > #no-lots').remove();
		$('#lots').append('<div id="no-lots">'+text+'</div>');
	}
});

var CBaseDisplay = Backbone.View.extend({
	collection: new Array(),

	initialize: function($parent, viewer, viewer_data) {
		if (viewer === undefined || $parent === undefined)
			return;

		this.viewer = function() {
			return viewer;
		};
		this.parent = function() {
			return $parent;
		};
		this.data = function() {
			return viewer_data;
		};
	},

	remove: function() {
		while (this.collection.length > 0) {
			this.collection.pop().remove();
		}
	},

	reshow: function(views) {
		this.remove();
		this.add(views);
	},

	add: function(views) {
		if (!views || views.length == 0) {
			if (App.page && App.page.no_lots)
				App.page.no_lots();
			App.router.page_build();
			return;
		} else {
			$('#lots > #no-lots').remove();
		}
		var v = this.viewer();
		for (var i = 0; i < views.length; i++) {
			views[i].data = this.data();
			this.collection.push(new v(views[i], this.parent()));
		}
		App.router.page_build();
	}
});

var CGeneralLotView = Backbone.View.extend({
	initialize: function(data, $parent) {
		var self = this;
		if (data) {
			this.data = {};
			this.data = data;
			this.data.user = (data.user.id == App.user.id?App.user:data.user);
			this.data.date = (data.date instanceof Date?data.date:new Date(data.date) || new Date.now());
			this.data.parent = '.remained_type > span';
			if (data.data !== undefined)
				this.page_cid = data.data.page_cid;
			this.render();
		}

		this.time_interaval_type = 0;

		var tl = timeleft(this.data);
		if (tl !== undefined) {
			this.$('.remained_type > span').text(tl);
			this.data.end_function = this.end_function;
			this.date_timer(this.data);
		} else {
			this.end_function();
		}

		$parent.append(this.$el);
	},

	events: {
		'click *': 'open_lot'
	},

	open_lot: function(e) {
		var this_lot = App.lots.query('user.first_name = "'+this.data.user.first_name+'":first')[0];
		if (($(e.target).attr('class') || '').search('remained_type') > -1 && this_lot.winner instanceof CUser)
			App.router.redirect('/user/'+this_lot.winner.id+'/');
		else
			App.router.redirect('/lot/'+this.data.id+'/');
	},

	date_timer: date_timer,
	end_function: function() {
		var this_lot = App.lots.query('user.first_name = "'+this.data.user.first_name+'":first')[0];
		if (this_lot === undefined)
			return;
		
		if (this_lot.winner instanceof CUser)
			this.$('.remained_type').html('Победитель пользователь "'+this_lot.winner.first_name+'"');
		else
			this.$('.remained_type').html('Идёт выбор победителя');
	},

	render: function() {
		return this.template('general_lot_view', this);
	}
});
