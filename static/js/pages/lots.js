

var CLotViewPage = CBaseUserPage.extend({
	lot: null,
	rate_type: -1,
	add_love_form: undefined,
	add_money_form: undefined,
	views: new Array(),

	events: {
		'click #rate-select > div > div': 'get_rate',
		'click #back': 'to_main',
		'click #close': 'close_add_form',
		'click #post': 'post_rate',
		'click #prev_page': 'prev_page',
		'click #next_page': 'next_page'
	},

	initialize: function(lot_id) {
		if (typeof lot_id == 'number') {
			this.lot = App.lots.query('id=='+lot_id)[0];
			if (!this.lot) {
				App.lots.upload(lot_id);
				this.lot = App.lots.query('id=='+lot_id)[0];
			}
		} else {
			this.lot = lot_id;
		}

		if (this.lot !== undefined) {
			this.constructor.__super__.initialize.apply(this, arguments);

			this.lot.rates.display = this;

			this.add_love_form = this.template('love_rate_add_form', this, true);
			this.add_money_form = this.template('money_rate_add_form', this, true);

			var self = this;
			this.add_money_form.find('#val').bind('keyup', function(e) {
				var $el = $(e.target);
				if ($el.val().length > 0)
					$el.val($el.val().match(/\d+/g).join(''));
			});

			this.lot.user.about_me = this.lot.user.about_me.replace(/\n/g, '<br>');
			this.template('lot_page', this);

			this.lot.over = true;
			var tl = timeleft(this.lot);
			if (tl !== undefined) {
				this.$('#time-left > span').append(tl);
				this.date_timer(jQuery.extend({}, this.lot, {
					parent: '#time-left > span',
					end_function: this.end_function
				}));
			}

			this.lot.rates.show();

			function auto_update(interval) {
				var t = setTimeout(function() {
					if (/^\/lot\/\d+\/$/.test(App.router.uri) && (!self.lot || (self.lot && App.router.uri.match(/\d+/)[0] == self.lot.id))) {
						clearTimeout(t);
						self.lot.rates.update();
						auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
					}
				}, interval);
			}
			if (App.user.is_authenticated())
				auto_update(SETTINGS.LOT_RATES_AUTOUPDATE_TIMEOUT);
			this.lot.rates.update(false);

			if (!tl) {
				//this.actives = false;
				if (!App.user.is_authenticated()) {
					App.router.redirect('/');
					return;
				} else {
					this.end_function();
				}
			} else {
				//this.actives = this.lot.rates.query('user.id = ' + App.user.id + ':[0]').length > 0;
			}
			this.actives = App.router.from != '/my_rates/';

			if (!App.user.profile_filled || this.lot.user.id == App.user.id)
				this.$('#rate-select').remove();
			$('#page_content').append(this.$el);
		} else {
			if (App.user.is_authenticated()) {
				App.router.error404();
			} else {
				App.router.redirect('/');
			}
		}
	},

	date_timer: date_timer,
	end_function: function() {
		if (!App.user.is_authenticated()) {
			App.router.redirect('/');
			return;
		}
		if (this.lot.user.id == App.user.id) {
			var t = setTimeout(function() {
				clearTimeout(t);
				App.router.redirect('/profile/');
			}, 200);
			return;
		}
		this.$('#rate-select').remove();
		if (this.lot.winner instanceof CUser) {
			this.$('.rates').empty();
			this.$('#time-left').html('Победил "'+this.lot.winner.first_name+'"');
			if (this.lot.winner.id == App.user.id) {
				this.chat = new CChat(this, this.lot);
				this.$('#rates > #title > div').text('Вы победили!');
				this.$el.append(this.chat.$el);
			} else {
				this.lot.rates.show();
				var self = this;
				q(60, false);
			}
		} else {
			this.lot.rates.update();
			if (this.lot.rates.length > 0) {
				this.$('#time-left').html('Идёт выбор победителя<span></span>');
				var self = this;
				function q(interval, wait_winner) {
					var t = setTimeout(function() {
						clearTimeout(t);
						self.lot.update();
						if (self.lot.active())
							if ((wait_winner === undefined || wait_winner == true) && self.lot.winner !== undefined && self.lot.winner !== null) {
								self.end_function();
							} else {
								if (interval < 60)
									interval++;
								q(interval, wait_winner);
							}
						else {
							var uid = App.page.lot.user.id;
							App.lots.remove(this.lot);
							App.router.redirect('/user/'+uid+'/');
						}
					}, interval * 1000)
				}
				q(1, true);
			} else {
				var self = this;
				jsonRequest({
					url: '/lots/get-state/',
					type: 'GET',
					data: {
						lot_id: this.lot.id
					},
					success: function(data) {
						switch (data.complete_status.toLowerCase()) {
							case 'select-winner':
								self.$('#time-left').html('Идёт выбор победителя<span></span>');
								break;
							case 'no-rates':
								var user_id = self.lot.user.id;
								App.lots.remove(self.lot);
								App.router.redirect('/user/'+user_id+'/');
								break;
						}
					}
				});
			}
		}
	},

	prev_page: function() {
		var lot = this.prev();
		if (lot !== undefined)
			App.router.redirect('/lot/'+lot.id+'/');
	},
	next_page: function() {
		var lot = this.next();
		if (lot !== undefined)
			App.router.redirect('/lot/'+lot.id+'/');
	},
	lots_with_filter: function() {
		var lots = [];
		with (App.lots.search_filter) {
		if (gender == '' && city == '')
			lots = App.lots.all();
		else
			lots = App.lots.query(
					  (gender != '' ? 'user.gender="'+gender+'" & ' : '') +
						(city != '' ? 'user.city="' + city + '" & ': '') +
						'date.getTime() > new Date().getTime() - DEFFERENCE_TIME' //Warning!! та строка может не работать, необходимо оттестить
					);
		};
		return lots;
	},
	my_rates_lots: function() {
		return App.lots.query('rates !== undefined && rates.query("user.id = '+App.user.id+':[0]").length > 0');
	},
	next: function() {
		var lots = this.actives ? this.lots_with_filter() : this.my_rates_lots();
		if (lots === undefined)
			return undefined;
		var index = lots.indexOf(this.lot);
		return ++index < lots.length ? lots[index] : undefined;
	},
	prev: function() {
		var lots = this.actives ? this.lots_with_filter() : this.my_rates_lots();
		if (lots === undefined)
			return undefined;
		var index = lots.indexOf(this.lot);
		return --index < lots.length ? lots[index] : undefined;
	},

	//get_rate: открывает нужную форму добавления ставки
	get_rate: function(e) {
		if (this.$('.rate-add-form')[0])
			this.close_add_form();

		this.$('.rates').append(this['add_' + e.target.id + '_form']);

		this.rate_type = ['love', 'money'].indexOf(e.target.id);

		if (this.rate_type == -~-13[13]) {
			var rates = this.lot.rates.query('type="money":last-first:max(value):[first]');
			this.$('#val').val(rates.length > 0 ? rates[0].value: 100);
			this.$('#val').bind('keyup keydown keypress', function(e) {
				var $e = $(e.target);
				if(!/^\d*$/.test($e.val())) {
					return false;
				}
			})
		}

		document.body.scrollTop = document.body.scrollHeight;
	},

	//post_rate: публикует ставку
	//to_main: родительская функция

	check_rate_form: function() {
		var $form = this.$('#content');
		var $val = $form.find('#val');
		var val = $val.val();
		var state = false;

		switch(this.rate_type) {
			case 1:
				var rates = this.lot.rates.query('type="money":last-first:max(value):[first]');
				var max = rates.length > 0 ? rates[0].value : 99;
				state = val % 100 == 0 && val > 0 && /^\d*$/.test(val) && val > max;
				if (val % 100 != 0) {
					App.msg.show('Ставка должна быть кратна 100');
				}

				if (val <= max) {
					App.msg.show('Ставка должна быть выше ' + max + '.');
				}
				break;
			case 0:
				state = val.length > 0 && val.length <= 150;
				break;
		}

		$val[state ? 'removeClass' : 'addClass']('bad_fill_field');
		return state;
	},

	post_rate: function(e) {
		var self = this;
		var last_love = this.lot.rates.query('type="love":last-first:first')[0];
		var last_money = this.lot.rates.query('type="money":last-first:first')[0];

		if (!this.check_rate_form()) {
			return;
		}
		
		jsonRequest({
			url: '/lots/set-rate/',
			data: {
				type: ['love', 'money'][this.rate_type],
				value: this.$('#content > #val')[(e.target.id == 'love'?'text':'val')](),
				lot_id: this.lot.id,
				last_love_id: last_love ? last_love.id : 0,
				last_rate_id: last_money ? last_money.id : 0
			},
			type: 'POST',
			success: function(data) {
				if (data.state == 0) {
					self.close_add_form();
				} else {
					var msg = "";
					switch(data.state) {
						case 1:
							msg = "У вас недостаточно средств.";
							break;
						case 2:
							msg = "Необходимо поднять ставку.";
							break;
						case 3:
							msg = "Вы признавались ранее.";
							break;
						case 4:
							msg = "Вы не можете поставить денежную ставку, т.к. отправляли признание";
							break;
						case 5:
							msg = "Вы не можете отправить признание, т.к. делали денежную ставку";
							break;
						default:
							throw "Does't exit error text";
					}
					App.msg.show(msg);
				}
				self.lot.rates.update_rates(data.upload);
			}
		})
	},
	//close_add_form: удаляет форму
	close_add_form: function() {
		this.$('.rate-add-form').remove();
		this.rate_type = -1;
	},

	add: function(rates) {
		if (!rates)
			return;

		for (var i = 0; i < rates.length; i++)
			this.views.push(new CRateView(rates[i], this.$('.rates')));
	},

	remove: function() {
		this.constructor.__super__.remove.apply(this, arguments);
	}
});

var CRateView = Backbone.View.extend({
	initialize: function(data, $parent) {
		if (!data) return;

		this.data = {};
		this.data = data;
		this.data.user = (data.user.id == App.user.id?App.user:data.user);
		this.data.date = (data.date instanceof Date?data.date:new Date(data.date) || new Date.now());
		this.data.line = true;
		this.data.nosecs = true;
		this.data.parent = '.time_less > span';
		this.data.first_timeout = 0.01;

		this.render();

		//this.$('.time_less > span').text(timeleft(this.data, true));
		this.date_timer(this.data);

		$parent.append(this.$el);
	},

	date_timer: date_timer,

	render: function() {
		return this.template('rate', this);
	}
});

var CRate = Backbone.Model.extend({
	defaults: {
		user: new CAnonymousUser(),
		date: new Date(),
		type: -1,
		value: null
	},

	initialize: function(data) {
		this.user = new CUser(data.user);
		this.date = typeof data.date == 'string' ? new Date(data.date) : data.date;
		this.type = ['love', 'money'][data.type];
		this.value = data.text || data.sum;
	}
});

var CRateList = CCollection.extend({
	model: CRate,

	update_rates: function(data) {
		if (data && data.length > 0) {
			var sf = this.length;
			this.add(data);
			this.show_some(sf);
		}
	},

	update: function(lot_id, async) {
		var self = this;
		if (async === undefined && lot_id !== undefined && typeof lot_id != 'boolean')
			async = true;
		if (async == undefined && typeof lot_id == 'boolean') {
			async = lot_id;
			lot_id = undefined;
		}

		var last_love = self.query('type="love":last-first:first')[0];
		var last_money = self.query('type="money":last-first:first')[0];

		jsonRequest({
			url: '/lots/get-rates/',
			type: 'GET',
			data: {
				lot_id: lot_id || self.lot_id,
				last_love_id: last_love ? last_love.id : 0,
				last_rate_id: last_money ? last_money.id : 0
			},
			async: async,
			success: function(data) {
				self.update_rates(data.upload);
			}
		})
	}
});

var CLot = Backbone.Model.extend({
	rates: null,
	defaults: {
		id: -1,
		user: new CAnonymousUser(),
		date: new Date(),
		winner: new CAnonymousUser()
	},

	initialize: function(data) {
		this.set_data(data);
	},

	set_data: function(data) {
		if (data !== undefined) {
			this.id = data.id;
			this.date = typeof(data.date) == 'string' ? new Date(data.date) : data.date;
			this.user = new CUser(data.user);
			this.winner = data.winner ? new CUser(data.winner) : null;
			this.percent = data.percent || undefined;
			if (this.rates === undefined || this.rates === null) {
				this.rates = new CRateList();
				this.rates.lot_id = this.id;
			}
		}
		this.active = function() {
			return data !== undefined ? data.active : false;
		}
	},

	update: function() {
		var self = this;
		jsonRequest({
			url: '/lots/get-lots/',
			type: 'GET',
			async: false,
			data: {
				method: 'include',
				id: this.id,
				count: 1
			},
			success: function(data) {
				if (data.upload)
					self.set_data(data.upload[0]);
			}
		})
	}
});

var CLots = CCollection.extend({
	model: CLot,
	display: null,
	search_filter: {
		gender: '',
		city: '',
		ids: [],
		count: SETTINGS.LOTS_IN_THE_PAGE // count lots upload
	},
	page_index: 0,

	initialize: function(data_arr) {
		this.add(data_arr);
		this.update_search_filter();
		if(App.user.constructor == CUser) {
			this.update_search_filter('gender', App.user.state.get('search').gender);
			this.update_search_filter('city', App.user.state.get('search').city);
		}
	},

	comparator: function(el) {
		return el.date;
	},

	show_page: function() {
		///Warning!: Тут надо откорректировать выборку
		this.display.remove();
		var q = '' + (this.search_filter.gender != '' ? 'user.gender="'+this.search_filter.gender+'"' : '');
		if (this.search_filter.city != '')
				q += (q.length > 0 ? ' & ' : '') + 'user.city="' + this.search_filter.city + '"';
		var lots = [];
		if (q == '')
			lots = this.query('date.getTime() > new Date().getTime() - DEFFERENCE_TIME:[first, '+(this.page_index * SETTINGS.LOTS_IN_THE_PAGE)+']');
		else
			lots = this.query('date.getTime() > new Date().getTime() - DEFFERENCE_TIME & ' + q + ':[first, '+(this.page_index * SETTINGS.LOTS_IN_THE_PAGE)+']');
		//this.display.reshow(lots.slice(0, this.page_index * SETTINGS.LOTS_IN_THE_PAGE));
		if (this.display.reshow)
			this.display.reshow(lots);
	},

	add: function(data) {
		var last_i = this.models.length - 1; last_i = last_i == -1? 0: last_i;
		this.constructor.__super__.add.apply(this, arguments);
		if (this.display) {
			this.show_page();
		}
		this.update_search_filter();
	},

	get_more: function() {
		var self = this;
		this._search(function(data) {
			self.search_filter.count *= 2;
			self.add(data.lots);
		});
	},

	search: function() {
		var self = this;
		this._search(function(data) {
			self.add(data.lots);
		});
	},

	update: function() {
		var self = this;
		var ids = this.get('id');

		jsonRequest({
			url: '/lots/update_list/',
			type: 'GET',
			data: {
				city: self.search_filter.city,
				gender: self.search_filter.gender,
				ids: ids.slice(0, self.search_filter.count - SETTINGS.LOTS_IN_THE_PAGE).toString()
			},
			success: function(data) {
				self.add(data.lots);
			}
		});
	},

	_search: function(success) {
		var self = this;
		this.search_filter.ids = this.get('id').toString();
		jsonRequest({
			url: '/lots/search/',
			data: this.search_filter,
			type: 'GET',
			success: function(data) {
				self.page_index++;
				if (App.user.is_authenticated()) {
					App.user.state.set('search', {city: self.search_filter.city, gender: self.search_filter.gender});
				}
				success(data);
			}
		})
	},

	update_search_filter: function(field, val) {
		switch(field){
			case 'city':
			case 'gender':
				this.search_filter[field] = val;
				break;
			case 'ids':
				this.search_filter[field] = val || this.get('id').toString();
				break;
			default:
				var last = this.last();
				this.search_filter.ids = this.get('id').toString();
		}

	},

	upload: function(get_ids, method, count) {
		if (! (typeof get_ids == 'number' || get_ids instanceof Array))
			return;
		if (typeof method != 'boolean')
			method = true;
		if (this.length != this.models.length) {
			this.length = this.models.length;
		}

		var d = {};
		d.method = method ? 'include' : 'exclude';
		d.count = count || '';

		if (get_ids instanceof Array) {
			var ids = undefined;
			if (App.lots.models.length > 0 && method) {
				ids = new Array();
				for (var i = 0; i < get_ids.length; i++)
					if (App.lots.query('id='+get_ids[i]+':[0]').length == 0)
						ids.push(get_ids[i]);
			}
			d.ids = ids !== undefined ? ids.toString() : get_ids.toString();
		} else {
			d.id = get_ids;
		}
		
		var self = this;
		if ((d.ids !== undefined && d.ids.length > 0) || (d.id !== undefined && App.lots.query('id='+d.id+':[0]').length == 0))
			jsonRequest({
				url: '/lots/get-lots/',
				type: 'GET',
				async: false,
				data: d,
				success: function(data) {
					if (App.user.is_authenticated()) {
						App.user.state.set('search', {city: self.search_filter.city, gender: self.search_filter.gender});
					}

					var have_ids = App.lots.get('id');
					var upload = data.upload.filter(function(a) {
						return (have_ids.indexOf(a.id) == -1 ? a : undefined);
					});
					self.add(upload);
				}
			})
	},

	get: function(field, lots) {
		if (this.models.length == 0)
			return [];
		if (this.length != this.models.length) {
			this.length = this.models.length;
		}
		if (typeof field != 'string')
			return lots || this.models;

		var get_line = function(index) {
			return (lots === undefined ? 'this.models['+(index || 0)+'].' : 'lots['+(index || 0)+'].');
		};
		try {
			eval(get_line()+field);
		} catch (e) {
			return [];
		}

		var list = new Array();
		for (var i = 0, l = lots === undefined ? this.length : lots.length; i < l; i++)
			list.push(eval(get_line(i)+field));

		return list;
	}
});