
var CRules = Backbone.View.extend({
	initialize: function() {
	},

	events: {
		'click #show_rules': 'rules_toggle',
		'click #close_rules': 'rules_toggle',
		'click .subject-title': 'subject_toggle'
	},

	rules_toggle: function(success) {
		var $rules =this.$('#lvl2_rules');
		if ([undefined, null, 'none'].indexOf($rules.css('display')) != -1) {
			$rules.slideDown('normal', function() {
				if (typeof success == 'function')
					success();
			});
		} else {
			$rules.slideUp('normal', function() {
				if (typeof success == 'function')
					success();
			});
		}
		$('#show_rules').toggle();
		$('#close_rules').toggle();
	},

	subject_toggle: function(e, success) {
		var $e = null;
		if (e instanceof jQuery) {
			$e = e;
		} else if (typeof e == 'string') {
			$e = $(e);
		} else {
			$e = $(e.target);
		}
		var $block = $e.next();
		if ([undefined, null, 'none'].indexOf($block.css('display')) != -1) {
			$block.slideDown('normal', function() {
				if (typeof success == 'function')
					success();
			});
		} else {
			$block.slideUp('normal', function() {
				if (typeof success == 'function')
					success();
			});
		}
	},

	goto: function(e) {
		var $e = null;
		var top = 0;
		if (e instanceof jQuery) {
			$e = e;
		} else if (typeof e == 'string') {
			$e = $(e);
		} else if (typeof e == 'number') {
			top = e;
		} else {
			$e = $(e.target);
		}
		$('body').scrollTop(top != 0? top : /*this.$el.position().top + */$e.position().top);
	},

	render: function() {
		this.template("rules", this);
		this.delegateEvents.call(this);
		return this.$el;
	}
});