var CPage = Backbone.View.extend({
	views: {},

	open: function(url) {
		App.router.redirect(url);
	},
	
	reinitialize: function() {
		this.remove();
		this.views = {};
		this.initialize();
		this.delegateEvents();
	},

	remove: function() {
		$('#page_content').empty();
		$('#body > #user_panel').remove();
		$(window).unbind('scroll');
	}
});

var CBaseMainPage = CPage.extend({

});

var CBaseUserPage = CPage.extend({
	initialize: function() {
		if (App.user.is_authenticated()) {
			this.userPanel = new CUserPanel();
			$('#body')
				.prepend(this.userPanel.render());
		} else {
			this.userPanel = new CLoginPanel();
			$('#body')
				.prepend(this.userPanel.$el)
				.append(this.userPanel.login_popup.render());
			this.userPanel.login_popup.$el.centered();
		}

		$('#padding').show();
	},

	events: {
		'click global #user_panel > #title': 'to_main'
	},

	//to_main: возвращает к галерее
	to_main: function() {
		if (this.actives !== undefined && !this.actives) {
			App.router.redirect('/my_rates/');
		} else {
			App.router.redirect('/');
		}
	}
});
