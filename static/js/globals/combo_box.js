
var combo = function(data) {
	var self = this;
	var si = -1;
	var last_text = '';
	var id = new Date().getTime();

	this.$el = $('<div></div>');
	this.$cnt = $('<div></div>');
	this.$val = $('<input type="text" autocomplete="off"/>');
	this.$list = $('<ul class="list"></ul>');
	this.$selector = $('<input type="button" value="▼" />');
	this.$parent= null;

	data = jQuery.extend({
		input: true, //Возможность вводить
		val: '', //Значение по-умолчанию
		holder: '', //Значение-подсказка (placeholder)
		render: true, //отрисовывать ли элемент сразу
		list: new Array(), //Список данный для бокса
		classes: {
			'parent': 'combo',
			'input': '',
			'list': ''
		}, //Класс(ы)
		ids: {
			//Эквивалентно классам
		},
		search: true, //Включить поиск (только если input == true)
		register: false, //Учитывать регистр при поиске
		free: true, //true - свободный поиск (содержит), false - бедный поиск (начинается с)
		lines: 0, //количество отображаемых линий (0 - отображать все)
		scroll: false, //true - будет прокрутка, если значение lines меньше, чем найденый эл-тов
							//false - будет искать и отображать только lines-элементов
		line_size: 20, //высота строки
		valid: true, //Выполнять ли проверку на валидность
		trim: false, //Исспользовать ли обрезание пробелов и табуляции по краям
		show_all: false, //При нажатии на стрелочку
		empty: false, //false - не использовать, true - исспользовать пустое, 'val'- исспользовать значение
		empty_val: '' //возвращаемый результат при пустом значении
	}, data);

	if (data.parent instanceof jQuery) {
		this.$parent = data.parent;
	} else if (typeof data.parent  == 'string') {
		this.$parent = $(data.parent);
	}

	function show_elements(list, not_null) {
		self.$list.empty();
		not_null = not_null === undefined ? true : not_null;
		if (!not_null || (not_null && list.length > 0)) {
			if (data.empty) {
				self.$list.append('<li class="list-item" id="-1" style="height: '+(data.line_size)+';">'+(typeof data.empty == 'bolean'?'':data.empty)+'</li>');
			}
			for (var i = 0, l = list.length; i < l; i++)
				self.$list.append('<li class="list-item"  style="height: '+(data.line_size)+';" id="'+data.list.indexOf(list[i])+'">'+list[i]+'</li>');
			self.$list.find('*').bind('click', function(e) {
				var $e = $(e.target);
				var $li = $e.hasClass('list-item') ? $e : $e.parents('.list-item');
				//var id = e.target.id * 1;
				var id =$li[0].id * 1;
				if (id > -1){
					self.$val.val(data.list[+id]);
				} else {
					self.$val.val(typeof data.empty == 'boolean' ? '' : data.empty);
				}
				self.$list.hide();
				onSelect();
				self.$val.focus();
			});
			if (data.lines > 0) {
				self.$list.css({
					'max-height': (data.line_size * data.lines) + 'px'
				});
				self.$list.css({
					'overflow': data.scroll ? 'auto': 'hidden'
				});
			}
			self.$list
				.find('li:odd').addClass('odd').end()
				.find('li:even').addClass('even');
			if (list.length == 1 && list[0] == self.$val.val()) {
				self.$list.hide();
			} else {
				self.$list.show();
				self.$el.height((self.$list.height() + self.$list.position().top) - self.$el.position().top + 20);
			}

			self.$list.find('li').bind('mouseenter', function(e) {
				var li = self.$list.find('li');
				li.removeClass('select');
				$(e.target).addClass('select');
				si = li.toArray().indexOf(e.target);
			});
		} else {
			self.$list.hide();
		}
	}

	function __search(text) {
		if (data.trim)
			text = text.trim();
		if (text == '') {
			return [];
		}
		var reg = eval('/^'+(data.free?'.*':'')+'('+text+').*$/'+(data.register ? '' : 'i'));
		return data.list.filter(function(q) {
			return reg.test(q) ? q : undefined;
		});
	}
	function search(text ,not_null) {
		show_elements(__search(text), not_null);
	}
	if (!data.input) {
		this.$val.attr('maxlength', '0');
	} //else {
		$('*').bind('keyup', function(e) {
			if (self.$val[0] != $('*:focus')[0])
				self.$list.hide();
		});
		this.$val.bind('keyup change', function(e) {
			var li;
			function selItem(q) {
				q = q.substr(last_text.length);
				self.$val.val(last_text+q);
				self.$val[0].selectionStart = last_text.length;
				self.$val[0].selectionEnd = last_text.length + q.length;
			}
			switch (e.which) {
				case 13:
					if (self.$list.css('display') && self.$list.css('display') != 'none') {
						last_text = $(self.$list.find('li')[si]).text();
						si = -1;
						self.$list.hide();
						self.$val.val(last_text);
						onSelect();
					}
					break;
				case 27:
					self.$list.hide();
					break;
				case 38:
					if (self.$val.val().length == 0)
						//search(self.$val.val(), false);
						search('', false);
					li = self.$list.find('li');
					if (si > -1)
						$(li[si]).removeClass('select');
					selItem($(li[si > 0 ? --si : si = (data.scroll ? li.length - 1 : data.lines - 1)]).addClass('select').text());
					if (data.scroll && si < 1 + self.$list[0].scrollTop / data.line_size)
						//self.$list[0].scrollTop = (si + 3) * data.line_size;
						self.$list[0].scrollTop -= data.line_size;
					if (si == li.length - 1)
						self.$list[0].scrollTop = data.scroll ? self.$list[0].scrollHeight : 0;
					break;
				case 40:
					if (data.free || !data.search || self.$val.val().length == 0)
						search('', false);
						//search(self.$val.val(), false);
					li = self.$list.find('li');
					if (si > -1)
						$(li[si]).removeClass('select');
					selItem($(li[si < (data.scroll ? li.length - 1 : data.lines - 1) ? ++si : si = 0]).addClass('select').text());
					if (data.scroll && si > (data.lines - 2) + self.$list[0].scrollTop / data.line_size)
						self.$list[0].scrollTop = (si - 3) * data.line_size;
					if (si == 0)
						self.$list[0].scrollTop = 0;
					break;
				default:
					var text = self.$val.val();
					last_text = text;
					if (text.length > 0) {
						search(text);
					} else show_elements([]);
					self.$list.find('#-1').remove();
					si = -1;
			}
		});
	//}

	if (typeof data.val == 'string' && data.val.length > 0) {
		this.$val.attr('value', data.val);
	}
	if (typeof data.holder == 'string' && data.holder.length > 0) {
		this.$val.attr('placeholder', data.holder);
	}

	if (data.classes) {
		function addClasses($el, classes) {
			if (typeof classes == 'string')
				classes = [classes];
			for (var i = 0, l = classes.length; i < l; i++)
				$el.addClass(classes[i]);
		}
		if (data.classes.parent) addClasses(this.$el, data.classes.parent);
		if (data.classes.input) addClasses(this.$val, data.classes.input);
		if (data.classes.list) addClasses(this.$list, data.classes.list);
	}
	if (data.ids) {
		function getIds(attr) {
			return attr instanceof Array ? attr.join(' ') : attr
		}
		if (data.ids.parent) this.$el.attr('id', getIds(data.ids.parent));
		if (data.ids.input) this.$val.attr('id', getIds(data.ids.input));
		if (data.ids.list) this.$list.attr('id', getIds(data.ids.list));
	}
	this.$selector.css('height', '100%');

	this.$val.bind('mouseenter', function() {
		self.$selector.show();
	});
	this.$val.bind('mouseleave', function() {
		self.$selector.hide();
	});
	function f() {
		if (data.input && data.search && !data.show_all)
			search(self.$val.val());
		else
			show_elements(data.list);
	}
	this.$val.bind('click', function() {
		if (self.$list.css('display') == 'none')
			f();
		else
			self.$list.hide();
	});
	this.$selector.bind('click', function() {
		if (data.show_all)
			show_elements(data.list);
		else
			f();
		self.$val.focus();
	});

	this.$el.addClass('comboId:'+id);
	this.$el.addClass('obj-combo');

	this.$cnt
		.append(this.$val)
		.append(this.$selector);
	this.$el
		.append(this.$cnt)
		.append(this.$list);

	if (data.render)
		if ([undefined, null].indexOf(this.$parent) == -1)
			this.$parent.append(this.$el);
		else
			throw('By create combo need set parent selector or jQuery object');

	$(window).click(function(e) {
		var $e = $(e.target).parents('.obj-combo');
		var cls = $e.attr('class');
		//if (!cls || cls.split(/\s/g).indexOf)
		if ($e[0] !== self.$el[0])
			self.$list.hide();
		/*if (!cls || cls.split(/\s/g).indexOf('obj-combo') == -1)
			self.$list.hide();*/
	});

	this.list = function(arr) {
		if (arr instanceof Array)
			data.list = arr;
	};

	this.render = function() {
		return this.$el;
	};

	this.select = function(val) {
		if (typeof val == 'string') {
			if (data.list.indexOf(val) != -1)
				self.$val.val(val);
			else
				throw 'List hasn\'t this element';
		} else if (typeof val == 'number') {
			if (val > -1 && val < data.list.length)
				self.$val.val(data.list[val]);
			else
				throw 'Index overflow of list';
		} else {
			throw 'Incorrect data';
		}
		last_text = self.$val.val();
	};

	this.select_default = function() {
		if (data.empty != false) {
			self.$val.val(typeof data.empty == 'boolean' ? '' : data.empty);
		} else {
			self.$val.val('');
		}
		last_text = self.$val.val();
	};

	this.valid = function() {
		var text = self.$val.val();
		if (data.trim)
			text = text.trim();
		var reg = eval('/^('+text+')$/'+(data.register ? '' : 'i'));
		return data.list.filter(function(q) {
			return reg.test(q) ? q : undefined;
		}).length > 0 ? true : (typeof data.empty == 'boolean' ? text == '' : text == data.empty || text == data.empty_val);
	};

	this.get = function(arg) {
		if (arg) {
			return data[arg];
		}
		return;
	};

	this.val = function(val) {
		if (val !== undefined) {
			val = __search(val)[0];
			if (val !== undefined)
				self.$val.val(val);
		} else {
			var text = last_text; //self.$val.val();
			val = this.valid() ? __search(text)[0] : undefined;
			if (val === undefined && (text == (typeof data.empty == 'boolean' ? '' : data.empty) || text == data.empty_val))
				val = data.empty_val;
		}
		return val;
	};
	this.__val = function() {
		return last_text;
	};
	this.index = function() {
		var val = __search(self.$val.val())[0];
		return data.list.indexOf(val);
	};
	//this.$el.val = this.val;

	function onSelect() {
		last_text = self.$val.val();
		if (self.onSelect) {
			self.onSelect.call(self, self.$val);
		}
	}
	this.onSelect = function() {};
};