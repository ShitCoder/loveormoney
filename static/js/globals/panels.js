
var CAuthorisationPanel = Backbone.View.extend({
	render: function() {
		var el = $('#body').find('#authorisation_panel');
		this.$el = el;
		return el.length ? el : this.template('authorisation_panel', this);
	}
});

var CLoginPanel = Backbone.View.extend({
	initialize: function() {
		this.login_popup = new CAuthorisationPanel();
		this.render();
		if (App.router.uri == '/regulations/')
			this.select(this.$el.find('#regulations'));
	},

	select: function(input) {
		var $e = typeof input == 'function' ? input() : input;
		this.$el.find('#navi_panel > button').removeClass('select');
		if ($e !== undefined)
			$e.addClass('select');
	},

	events: {
		'click #login': 'open_auth',
		'click global #shadow': 'close',
		'click global #authorisation_panel_close': 'close',
		'click #regulations': 'regulations'
	},

	open: function() {
		$('body').append('<div id="shadow"></div>');
		this.select(this.$('#login'));
		this.login_popup.$el.show();
		if (uLogin) {try{uLogin.init();}catch(e){}}
	},

	close: function() {
		$('#shadow').remove();
		this.select(undefined);
		this.login_popup.$el.hide();
	},

	open_auth: function() {
		if (this.login_popup.$el.css('display') == 'none') {
			this.open();
		} else {
			this.close();
		}
	},

	regulations: function() {
		this.close();
		App.router.redirect('/regulations/');
	},

	render: function() {
		return this.template('login_panel', this);
	}
});

var CUserPanel = Backbone.View.extend({
	initialize: function() {
		App.user.purse.bind('change_sum', this.on_change_sum, this);
		App.user.bind("change", this.on_change_user, this);
	},

	select: function(input) {
		var $e = typeof input == 'function' ? input() : input;
		this.$el.find('#navi_panel > button').removeClass('select');
		if ($e !== undefined)
			$e.addClass('select');
	},

	on_change_user: function(user) {
		this.$('#win_indicator')[user.get('is_win') ? 'show': 'hide']();
	},

	on_change_sum: function(purse) {
		this.$('#btn3').text(this.$('#btn3').text().replace(/\d+/, purse.available()));
	},

	render: function() {
		this.template('user_panel', this);
		var self = this;
		this.select(function() {
			var urls = {
				'/profile/': '#open_profile',
				'/my_rates/': '#my_rates_btn',
				'/my_bill/': '#my_money'
			};
			var url = urls[App.router.uri];
			return url ? self.$el.find(url) : undefined;
		});
		$(window).resize(function() {
			self.wnd_resize();
		});
		/*$(window).scroll(function() {
			self.$el.css('opacity', (document.body.scrollTop > 10 ? '0.3' : '1'));
		});*/
		return this.$el;
	},

	activate: function() {
		if (App.page && App.page.lot) {
			this.div$place = this.$el.find('#lot-place');
			this.span$place = this.div$place.find('span');
			this.div$place.css('display', 'inline-block');
			this.span$place.text(App.page.lot.user.city);
		}
	},

	wnd_resize: function() {
		if (App.page)
			if (App.page.userPanel && App.page.userPanel.cid == this.cid) {
				var $lp = this.$('#lot-place');
				$lp.width(this.$el.width() - this.$('#navi_panel').width() - $lp.position().left);
				if (window.innerWidth < 400) {
					this.$('#my_money').text(App.user.purse.available() + ' chr.');
				} else {
					this.$('#my_money').text('Мои ставки: ' + App.user.purse.available() + ' chr.');
				}
			} else {
				$(window).unbind('resize', this.wnd_resize);
			}
	}
});

var CMsg = Backbone.View.extend({
	default_life_time: 2,
	time_interval_by_one_type_msg: 10,

	initialize: function() {
		$('#body').append(this.template('message-bar', this));
		this.msg_stack = new Array();
		this.msg_types = {};
	},

	open_msg: function(text) {
		this.$el.html(text.replace(/\n/g, '<br>'));
		this.$el.animate({
			top: 0
		}, 'slow');
		this.$el.removeClass('mb-hide');
	},
	close_msg: function() {
		var self = this;
		this.$el.animate({
			top: -this.$el.height()-2
		}, 'slow', function() {
			self.$el.addClass('mb-hide');
			if (self.msg_stack.length > 0) {
				var data = self.msg_stack[0];
				self.msg_stack = self.msg_stack.slice(1);
				self.show(data[0], data[1]);
			}
		});
	},

	show: function(text, life_time) {
		if (typeof text == 'string' && text.length > 0) {

			if (this.$el.hasClass('mb-hide')) {
				this.open_msg(text);
				if (typeof life_time != 'number')
					life_time = this.default_life_time;
				if (typeof life_time == 'number' && life_time > 0) {
					var self = this;
					var t = setTimeout(function() {
						clearTimeout(t);
						self.close_msg();
					}, life_time * 1000);
				}
			} else {
				if (!this.msg_types[text]|| (this.msg_types[text] && new Date().getTime() > this.msg_types[text] + this.time_interval_by_one_type_msg * 1000)) {
					this.msg_stack.push([text, life_time]);
				}
			}
			this.msg_types[text] = new Date().getTime();
		}
	}
});