
var CChat = Backbone.View.extend({
	views: null,
	timeout: 5,
	is_active: false,

	initialize: function(parent_page, lot, user, winner) {
		this.render();
		var $result_btn = this.$('.result');
		var $result_label = this.$('#text');

		if (lot === undefined)
			return;

		if (
				((user === undefined || winner === undefined) && lot === undefined) ||
				(lot !== undefined && (lot.user === undefined || lot.winner === undefined))
			) return;

		this.lot = lot;
		this.user = user || this.lot.user;
		this.winner = winner || this.lot.winner;

		if (App.user.id == this.user.id) {
			$result_label
				.text('Свидание не состоялось');
			$result_btn
				.attr('id', 'no-miss');
		} else if (App.user.id == this.winner.id) {
			$result_label
				.text('Свидание состоялось');
			$result_btn
				.attr('id', 'miss');
		}

		this.messages = new CChatCollection();
		this.messages.data = this.lot;
		this.messages.display = this;
		this.messages.upload();

		this.views = new Array();

		var self = this;
		function autoupdate() {
			var t = setTimeout(function() {
				if (App.page !== undefined && App.page.chat !== undefined && App.page.cid == parent_page.cid && self.is_active) {
					clearTimeout(t);
					self.messages.upload();
					autoupdate();
				}
			}, self.timeout * 1000);
		}
		autoupdate();
		this.is_active = true;
	},

	events: {
		'click #send': 'add_post',
		'click .result': 'conf'
	},

	add_post: function() {
		this.ajax_message_post();
	},

	ajax_message_post: function() {
		var self = this;
		jsonRequest({
			url: '/lots/message-post/',
			type: 'POST',
			data: {
				lot_id: this.lot.id,
				text: this.$('#message_text').val(),
				last_msg_id: this.messages.length > 0 ? this.messages.last().id : 0
			},
			success: function(data) {
				if (!data.active) {
					App.lots.remove(App.page.lot);
					if (App.page.lot.user.id == App.user.id)
						App.user.lot = undefined;
					App.page.reinitialize();
					return;
				}
				self.$('#message_text').val('');
				self.messages.update(data.upload);
			}
		})
	},

	conf: function() {
		var self = this;
		jsonRequest({
			url: '/lots/set-complete/',
			type: 'POST',
			data: {
				lot_id: this.lot.id
			},
			success: function() {
				self.remove();
				var lot = self.lot;
				App.lots.remove(lot);
				if (App.user.id == lot.user.id) {
					App.page.chat = undefined;
					App.user.lot = undefined;
					App.page.reinitialize();
				} else {
					App.router.redirect('/user/'+lot.user.id+'/');
				}
				App.router.page_build();
			}
		})
	},

	add: function(messages) {
		if (messages.length == 0)
			return;
		
		for (var i = 0; i < messages.length; i++)
			this.views.push(new CChatRowView(messages[i], this.$('#history_content')));
	},

	time: function(date) {
		var data = {
			parent: '#to-meet-timeleft span',
			date: date
		};
		this.date_timer(data);
	},
	date_timer: date_timer,

	render: function() {
		var t = setTimeout(function() {
			clearTimeout(t);
			App.router.page_build();
		}, 200);
		return this.template('chat', this);
	}

});

var CChatRowView = Backbone.View.extend({

	initialize: function(model, $parent) {
		this.data = model;
		$parent.append(this.render());

		this.data = {
			date: this.data.date,
			parent: this.$('#time-left'),
			line: true,
			nosecs: true
		};
		this.date_timer(this.data);
	},

	date_timer: date_timer,

	render: function() {
		return this.template('chat-row', this);
	}
});

var CChatRow = Backbone.Model.extend({

	initialize: function(data) {
		this.id = data.id;
		if (data.author === undefined)
			this.user = App.user;
		else {
			var users = App.users.query('first_name="'+data.author.first_name+'":first');
			if (users.length > 0)
				this.user = users[0];
			else if (App.user.first_name == data.author.first_name)
				this.user = App.user;
			else
				this.user = App.users.add(data.author).last();
		}
		this.text = data.text.replace(/\n/g, '<br>');
		this.date = new Date(data.date);
	}
});

var CChatCollection = CCollection.extend({
	model: CChatRow,
	data: null,

	update: function(rows) {
		if (rows.length > 0) {
			var sf = this.length;
			this.add(rows);
		} else
			return;

		if (this.display !== undefined || this.display !== null) {
			this.show_some(sf);
		}
	},

	upload: function() {
		var self = this;
		jsonRequest({
			url: '/lots/messages-get/',
			type: 'GET',
			data: {
				lot_id: this.data.id,
				last_message_id: this.length > 0 ? this.last().id : 0
			},
			success: function(data) {
				if (App.page.chat === undefined) {
					return;
				}
				if (!data.active) {
					App.page.chat.is_active = false;
					App.page.chat = undefined;
					if (App.page.lot === undefined) {
						if (App.lots.query('id='+App.user.lot.id+':[0]'))
							App.lots.remove(App.user.lot);
						App.user.lot = undefined;
						App.page.reinitialize();
					} else if (App.page.lot !== undefined && App.page.lot.user.id !== App.user.id) {
						App.router.redirect('/user/'+App.page.lot.user.id+'/');
						if (App.lots.query('id='+App.user.lot.id+':[0]'))
							App.lots.remove(App.page.lot);
					} else {
						throw 'You are hacker'
						return;
					}
					/*if (App.page.lot && App.page.lot.user && App.page.lot.user.id == App.user.id) {
						App.lots.remove(App.page.lot);
						//App.lots.remove(App.user.lot);
						//App.user.lot = undefined;
					} else if (App.page.lot === undefined && App.page.uri == '/profile/' && App.user.lot !== undefined) {
						App.user.lot = undefined
					}*/
					return;
				}

				if (data.upload) {
					self.update(data.upload);
				}

				if (data.chat && self.display) {
						self.display.time(new Date(data.chat.date));
				}
			}
		})
	}
});
