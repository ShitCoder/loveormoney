var UloginWidget = Backbone.View.extend({
	events: {
	},
	initialize: function(conf) {
		this.providers = _.filter(SETTINGS.SOCIAL_PROVIDERS, function(el) {
			return !conf.exproviders || _.indexOf(conf.exproviders, el) == -1;
		});

		this.providers += "";
		this.callback = conf.callback;

		this.display = conf.display || "small";
		this.redirect_url = conf.redirect_url || SETTINGS.SITE_URL;
		this.id = "uLogin" || conf.id;
	},

	render: function() {
		return this.template("ulogin_widget", this);
	}
});
