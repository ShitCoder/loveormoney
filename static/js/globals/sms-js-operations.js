

var CNumberConfirmUpgrade = Backbone.View.extend({
	$numfield: null,

	initialize: function($nf) {
		if ($nf instanceof jQuery)
			this.$numfield = $nf;
		if (!App.user.number_confirm)
			this.$numfield.removeAttr('readonly');
		this.getConfirmData();
	},

	events: {
		'click #get-code': 'get_code',
		'click #reget-code': 'get_code',
		'click #try-code': 'try_code',
		'click #get-change-code': 'change_number'
	},

	getConfirmData: function() {
		var self = this;
		jsonRequest({
			url: '/sms/get-data/',
			type: 'GET',
			async: false,
			success: function(data) {
				if (data.__confirm_state__) {
					App.user.number_confirm = data.__confirm_state__;
					if (App.user.number_confirm)
						self.$numfield.attr('readonly', 'readonly');
				}
			}
		});
		/*var self = this;
		jsonRequest({
			url: '/sms/get-data/',
			type: 'GET',
			async: false,
			success: function(data) {
				if (data.__confirm_state__) {
					self.$numfield
						.val(data.number)
						.attr('readonly', 'readonly');
				} else {
					App.user.number_confirm = true;
					self.$numfield.attr('readonly', 'readonly');
					self.render();
				}
			}
		})*/
	},

	change_number: function() {
		var self = this;
		jsonRequest({
			url: '/sms/pre--change-number/',
			method: 'GET',
			success: function(data) {
				if (data.change_number__time !== undefined) {
					date = new Date(data.change_number__time) - new Date() + DEFFERENCE_TIME;
					if (date > 0) {
						text = 'Менять номер телефона можно но чаще, чем раз в сутки.\n\
								  Изменить номер Вы сможете ';
						date /= 60000;
						if (date > 1) {
							text += 'через %m %t';
							t = 'мин.';
							if (date > 60) {
								date /= 60;
								t = 'ч.'
							}
							text = text.replace('%m', Math.floor(date)).replace('%t', t);
						} else {
							text += 'меньше, чем через минуту.';
						}
						App.msg.show(text, 5);
					}
				} else {
					self.$numfield.removeAttr('readonly');
					self.render();
				}
			}
		});
	},

	get_code: function(){
		var self = this;
		self.$numfield.attr('readonly', 'readonly');
		if (App.user.telephone_number == '' ||
			App.user.telephone_number != self.$numfield.val() ||
			(App.user.telephone_number == self.$numfield.val() && !App.user.number_confirm))
			jsonRequest({
				url: '/sms/get-code/',
				type: 'GET',
				data: {
					number: this.$numfield.val()
				},
				success: function(data) {
					if (data.data_error) {
						App.msg.show('Отсутствует номер');
						return self.$numfield.removeAttr('readonly');
					}
					self.render();
					if (data.next_send !== undefined) {
						date = new Date(data.next_send) - new Date() + DEFFERENCE_TIME;
						text = 'Колличество повторных отправок ограничено.\nДо следующей попытки ';
						if (date > 0) {
							date /= 1000;
							if (date > 60.0) {
								date /= 60;
								text += Math.floor(date) + ' мин.';
							} else {
								text += 'меньше минуты'
							}
							App.msg.show(text, 5);
						}
					}
					if (!data.___status && App.msg !== undefined && App.msg.show !== undefined)
						App.msg.show('Вы не изменили номер телефона');
						self.$numfield.removeAttr('readonly');
				}
			});
		else
			if (App.msg !== undefined && App.msg.show !== undefined)
				App.msg.show('Вы не изменили номер телефона')
	},

	try_code: function() {
		var self = this;
		jsonRequest({
			url: '/sms/try-code/',
			type: 'POST',
			data: {
				code: this.$('#code').val(),
				number: this.$numfield.val()
			},
			success: function(data) {
				if (data.state == 'complete') {
					App.user.telephone_number = data.number;
					App.user.number_confirm = true;
					self.$numfield.attr('readonly', 'readonly');
					self.render();
				} else {
					if (App.msg !== undefined && App.msg.show !== undefined) {
						text = 'Обновите страницу';
						if (data.next_try == 0) {
							text = 'Неверный код, попробуйте снова';
						} else {
							date = new Date(data.next_try) - new Date() + DEFFERENCE_TIME;
							date /= 60000;
							text = 'Вы истратили 3 попытки.<br>До следующих 3-х попыток ';
							if (date > 1.0)
								text += Math.floor(date) + ' мин.';
							else
								test += 'меньше минуты';
						}
						App.msg.show(text)
					}
				}
			}
		})
	},

	render: function() {
		if (!(this.$numfield instanceof jQuery))
			throw 'Incorrect data :: number field must be of jQuery inheritance'

		if (this.$el)
			this.$el.remove();

		//if (App.user.number_confirm)
		//	return;

		if (
				App.user.telephone_number != '' &&
				App.user.telephone_number == this.$numfield.val() &&
				this.$numfield.attr('readonly') !== undefined &&
				App.user.number_confirm
			)
			this.template('tl-confirm--change', this);
		else
			if (this.$numfield.attr('readonly') === undefined)
				this.template('tl-confirm--info', this);
			else
				this.template('tl-confirm--confirmation', this);

		this.$numfield.after(this.$el);
		this.delegateEvents();
	}
});