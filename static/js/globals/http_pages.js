var CHttpPage = function(type) {
	var pp = (App.user === null || !App.user.is_authenticated() ? CLoginPage : CBaseUserPage); //parent page
	var r = pp.extend({
		error: {
			404: 'Error<br>404'
		},

		initialize: function(type) {
			this.constructor.__super__.initialize(this);
			this.template('HttpError', this);

			this.$el.html(this.error[type]);
			$('#page_content').append(this.$el);
		}
	});
	return new r(type);
};