

var CMainTop = Backbone.View.extend({

});

function timeleft(self) {
	//self это data
	if (!self.date || !(self.date instanceof Date))
		try {
			self.date = new Date(self.date);
		} catch (e) {
			return;
		}

	if (!self.line)
		self.line = false;
	//DEFFERENCE_TIME
	var delta = (self.line?new Date().getTime() - self.date.getTime() - DEFFERENCE_TIME:self.date.getTime() - new Date().getTime() + DEFFERENCE_TIME);

	if (delta < 0)
		if (self.line) {
			delta = 1000;
		} else if (self.nosecs) {
				delta = 10000;
		}

	var fs = delta / 1000;
	var fm = fs / 60;
	var fh = fm / 60;
	var d = Math.floor(fh / 24);
	var h = Math.floor(fh - d * 24);
	var m = Math.floor(fm - Math.floor(fh) * 60);
	var s = Math.floor(fs - Math.floor(fm) * 60);
	function getTimeName(time, root, suffixes) {
		//suffixes [x*1, x*2-4, x*5-9]
		if (time % 10 == 1 && (time > 20 || time == 1))
			return time + ' ' + (root=='дн'?'день':root + suffixes[0]);
		else if ([2, 3, 4].indexOf(time % 10) != -1 && (time < 10 || time > 20))
			return time + ' ' + root + suffixes[1];
		else
			return time + ' ' + root + suffixes[2];
	}
	var left = '';
	if (d > 0) {
		left = getTimeName(d, 'дн', ['', 'я', 'ей']);
		this.time_interval_type = (m > 1 ? 0: 1);
	} else if (h > 0) {
		left = getTimeName(h, 'час', ['', 'а', 'ов']);
		this.time_interval_type = (m > 1 ? 1: 2);
	} else if (m > 0) {
		left = getTimeName(m, 'минут', [(self.line || self.over?'у':'а'), 'ы', '']);
		this.time_interval_type = (m > 1 ? 2: 3);
	} else if (s >= 0) {
		if (!self.nosecs)
			left = getTimeName(s, 'секунд', [(self.line || self.over?'у':'а'), 'ы', '']);
		else
			left = 'меньше минуты';
		this.time_interval_type = 3;
	}

	if (self.nosecs && delta < 0)
		left = 'меньше минуты';

	if (!self.line && self.date.getTime() + DEFFERENCE_TIME < new Date().getTime())
		left = undefined;
	
	return left;
}


function date_timer(data) {
	/*
		data должна сожержать:
			parent - селектор объекта, в который нужно вставить дату
			line - true: времени прошло; false или undefined: времени осталось
			date - дата и время
	 		sefl - передаётся, только если эта функция не подключена в объекте, в котором исспользуется
	 				self должен содержать parent_cid - cid страницы, к которой привязан объект таймера
	 				наче будет взят при создании, что может повлеч ошибки

		self = объект, для которого идёт расчёт

		функция timeleft должна быть встроена и иметть имя timeleft, в противном случае встроится автоматически
	*/
	if (!data || (!this && !data.self))
		return;

	var self = this || data.self;
	delete data.self;

	if (!self.timeleft)
		self.timeleft = timeleft;

	var left = [3600, 600, 30, 0.5]; //at seconds
	var t = null;
	function q(__left) {
		t = setTimeout(function() {
			if (self.page_cid === undefined && App.page !== undefined)
					self.page_cid = App.page.cid;

			if (self.page_cid == App.page.cid) {
				clearTimeout(t);
				var tt = self.timeleft(data);
				self.$(data.parent).html(tt || '0');
				if (tt === undefined) {
					if (self.end_function)
						self.end_function();
				} else {
						q(left[self.time_interval_type]);
				}
			}
		}, __left * 1000);
	}

	function __q() {
		var __t = setTimeout(function() {
			clearTimeout(__t);
			if (self.page_cid === undefined && App.page !== undefined)
					self.page_cid = App.page.cid;
			if (self.page_cid === undefined)
				__q();
		}, 10);
	}
	__q();
	q(data.first_timeout || 0.5);
}


var CCollection = Backbone.Collection.extend({
	display: null,

	comparator: function(a, b) {
		return a.id > b.id;
	},

	//show: использюя вьювер-дисплэй, передавая ему данные, отображает нужные лоты
	show: function(lots) {
		if (!this.display)
			return;
		if (typeof lots == 'string')
			lots = this.query(lots);
		if (!lots )
			lots = this.models;
		if (lots.length == 0)
			return;

		this.display.add(lots);
	},

	show_some: function(from, to) {
		if (typeof from == 'string') {
			this.show(this.query(from));
		} else {
			this.show(this.interval(from, to))
		}
	},

	all: function() {
		return this.models;
	},

	interval: function (from, to) {
		return this.models.slice(from || 0, to || this.models.length);
	}

});

