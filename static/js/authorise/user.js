
var CUser = Backbone.Model.extend({
	initialize: function(data) {
		this.purse = undefined;

		this.constructor.__super__.initialize.apply(this, arguments);
		for (var i in data)
			if (i)
				this[i] = data[i];
	},

	money: function() {
		function q(val) {
			val += '';
			var str = '';
			for (var i = 0; i < val.length; i++)
				str = val[val.length - i - 1] + (i != 0 && i % 3 == 0 ? '\' ' : '') + str;
			return str;
		}
		var result = {};
		result.sum = q(this.purse.sum);
		result.available = q(this.purse.available());
		result.involved = q( this.purse.sum - this.purse.available());
		return result;
	},

	is_authenticated: function() {
		return true;
	}
});

CUser.upload = function(user_id) {
	var id = -1;
	jsonRequest({
		url: '/auth/get-user/',
		type: 'GET',
		data: {
			user_id: user_id
		},
		async: false,
		no_msg: true,
		success: function(data) {
			if (data.___status) {
				var user = App.users.query('id='+data.user.id+':first')[0];
				if (user === undefined) {
					user = new CUser(data.user);
					App.users.add(user);
				}
				if (data.lot && data.lot[0] && new Date(data.lot[0].date).getTime() > new Date().getTime()) {
					var lot = new CLot(data.lot[0]);
					App.lots.add(lot);
					lot.user = user;
				}
				id = user.id;
			}
		}
	});
	return id;
};

var State = Backbone.Model.extend({
	defaults:{
		search: {
			gender: null,
			city: null
		}
	},

	initialize: function() {
		this.constructor.__super__.initialize.apply(this, arguments.length == 0? this.defaults: arguments);
	},

	set: function() {
		if (arguments[0] == 'user' && this.get('search').gender == null && this.get('search').city == null) {
			var user = arguments[1];
			this.set('search', {gender: user.gender == 'male' ? 'female': 'male', city: user.city});
			return;
		}
		this.constructor.__super__.set.apply(this, arguments);
	}
})

var CAnonymousUser = Backbone.Model.extend({
	is_authenticated: function() {
	   return false;
	}
});


var CUserCollection = CCollection.extend({
	model: CUser,

	initialize: function(data_arr) {
		this.add(data_arr);
	}
});
