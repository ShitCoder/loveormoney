var RESPONSE_BACKENDS = [];

function jsonRequest(_data) {
	var data = _.clone(_data);

	if (!data.data) {
		data.data = {};
	} else {
		data.data = _.clone(_data.data);
	}

	data.data.csrfmiddlewaretoken = CSRF_TOKEN;
	data.dataType = 'json';
	var s = data.success;
	data.success = function(data){
		if (!data.___status) {
			throw data.error;
			App.msg.show('У Вас нет доступа');
		}
		if (data.purse) {
			App.user.purse.sum = data.purse.sum;
			App.user.purse.involved = data.purse.involved;
			App.user.purse.on_change_sum();
		}

		s(data);

		var hash = data['___hash'];
		hash = hash || {};
		for (var i = 0; i < RESPONSE_BACKENDS.length; i++) {
			window[RESPONSE_BACKENDS[i]](hash);
		}
	};
	var error = function() {};
	if (data.error)
		error = data.error;

	var msg = true;
	if (_data.no_msg)
		msg = false;
	delete _data.no_msg;

	data.error = function(idata) {
		if (msg)
			App.msg.show('Обновите страницу');
		error(idata);
		App.router.page_build();
	};
	$.ajax(data);
}

function _default(field, val) {
	return field? field: val;
}