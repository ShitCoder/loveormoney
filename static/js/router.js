var CRouter = Backbone.Router.extend({
	routes: {
		'': 'index',
		'/': 'index',
		"_=_": "plug_facebook",
		'profile/': 'profile',
		'user/:id/': 'user_profile',
		'lot/:id/': 'lot_view',
		'my_bill/': 'my_bill',

		'logout/': 'logout',
		'my_rates/': 'my_rates',
		
		'*path': 'error404'
	},

	initialize: function() {
		$(window).resize(this.page_build);
		$('body > *').bind('click', this.page_build);
	},

	plug_facebook: function() {
		this.redirect('/');
	},

	index: function() {
		if (App.page)
			App.page.remove();

		if (App.user.is_authenticated()) {
			App.page = new CMainPage();
		} else {
			App.page = new CLoginPage();
		}
		this.page_build();
	},

	profile: function() {
		if (App.page)
			App.page.remove();
		
		if (App.user.is_authenticated()) {
			App.page = new CProfile();
		} else {
			App.router.redirect('/');
		}
		this.page_build();
	},


	logout: function() {
		jsonRequest({
			url: '/auth/logout/',
			type: 'POST',
			success: function() {
				App.user = new CAnonymousUser();
				App.router.redirect('/');
			}
		})
	},

	lot_view: function(id) {
		if (App.page)
			App.page.remove();

		//if (App.user.is_authenticated()) {
		App.page = new CLotViewPage(+id);
		if (App.user.is_authenticated()) {
			App.page.userPanel.activate();
		} /*else {
			App.router.redirect('/');
		}*/
		this.page_build();
	},

	user_profile: function(id) {
		if (App.page)
			App.page.remove();

		if (App.user.is_authenticated()) {
			App.page = new CUserPage(id);
		} else {
			App.router.redirect('/');
		}
		this.page_build();
	},

	my_rates: function() {
		if (App.page)
			App.page.remove();
		
		if (App.user.is_authenticated()) {
			App.page = new CMyRatesPage();
		} else {
			App.router.redirect('/');
		}
		this.page_build();
	},

	my_bill: function(){
		if (App.page)
		  App.page.remove();

		if (App.user.is_authenticated()) {
			App.page = new CMyBillPage();
		} else {
			this.redirect('/');
		}
		this.page_build();
	},



	error404: function() {
		if (App.page)
			App.page.remove();

		App.page = CHttpPage(404);
		this.page_build();
	},

	page_build: function() {
		var cnt_top = $('#body').position().top;
		var cnt_height = $('#page_content').height();
		var wnd_height = window.innerHeight;
		var cpr_height = $('#copyright').height() + $('#copyright > div').height();

		if (cnt_top + cnt_height < wnd_height) {
			$('#space').height((wnd_height - cpr_height)/*bottom of space*/ - (cnt_top + cnt_height)/*top of space*/);
		} else {
			$('#space').height(0);
		}

		if (window.innerWidth >= 320) {
			$('body').css('overflow-x', 'hidden');
		} else {
			$('body').css('overflow-x', 'auto');
		}
	}
});
