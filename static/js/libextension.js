 Backbone.Router.prototype.redirect = function(fragment) {
	this.from = this.uri;
	this.uri = fragment;
	this.navigate(fragment, {trigger:true});
};

Backbone.Router.prototype.uri = window.location.pathname;


Backbone.View.prototype.template = function(selector, self, unuse_$el){
	if (!Templates[selector]) {
		throw "Templater ERROR: template \"" + selector + "\" is not define in Templates";
	}

	var c = _.templateSettings;
	var noMatch = /.^/;
	var code = 'var __r, __s={}, __s_len = 0, __p=[],print=function(){__p.push.apply(__p,arguments);};' +
		'with(obj||{}){__p.push(\'' +
		Templates[selector]
			.replace(/\\/g, '\\\\')
			.replace(/'/g, "\\'")
			.replace(c.escape || noMatch, function(match, code) {
				         return "',_.escape(" + code.replace(/\\'/g, "'") + "),'";
			         })
			.replace(/\r/g, '\\r')
			.replace(/\n/g, '\\n')
			.replace(/\t/g, '\\t')
			.replace(c.interpolate || noMatch, function(match, code) {
				         return "');\n "+
					         "__r = " + code.replace(/\\'/g, "'") + ";\n" +
					         "\nif(typeof(__r) == 'object' && __r.constructor == $) {\n" +
					         "__s[__s_len]= __r;\n"+
					         "__p.push('<div class=\"__t__\" id=\"' + __s_len + '\"></div>');\n"+
					         "__s_len++;\n"+
					         "}"+
					         "else {\n__p.push(__r)\n;}\n __p.push('";
			         })
			.replace(c.evaluate || noMatch, function(match, code) {
				         return "');" +
					         code.replace(/\\'/g, "'")
						         .replace(/[\r\n\t]/g, ' ')
						         .replace(/\\\\/g, '\\') + ";__p.push('";
			         })
		+
		"');} __r = $(__p.join(''));\n var __es = __r.find('.__t__'); " +
		"for (var i = 0; i < __es.length; i++){\n $(__es[i]).replaceWith(__s[__es[i].id]); }" +
		"__es = __r.filter('.__t__');" +
		"for (var i = 0; i < __es.length; i++){\n $(__es[i]).replaceWith(__s[__es[i].id]); }" +
		"return __r";

	var func = new Function('obj', code);
	var $el = func({obj: this, settings: SETTINGS, app: App});
	this.delegateEvents();
	if (!unuse_$el)
		this.$el = $el;
	return $el;
};

Backbone.View.prototype.reinitialize = function() {
	this.remove();
	this.initialize();
	this.delegateEvents();
}

jQuery.prototype.center = function(type) { //type: 0 | undefined ~ 1 + 2, 1 - horizontal, 2 - vertical
	var d = {
		'position': 'fixed'
	};
	if (!type)
		type = 0;
	with(this) {
		if (type == 0 || type == 1)
			d.left = window.innerWidth / 2 - width() / 2;
		if (type == 0 || type == 2)
			d.top = window.innerHeight / 2 - height() / 2;
		css(d);
	}
	return this;
};
jQuery.prototype.centered = function(type) {
	var $self = this;
	$self.center(type);
	$(window).resize(function() {
		$self.center(type);
	});
};

Array.prototype.top = function() {
    return this[this.length - 1];
};

Backbone.View.prototype.delegateEvents = function(events) {
	var getValue = function(object, prop) {
		if (!(object && object[prop])) return null;
		return _.isFunction(object[prop]) ? object[prop]() : object[prop];
	};

	var eventSplitter = /^(\S+)\s*(.*)$/;
	var eventGlobalSplitter = /^(\S+)\s*(global)\s*(.*)/;

	if (!(events || (events = getValue(this, 'events')))) return;
	this.undelegateEvents();
	for (var key in events) {
		var method = events[key];
		if (!_.isFunction(method)) method = this[events[key]];
		if (!method) throw new Error('Event "' + events[key] + '" does not exist');

		var global = eventGlobalSplitter.test(key) && key.match(eventGlobalSplitter)[2] == 'global';
		var el = global? $('body') : this.$el;

		var match = key.match(global? eventGlobalSplitter : eventSplitter);
		var eventName = match[1], selector = match[global? 3 : 2];
		method = _.bind(method, this);
		eventName += '.delegateEvents' + this.cid;

		if (selector === '') {
			el.bind(eventName, method);
		} else {
			el.delegate(selector, eventName, method);
		}
	}
};

var Module = { //Набор функций для query2 (нынче query)
	//version 2.1

	//parse дробит параметр на 2 части: 1 - поле; 2 - тип (если указан)
	///<field> as <type> - шаблон записи, возможно только одно поле
	///доступные типы описаны ниже
	parse: function(field) {
		var r = field.replace(/\s+as\s+/, '~').split('~');
		return [r[0], r[1]];
	},

	/////Типы (для типизации параметров выборки query)////

	//num - приведение к числу
	num: function(val) {
		return /^\d+((\.{1}|,\,{1})\d+)?$/.test(val) ? +val : undefined;
	},

	//int - приведение к целому числу, путём взятия целой части
	int: function(val) {
		return this.num(val) !== undefined ? Math.floor(val) : undefined;
	},

	//'int of round' - приведение к целому числу, путём округления
	'int of round': function(val) {
		return this.num(val) !== undefined ? Math.round(val) : undefined;
	},

	//float - взятие дробной части числа
	float: function(val) {
		return this.num(val) !== undefined ? val - this.int(val) : undefined;
	},

	//str - приведение к строке
	str: function(val) {
		return val.toString();
	},

	//date - переводит дату в миллисекунды для сравнения в функциях выбора query
	date: function(val) {
		if (val instanceof Date)
			val = val.getTime();
		return val;
	},
	/////--end--/////

	/////Функции выборки query/////
	max: function(field, arr) {
		var last_max;
		var p = this.parse(field);
		field = p[0];
		var func = p[1] ? ['Module["'+p[1]+'"](', ')'] : ['', ''];
		for (var i = 0; i < arr.length; i++)
			if (last_max === undefined || eval((func[0] + 'arr['+i+'].'+field+func[1]+' > '+func[0]+'arr['+last_max+'].'+field+func[1])))
				last_max = i;
		return [arr[last_max]];
	},
	min: function(field, arr) {
		var last_min;
		var p = this.parse(field);
		field = p[0];
		var func = p[1] ? ['Module["'+p[1]+'"](', ')'] : ['', ''];
		for (var i = 0; i < arr.length; i++)
			if (last_min === undefined || eval((func[0] + 'arr['+i+'].'+field+func[1]+' < '+func[0]+'arr['+last_min+'].'+field+func[1])))
				last_min = i;
		return [arr[last_min]];
	}
	/////--end--/////
};

var ___query = function(array, query) {
	if (array.length == 0)
		return [];
	///version 2.2.2
	var self = this;
	//black "new in 2.2.2 version
	var stat = [false, -1, ''];
	var str_stack = new Array();

	query = query.replace('$', '\$');

	for (var p = 0; p < query.length; p++)
		switch (query[p]) {
			case '\\':
				if (['"', "'"].indexOf(query[p+1]) == -1)
					break;
			case '"':
			case "'":
				if (stat[0] && (!stat[2].test(query.substr(p-1, 2))))
					break;
				stat[0] = !stat[0];
				if (stat[0]) {
					stat[1]= p;
					if (query[p-1] == '\\')
						stat[2] = /\\(\'|\")/
					else
						stat[2] = /[^\\](\'|\")/
				} else {
					str_stack.push(query.substring(stat[1], p+1));
					query = query.substr(0, stat[1]) + '~$'+query.substr(p+1);
				}
				break;
			//case ':': if (stat) query = query.substr(0, p+1) + '$' + query.substr(p+1); break;

		}
	//end block

	var parse = query.split(/\:(?!\$)/);
	var success_query = parse[0];
	/*var adv_condition = parse[1] || 'first-last';
	var from_to = adv_condition.search('-') > -1 ? adv_condition.match(/\d+|(first)|(last)/g) : undefined;
	var index = !from_to ? adv_condition.match(/\d+|(first)|(last)/)[0] : undefined;
	var module = parse[2] || undefined;*/

	var from_to = undefined; //интервал выборки по массиву (from-to)
	var index = undefined; //вернуть массив единственного значения, индекс указывается по результату (first|last|число)
	var module = undefined; //модуль - расширение query, выполняет функцию из Module
	var result_interval = undefined; //делает срез массива результата, границы не могут превосходить границы from_to ([from, to] | from, to)
	var module_is_first = undefined;


	for (var pi = 1; pi < parse.length; pi++) {
		if (parse[pi].length == 0)
			continue;

		if (parse[pi].search('-') > -1)
			from_to = parse[pi].match(/\d+|(first)|(last)/g);
		else if (parse[pi].search(',') > -1)
			result_interval = parse[pi].replace(/\[&\]/g, '').match(/\d+|(first)|(last)/g);
		else if (parse[pi].search(/\(/g) > -1)
			module = parse[pi];
		else
			index = parse[pi].match(/\[?\d+|(first)|(last)\]?/g, '')[0];


		if (module_is_first === undefined && (result_interval !== undefined || module !== undefined))
			module_is_first = (result_interval === undefined && module !== undefined);
	}

	if (from_to === undefined)
		from_to = ['first', 'last'];

	var norm = function(vario) {
		if (vario == 'first')
			vario = 0;
		else if (vario == 'last')
			vario = array.length - 1;
		else
			vario = +vario;
		return vario;
	};
	if (index)
		index = norm(index);
	if (from_to) {
		from_to[0] = norm(from_to[0]);
		from_to[1] = norm(from_to[1]);
	}
	if (result_interval) {
		result_interval[0] = norm(result_interval[0]);
		result_interval[1] = norm(result_interval[1]);
	}

	var result = new Array();

	success_query = success_query.replace('==', '~').replace(/[^!]=/g, '$&=').replace(/\~(?!\$)/g, '==')
									.replace('<==', '<=').replace('>==', '>=')
									.replace('&&', '~').replace(/\&|(~(?!\$))/g, '&&')
									.replace('||', '~').replace(/\||(~(?!\$))/g, '||');

	var condition_index = index !== undefined ? index <= this.length / 2 : false;
	var condition_fromto = from_to !== undefined ? from_to[0] <= from_to[1] : false;
	var iterator = function(i) {
		try {
			// version: 2.0.2
			//if (success_query == '' || eval(success_query.replace(/(\w|\d|\.)+\s*(?=\=|\>|\<)/g, 'self.models['+i+'].$&')))
			//
			// version 2.1.2
			// if (success_query == '' || eval(success_query.replace(/(\w|\d|\.)+\s*(?=\=|\>|\<)/g, 'self.models['+i+'].$&').replace(/(\=|\>|\<)(\w|\d\.)+/g, function() {return arguments[0][0]+'('+arguments[0].substr(1)+' || ' + '"'+arguments[0].substr(1)+'")'})))
			// version 2.2.2
			var q = success_query.replace(/([^!<=>"'\s])+\s*(?=\!|\=|\>|\<)/g, function() {
				return ['@', '%'].indexOf(arguments[0][0]) == -1 ? 'array['+i+'].'+arguments[0] : arguments[0];
			});
			q = q.replace(/(\@|\%)(me)(\@|\%)?/g, 'array['+i+']').replace('@', '').replace(/\%(index)\%?/g, i);
			for (var j = 0; j < str_stack.length; j++)
				q = q.replace(/\~\$/, str_stack[j]);
			if (success_query == '' || eval(q))
				//result.push(self.models[i]);
				result.push(array[i]);
		} catch (e) {
			throw('['+e+'] Ошибка запроса: данный путь не найден');
		}
	};

	var i = 0;
	if ((from_to !== undefined && condition_fromto) || (from_to === undefined && index !== undefined && condition_index)) {
		for (i = (from_to ? from_to[0] : 0); i < (from_to ? from_to[1] + 1 : array.length - 1); i++) {
			iterator(i);
			if (
					(index !== undefined && result.length - 1 == index) ||
					(result_interval !== undefined && !module_is_first && result.length == Math.max(result_interval[0], result_interval[1]))
			)
				break;
		}
	} else if ((from_to !== undefined && !condition_fromto) || (index !== undefined && !condition_index)){
		for (i = (from_to ? from_to[0] : this.length - 1); i > (from_to ? from_to[1] - 1 : -1); i--) {
			iterator(i);
			if (index !== undefined && result.length - 1 == index)
				break;
		}
	}

	var __result_interval = function(interval) {
		var from = interval[0];
		var to = interval[1];
		var vector = from <= to; //true - from left to right; false - (invert) from right to left
		var start = vector ? from : to;
		var end = vector ? to : from;
		if (from_to === undefined)
			from_to = [0, norm('last')];
		from = Math.min(from_to[0], from_to[1]);
		to = from == from_to[0] ? from_to[1] : from_to[0];
		start = Math.min(start, from);
		end = Math.min(end, to);
		var __result = new Array();
		if (vector)
			result = result.splice(start, end + 1);
		else {//Делает обратный порядок, если пиздец надо, но это цикл ="(
			for (i = end; i >= (to < 0 ? 0 : to); i++)
				__result.push(result[i]);
			result = __result;
		}
	};

	if (result_interval !== undefined && !module_is_first)
		__result_interval(result_interval);

	if (module && result.length > 0) {
		try {
			result = Module[module.match(/.*(?=\()/)](module.match(/\(.*(?=\))/)[0].substr(1), result);
		} catch (e) {
			console.log('No match some function');
		}
	}

	if (result_interval !== undefined && module_is_first)
		__result_interval(result_interval);

	return index ? (result === undefined ? result : [result[index]]) : result;
};

Backbone.Collection.prototype.query = function(filter_query) {
	return ___query(this.models, filter_query)
};

Backbone.Collection.prototype.last = function () {
	return this.models[this.models.length - 1];
};

Backbone.Collection.prototype.first = function () {
	return this.models[0];
};


_.array = function(array, func) {
	var res = [];

	for(var i = 0; i < array.length; i++) {
		res.push(func(array[i]));
	}
	return res;
};

Array.prototype.update = function(array) {
	this.push.apply(this, array);
	return this;
};