from django.db import models
from qiwi.qiwi_status_code import QIWI_STATUS_EXPOSED

class QIWIBill(models.Model):
	txn = models.CharField(max_length=256)
	amount = models.FloatField()
	user = models.CharField(max_length=10)
	comment = models.CharField(max_length=256)
	status = models.IntegerField(default=QIWI_STATUS_EXPOSED)

