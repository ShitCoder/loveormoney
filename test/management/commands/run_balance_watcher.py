from django.core.management.base import NoArgsCommand
from sms_service.sms_daemon import BalanceWatcher

class Command(NoArgsCommand):

	def handle_noargs(self, **options):
		server =  BalanceWatcher()
		server.run()