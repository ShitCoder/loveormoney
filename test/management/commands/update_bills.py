from django_qiwi.soap.client import Client
from django.core.management.base import  BaseCommand
from qiwi.models import Bill
import time
from qiwi.qiwi_status_code import QIWI_STATUS_PAID
from qiwi.backends import update_bill

class Command(BaseCommand):
	def handle(self, *args, **options):
		start = time.strptime(args[0], '%d.%m.%Y')
		c = Client()
		for bill in Bill.objects.filter(state!=QIWI_STATUS_PAID, date__gte=start):
			res = c.checkBill(bill.pk)
			update_bill(bill.pk, res['status'])



