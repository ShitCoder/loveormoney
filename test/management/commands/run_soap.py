#encoding:utf8
from django.core.management.base import NoArgsCommand
from django_qiwi.soap.server import runserver
from settings import SOAP_DEBUG
from soap_server import QiwiProvider
from helpers import send_email_admin

class Command(NoArgsCommand):

	def handle_noargs(self, **options):
		if not SOAP_DEBUG:
			try:
				runserver()
			except Exception, e:
				send_email_admin('SOAP SERVER PANIC SHUTDOWN')
		else:
			server = QiwiProvider()
			server.run()