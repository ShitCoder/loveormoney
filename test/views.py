#encoding=utf-8
#coding=utf-8
from django.shortcuts import render
from models import QIWIBill
from qiwi.models import Bill
from django.http import HttpResponseRedirect
import SOAPpy
import settings
from hashlib import md5
from qiwi.qiwi_status_code import QIWI_STATUS_PAID
from qiwi.backends import update_bill
from qiwi.models import QIWI_PERCENT

def bills(request):
	if request.method == 'GET':
		bills = QIWIBill.objects.filter(user=request.user.telephone_number)
		return render(request, 'test/bills.html', {'bills': bills})
	else:
		bills = QIWIBill.objects.filter(txn=request.POST['txn'])
		for bill in bills:
			bill.status = QIWI_STATUS_PAID
			bill.amount -= bill.amount / 100 * QIWI_PERCENT
			bill.save()
			state = update_bill(txn=int(bill.txn), status=60)
			if state == 0:
				bill.delete()

		return HttpResponseRedirect('/test/my_bills/')
