#code utf-8
from django.shortcuts import render
from lots.models import Lot
from lots.views import search as search_lots
from consts import *
from helpers import Mapper, to_json, jsonResponse
from lots.views import COUNT_LOTS_ON_PAGE
from test.models import QIWIBill
from qiwi.models import Bill
from datetime import datetime
import time
from django_qiwi.soap.client import Client
from django_ulogin.models import ULoginUser
from django_ulogin.settings import PROVIDERS

from settings import INVITE
def index(request):
	user = request.user
	mapper = Mapper()
	lots = []
	social_accounts = []
	if user.is_authenticated():
		social_accounts = [pr[0] for pr in ULoginUser.objects.filter(user=request.user).values_list('network')]
		try:
			lot = user.get_lot()
			if lot != None:
				user.lot = to_json(mapper.mapping(lot))
		except Lot.DoesNotExist:
			pass

		gender_converter = {'male': 'female', 'female': 'male', '': ''}

		lots = search_lots(COUNT_LOTS_ON_PAGE, city=user.city, gender=gender_converter[user.gender])
	else:
		lots = search_lots(COUNT_LOTS_ON_PAGE)

	ctx = {
		'user': user,
		'BAD_BROWSER': 'msie' in request.META.get('HTTP_USER_AGENT', 'unknown').lower(),
	   'cities': to_json(CITIES),
		'lots': to_json(mapper.mapping(lots)),
		'time_server': to_json(datetime.now()),
	   'user_is_win': to_json(bool(len(Lot.objects.filter(winner=request.user, active=True))) if request.user.is_authenticated() else False),
		'user_social_accounts': to_json(social_accounts),
		'SOCIAL_PROVIDERS': to_json(PROVIDERS),
		'INVITE': INVITE
	}

	return render(request, 'index.html', ctx)

def uloginxd(request):
	return render(request, 'ulogin_xd.html')