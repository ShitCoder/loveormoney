"""
Vkontakte OAuth support.
"""
import cgi
from urllib import urlencode
from urllib2 import urlopen

from django.utils import simplejson
from django.contrib.auth import authenticate

from social_auth.backends import BaseOAuth2, OAuthBackend, USERNAME
from social_auth.utils import sanitize_log_data, setting, log

VKONTAKTE_ME = 'https://api.vkontakte.ru/method/users.get?'

class VkontakteBackend(OAuthBackend):
	"""Vkontakte OAuth2 authentication backend"""
	name = 'vkontakte'
	# Default extra data to store
	EXTRA_DATA = [
		('id', 'id'),
	]

	def get_user_details(self, response):
		"""Return user details from vkontakte account"""
		return {USERNAME: response.get('username'),
		        'email': response.get('email', ''),
		        'fullname': response.get('name', ''),
		        'first_name': response.get('first_name', ''),
		        'last_name': response.get('last_name', '')}


class VkontakteAuth(BaseOAuth2):
	AUTH_BACKEND = VkontakteBackend
	RESPONSE_TYPE = 'code'
	SCOPE_SEPARATOR = ''
	AUTHORIZATION_URL = 'http://oauth.vkontakte.ru/authorize'
	SETTINGS_KEY_NAME = 'VKONTAKTE_APP_ID'
	SETTINGS_SECRET_NAME = 'VKONTAKTE_API_SECRET'

	EXTRA_DATA = [
		('id', 'id'),
	]

	def user_data(self, access_token, user_uid):
		"""Loads user data from service"""
		data = None
		url = VKONTAKTE_ME + urlencode({
			'access_token': access_token,
			'fields': ' uid,first_name,last_name,nickname,screen_name,sex,\
			 bdate (birthdate),city,country,timezone,photo,photo_medium,photo_big,\
			 has_mobile,rate,contacts,education,online,counters',
		   'uids': user_uid

		})

		try:
			data = simplejson.load(urlopen(url))['response'][0]
			data['id'] = data['uid']
		except ValueError:
			extra = {'access_token': sanitize_log_data(access_token)}
			log('error', 'Could not load user data from Facebook.',
			    exc_info=True, extra=extra)
		else:
			log('debug', 'Found user data for token %s',
			    sanitize_log_data(access_token),
			    extra=dict(data=data))
		return data

	def get_scope(self):
		return ['wall']

	def auth_complete(self, *args, **kwargs):
		"""Completes loging process, must return user instance"""
		if 'code' in self.data:
			url = 'https://oauth.vkontakte.ru/access_token?' +\
			      urlencode({'client_id': setting('VKONTAKTE_APP_ID'),
			                 'client_secret': setting('VKONTAKTE_API_SECRET'),
			                 'code': self.data['code']})

			response = simplejson.load(urlopen(url)) #urlopen(url).read()
			access_token = response['access_token']
			data = self.user_data(access_token, response['user_id'])
			if data is not None:
				if 'error' in data:
					error = self.data.get('error') or 'unknown error'
					raise ValueError('Authentication error: %s' % error)
				data['access_token'] = access_token
				# expires will not be part of response if offline access
				# premission was requested
				if 'expires' in response:
					data['expires'] = response['expires'][0]
			kwargs.update({'response': data, self.AUTH_BACKEND.name: True})
			return authenticate(*args, **kwargs)
		else:
			error = self.data.get('error') or 'unknown error'
			raise ValueError('Authentication error: %s' % error)

	@classmethod
	def enabled(cls):
		"""Return backend enabled status by checking basic settings"""
		return setting('VKONTAKTE_APP_ID') and setting('VKONTAKTE_API_SECRET')

	def auth_extra_arguments(self):
		return {'display': 'page'}

# Backend definition
BACKENDS = {
	'vkontakte': VkontakteAuth,
}
