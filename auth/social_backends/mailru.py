"""
Mail.ru OAuth support.
"""
import cgi
import httplib
from urllib import urlencode
from urllib2 import urlopen
import hashlib

from django.utils import simplejson
from django.contrib.auth import authenticate

from social_auth.backends import BaseOAuth2, OAuthBackend, USERNAME
from social_auth.utils import sanitize_log_data, setting, log

MAILRU_ME = 'http://www.appsmail.ru/platform/api?'

class MailRuBackend(OAuthBackend):
	"""Facebook OAuth2 authentication backend"""
	name = 'mailru'
	# Default extra data to store
	EXTRA_DATA = [
		('id', 'id'),
		('expires', setting('SOCIAL_AUTH_EXPIRATION', 'expires'))
	]

	def get_user_details(self, response):
		"""Return user details from Facebook account"""
		response = {} if not response else response
		return {
			USERNAME: response.get('username'),
			'email': response.get('email', ''),
			'fullname': response.get('name', ''),
			'first_name': response.get('first_name', ''),
			'last_name': response.get('last_name', '')
		}


class MailRuAuth(BaseOAuth2):
	AUTH_BACKEND = MailRuBackend
	RESPONSE_TYPE = 'code'
	SCOPE_SEPARATOR = ','
	AUTHORIZATION_URL = 'https://connect.mail.ru/oauth/authorize'
	SETTINGS_KEY_NAME = 'MAILRU_APP_ID'
	SETTINGS_SECRET_NAME = 'MAILRU_API_SECRET'

	EXTRA_DATA = [
		('id', 'id'),
	]

	def user_data(self, access_token, user_uid):
		"""Loads user data from service"""
		data = {'uid': user_uid, 'id': user_uid}
		params = {
			'app_id': setting('MAILRU_APP_ID'),
		   'session_key': access_token,
			'uids': user_uid,
		   'method': 'users.getInfo',
		   'secure': '1'
		}

		strparam = ''
		for key in params:
			strparam += key + "=" + params[key]

		h = hashlib.md5()
		h.update(strparam + setting('MAILRU_API_SECRET'))
		params.update({'sig': h.hexdigest()})
		url = MAILRU_ME +  urlencode(params)
		try:
			data = simplejson.load(urlopen(url))['response'][0]
			data['id'] = data['uid']
		except Exception:    # Exception replace ValueError
			extra = {'access_token': sanitize_log_data(access_token)}
			log('error', 'Could not load user data from Mail.ru.',
			    exc_info=True, extra=extra)
		else:
			log('debug', 'Found user data for token %s',
			    sanitize_log_data(access_token),
			    extra=dict(data=data))
		return data

	def auth_complete(self, *args, **kwargs):
		"""Completes loging process, must return user instance"""
		if 'code' in self.data:
			params = urlencode({
				'client_id': setting('MAILRU_APP_ID'),
            'client_secret': setting('MAILRU_API_SECRET'),
            'code': self.data['code'],
            'redirect_uri': self.redirect_uri,
            'grant_type': 'authorization_code'
			})

			conn = httplib.HTTPSConnection("connect.mail.ru")
			conn.request("POST", "/oauth/token", params)
			response = simplejson.loads(conn.getresponse().read())
			conn.close()

			access_token = response['access_token']
			refresh_token = response['refresh_token']
			data = self.user_data(access_token, response['x_mailru_vid'])
			if data is not None:
				if 'error' in data:
					error = self.data.get('error') or 'unknown error'
					raise ValueError('Authentication error: %s' % error)
				data['access_token'] = access_token
				data['refresh_token'] = refresh_token

				if 'expires_in' in response:
					data['expires'] = response['expires_in']
			kwargs.update({'response': data, self.AUTH_BACKEND.name: True})
			return authenticate(*args, **kwargs)
		else:
			error = self.data.get('error') or 'unknown error'
			raise ValueError('Authentication error: %s' % error)

	@classmethod
	def enabled(cls):
		"""Return backend enabled status by checking basic settings"""
		return setting('MAILRU_APP_ID') and setting('MAILRU_API_SECRET')

# Backend definition
BACKENDS = {
	'mailru': MailRuAuth,
}
