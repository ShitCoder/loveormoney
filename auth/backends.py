from django.contrib.auth.backends import ModelBackend
from models import LMUser

class DefaultUser(ModelBackend):
	def authenticate(self, username, password):
		try:
			user = LMUser.objects.get(username=username)
			if user.check_password(password):
				return user
		except LMUser.DoesNotExist:
			return None

	def get_user(self, user_id):
		try:
			return LMUser.objects.get(pk=user_id)
		except LMUser.DoesNotExist:
			return None