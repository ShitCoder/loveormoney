from django.forms import ModelForm
from auth.models import Profile

class ProfileForm(ModelForm):
	class Meta:
		model = Profile