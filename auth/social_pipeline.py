#encoding=utf-8
import urllib
import hashlib
from settings import TEMP_MEDIA_ROOT
import os
from django.core import serializers
from consts import CITIES
def ulogin_constroller(*args, **kwargs):
	if not kwargs['registered']:
		return
	user = kwargs['user']
	data = kwargs['ulogin_data']

	username = data.get('first_name', '')
	if len(username):
		username = data['first_name']
		
	if len(data.get('last_name', '')) and len(username):
		username += ' ' + data['last_name']
		
	if not len(username):
		username = data.get('nickname', '')
		
	if username:
		user.default_user_login = False
		user.first_name = username

	sex = data.get('sex', 0)
	if sex == 1:
		user.gender = "female"
	elif sex == 2:
		user.gender = "male"

	if CITIES.count(data.get('city', '')):
		user.city = data.get('city')
	else:
		from xmlparser import XML2Py
		ip = kwargs['request'].META.get('REMOTE_ADDR', False) or kwargs['request'].META.get('REMOTE_ADDR', False)
		if ip:
			try:
				city = XML2Py().parse(urllib.urlopen('http://ipgeobase.ru:7020/geo?ip=' + ip).read())['ip-answer'][0]
			except Exception:
				city = ''

			if CITIES.count(city):
				user.city = data.get('city')

	user.save()