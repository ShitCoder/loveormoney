from django.conf.urls.defaults import patterns, include, url
import settings
from django.conf.urls.static import static



urlpatterns = patterns('auth.views',
   url(r'check_username/$', 'check_username'),
   url(r'check_telephone_number/$', 'check_telephone_number'),
	url(r'^logout/$', 'logout'),
   url(r'^update/photo/$', 'update_photo'),
   url(r'^update/profile/$', 'update_user_data'),
	url(r'^get-user/$', 'get_user'),
	url(r'^add_social_account', 'add_social_account'),
	url(r'^', include('django_ulogin.urls'))
)
