from settings import DEBUG, TEMP_MEDIA_ROOT, MEDIA_ROOT
from django.contrib import auth
from auth.models import LMUser, DEFAULT_PHOTO
from settings import INVITE
from helpers import *
from django.views.decorators.http import require_GET
from django.core.files.base import ContentFile
from consts import CITIES
import re
from lots.models import Lot
from django_ulogin.views import ulogin_response
from django_ulogin.models import ULoginUser

def logout(request):
	auth.logout(request)
	return jsonResponse(request)

@user_authenticated
def add_social_account(request):
	response = ulogin_response(request.GET['token'], request.get_host())
	if 'error' in response:
		return jsonResponse(request, {'state': False})

	ULoginUser.objects.create(
		network  = response['network'],
		uid      = response['uid'],
		identity = response['identity'],
		user     = request.user
	)

	return jsonResponse(request, {'state': True, 'social_account': response['network']})

@require_GET
@user_authenticated
def check_telephone_number(request):
	try:
		user = LMUser.objects.get(telephone_number=request.GET['telephone_number'])
		if user == request.user:
			raise LMUser.DoesNotExist()

		return jsonResponse(request, {'is_exist': True})
	except LMUser.DoesNotExist:
		pass

	return jsonResponse(request, {'is_exist': False})

@require_GET
@user_authenticated
def check_username(request):
	try:
		user = LMUser.objects.get(first_name=request.GET['first_name'])
		if user == request.user:
			raise LMUser.DoesNotExist()

		return jsonResponse(request, {'is_exist': True})
	except LMUser.DoesNotExist:
		pass

	return jsonResponse(request, {'is_exist': False})

@user_authenticated
def update_user_data(request):
	if not INVITE and not request.user.user_confirmation:
		if not (request.POST.get('user_confirmation', False) or bool(request.POST.get('user_confirmation', False))):
			return jsonResponseError(request, 'not user confirmation')

		request.user.user_confirmation = True

	if 'about_me' in request.POST:
		about_me = request.POST.get('about_me')
		if len(about_me) > 200:
			return jsonResponseError(request, 'bad text for "about me".')

		about_me = about_me.replace('\n', '\\n')
		request.user.about_me = about_me

	if 'telephone_number' in request.POST:
		telephone_number = request.POST.get('telephone_number', '')

		if len(telephone_number) != 10 or len(LMUser.objects.filter(telephone_number=telephone_number)) > 0 or len(request.user.telephone_number) > 0:
			return jsonResponseError(request, 'bad telephone number')

		request.user.telephone_number = telephone_number
	if 'first_name' in request.POST:
		first_name = request.POST.get('first_name', '')

		if len(first_name) > 32 or len(first_name) == 0 or len(LMUser.objects.filter(first_name=first_name).exclude(pk=request.user.pk)) > 0:
			return jsonResponseError(request, 'bad username')

		request.user.first_name = first_name
		request.user.default_user_login = False
	if 'city' in request.POST:
		if CITIES.count(request.POST.get('city')) == 0:
			return jsonResponseError(request)

		request.user.city = request.POST.get('city')
	if 'gender' in request.POST:
		if not request.POST['gender'] in ('male', 'female'):
			return jsonResponseError(request)

		request.user.gender = request.POST['gender']

	request.user.save()
	return jsonResponse(request, {'status': True, 'profile_filled': request.user.profile_filled})

@user_authenticated
def update_photo(request):
	formats = ['.png', '.jpg', '.jpeg', '.bmp', '.ico', '.gif']
	if not 'file' in request.FILES:
		return jsonResponseError(request, 'not file.')

	file = request.FILES['file']
	ext = re.search(re.compile('\.[^.]+$'), file.name).group()
	if not ext in formats or file.size >= 1024*1024*20:
		return jsonResponse(request, {'status': False, 'error': 'bad format'})

	save_photo(file, request.user)

	return jsonResponse(request, {
		'status': True,
		'photo_uri': request.user.photo.url,
	   'photo_mini_uri': request.user.photo_mini.url,
	   'photo_middle_uri': request.user.photo_middle.url,
		'profile_filled': request.user.profile_filled
	})

from django.core.files.uploadedfile import TemporaryUploadedFile
def save_photo(file, user, div_img=True):
	rm = user.photo.name != DEFAULT_PHOTO
	filename = file.name

	for attr, size in {'photo': (500,400), 'photo_middle': (150, 100), 'photo_mini': (50, 50)}.iteritems():
		file.seek(0)

		filestring = resizeImage(file, size, div_img)
		file_content = ContentFile(filestring.getvalue())
		if rm:
			try:
				getattr(user, attr).delete()
			except Exception:
				pass
		getattr(user, attr).save(filename, file_content)


@user_authenticated
@require_GET
def get_user(request):
	user_id = request.GET.get('user_id', None)
	if user_id == None:
		return jsonResponseError(request)
	user = LMUser.objects.get(id=user_id)
	try:
		lot = Lot.objects.filter(user=user, active=True)
	except Exception:
		lot = None

	mapper = Mapper()

	return jsonResponse(request, {
		'user': mapper.mapping(user, ('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender', 'city', 'number_confirm')),
		'lot': mapper.mapping(lot, ('id', {'winner': ('first_name', 'photo', 'photo_middle', 'photo_mini', 'id', 'about_me', 'gender', 'city')}, 'date', 'my_confirm', 'winner_confirm')) if lot != None else {}
	})