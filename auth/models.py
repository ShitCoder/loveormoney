from django.contrib.auth.models import User , UserManager
from django.db import models

DEFAULT_PHOTO = 'photo/original/default.jpg'
DEFAULT_MINI_PHOTO = 'photo/mini/default.jpg'
DEFAULT_MID_PHOTO = 'photo/middle/default.jpg'

class ImageField(models.ImageField):
	def value_to_string(self, obj):
		value = self._get_val_from_obj(obj)
		return value.url

class LMUser(User):
	photo = ImageField(upload_to='photo/original', default=DEFAULT_PHOTO)
	photo_mini = ImageField(upload_to='photo/mini', default=DEFAULT_MINI_PHOTO)
	photo_middle = ImageField(upload_to='photo/middle', default=DEFAULT_MINI_PHOTO)
	default_user_login = models.BooleanField(default=True)
	profile_filled = models.BooleanField(default=False)
	about_me = models.CharField(max_length=256, default='')
	telephone_number = models.CharField(max_length=10, default='')
	city = models.CharField(max_length=256, default='')
	gender = models.CharField(max_length=10, default='')
	user_confirmation = models.BooleanField(default=False)
	save_state = models.CharField(max_length=1024, default='{}')

	_mapping_fields = ('id', 'first_name')

	objects = UserManager()
	backend = 'auth.backends.DefaultUser'

	def set_save_state(self, kwargs):
		from helpers import to_json
		self.save_state = to_json(kwargs)

	def save(self, force_insert=False, force_update=False, using=None):
		is_create = self.pk == None
		if force_insert:
			self.username = self.username.lower()
		if not self.profile_filled:
			self.profile_filled = not self.default_user_login and self.photo != DEFAULT_PHOTO and len(self.telephone_number) > 0 and len(self.city) > 0 and len(self.gender) > 0 and self.user_confirmation

		super(LMUser, self).save(force_insert, force_update, using)

		if is_create:
			from qiwi.models import Purse
			Purse.objects.create(user=self)

			from sms_service.models import NumberConfirmNote
			NumberConfirmNote.objects.create(user=self)

	def get_lot(self):
		from lots.models import Lot
		try:
			return Lot.objects.get(user=self, active=True)
		except Lot.DoesNotExist:
			return None

		return Lot.objects.get(user=self, my_confirm=False)

	@staticmethod
	def validate_data(data):
		try:
			LMUser.objects.get(first_name=data['first_name'])
			return False
		except LMUser.DoesNotExist:
			pass

		return True

	def number_is_confirmed(self, number=None):
		from datetime import datetime
		if type(number )!= str and not len(number) in [10, 11]:
			raise Exception('Incorrect data :: number must be...')
		if len(number) == 11:
			number = number[1:]
		self.telephone_number = number
		self.numberconfirmnote.number_confirm = number
		self.numberconfirmnote.number_accept_date = datetime.now()
		self.numberconfirmnote.save()
		self.save()