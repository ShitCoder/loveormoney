#coding=utf-8

nicks = [u'dublyor', u'nastradamus', u'innochka__mambo', u'dyavol_no_dobriy', u'nastya',
		 u'intelligent', u'dagestanec', u'natalia_oriero', u'yujh_cebep', u'_cako_m',
		 u'danger', u'nathasa', u'lackaba_ya', u'_i_b_o', u'darksteel', u'nargila',
		 u'le3gihchik', u'aydan', u'dasdafsdf', u'neftci_pfk', u'luha', u'martin', u'daywalker',
		 u'neitrino', u'lyapota', u'_zaur', u'dehwet', u'nelly', u'lackovaya_pantepa', u'_ciqan',
		 u'deli', u'nelly_furtado', u'lackovy_bakinec', u'_nemo', u'dead_girl', u'neqatif',
		 u'lactochka', u'_urek', u'deart_wolf', u'nergiz_132', u'lapylya', u'_ciqan', u'death',
		 u'nerpatoluq', u'lapka', u'_kaketka', u'death_angel', u'new_girl', u'ledi', u'_rio_',
		 u'dedmopo3', u'new_world', u'ledi_bvaly', u'_azerbaycan', u'delpiero', u'nfs_carbon', u'leyla',
		 u'_anarchik', u'delete1', u'nicat.59', u'lenka', u'_baksyor', u'delfin', u'nigar', u'lolita',
		 u'_killer', u'desant016', u'nightwolf', u'lyna', u'_nayomnik', u'desert_eagle', u'night_hunter',
		 u'lychschechemangel', u'__anar__', u'detka', u'niko_375', u'lixach', u'_shamaxihec', u'detka',
		 u'nil_ens', u'lika', u'__anar__', u'devdas', u'ninja', u'lvinoe_cepdce', u'_azeri_',
		 u'devushka_jagoza', u'nod32', u'lyucka', u'_bipuc_666', u'dikarochka', u'nonda',
		 u'lyubovcladkiycon', u'_cmept_on', u'director', u'nuhante', u'lyubimchik_ya', u'_dmx_b_',
		 u'diabolus666', u'nuriyev', u'lyubitel_kekcov', u'_fu_adell_', u'diams', u'nurlan_dragon',
		 u'lyudi_invalid', u'_muzik_', u'diana_84', u'nyuton_a', u'patpiot', u'_ramal', u'die_hard',
		 u'narko_biznes', u'pcix', u'_agdamec_', u'dina', u'narkaman_789', u'pepbi3cik', u'_canavar_',
		 u'djamila', u'nacnoy_snayper', u'pepedo3ipobka', u'_promete', u'dj_dance', u'naile', u'potya',
		 u'_sexyboy_', u'dj_emo', u'naina', u'pobeguhchik', u'_4clubbers_', u'dj_polina', u'nanit',
		 u'polka', u'_anonim_', u'dj_perviz', u'napaleon', u'poceluy_dpakoha', u'_bratelnik_',
		 u'dj_skypegirl', u'narin_yagish', u'podelhik', u'_d_r_a_k_o_n_', u'dodaqdan_qelbe',
		 u'narkaman_8km', u'ppaclablehhy', u'_eva_', u'dodger', u'narkaman_lubvi', u'ppodyucep',
		 u'_gladiator_', u'doktor_elcan', u'narmina', u'ppischik', u'_jvc_', u'dolmakimioglan', u'nastinka',
		 u'papenbezdevyshki', u'_katala_', u'donjuan89', u'nasty_girl', u'papen_v_chepnom']

cities = [u'Москва', u'Санкт-Петербург', u'Новосибирск', u'Екатеринбург', u'НижнийНовгород', u'Самара', u'Омск',
		  u'Казань', u'Челябинск', u'Ростов-на-Дону', u'Уфа', u'Волгоград', u'Пермь', u'Красноярск', u'Воронеж',
		  u'Саратов', u'Краснодар', u'Тольятти', u'Ижевск', u'Ульяновск', u'Барнаул', u'Владивосток', u'Ярославль',
		  u'Иркутск', u'Тюмень', u'Махачкала', u'Хабаровск', u'Новокузнецк', u'Оренбург', u'Кемерово', u'Рязань',
		  u'Томск', u'Астрахань', u'Пенза', u'НабережныеЧелны', u'Липецк', u'Тула', u'Киров', u'Чебоксары',
		  u'Калининград', u'Брянск', u'Курск', u'Иваново', u'Магнитогорск', u'Улан-Удэ', u'Тверь', u'Ставрополь',
		  u'НижнийТагил', u'Белгород', u'Архангельск', u'Владимир', u'Сочи', u'Курган', u'Смоленск', u'Калуга',
		  u'Чита', u'Орёл', u'Волжский', u'Череповец', u'Владикавказ', u'Мурманск', u'Сургут', u'Вологда', u'Саранск',
		  u'Тамбов', u'Стерлитамак', u'Грозный', u'Якутск', u'Кострома', u'Комсомольск-на-Амуре', u'Петрозаводск',
		  u'Таганрог', u'Нижневартовск', u'Йошкар-Ола', u'Братск', u'Новороссийск', u'Дзержинск', u'Шахты',
		  u'Нальчик', u'Орск', u'Сыктывкар', u'Нижнекамск', u'Ангарск', u'СтарыйОскол', u'ВеликийНовгород',
		  u'Балашиха', u'Благовещенск', u'Прокопьевск', u'Бийск', u'Химки', u'Псков', u'Энгельс', u'Рыбинск',
		  u'Балаково', u'Северодвинск', u'Армавир', u'Подольск', u'Королёв', u'Южно-Сахалинск',
		  u'Петропавловск-Камчатский', u'Сызрань', u'Норильск', u'Златоуст', u'Каменск-Уральский', u'Мытищи',
		  u'Люберцы', u'Волгодонск', u'Новочеркасск', u'Абакан', u'Находка', u'Уссурийск', u'Березники', u'Салават',
		  u'Электросталь', u'Миасс', u'Рубцовск', u'Альметьевск', u'Ковров', u'Коломна', u'Майкоп', u'Пятигорск',
		  u'Одинцово', u'Копейск', u'Хасавюрт', u'Железнодорожный', u'Новомосковск', u'Кисловодск', u'Серпухов',
		  u'Первоуральск', u'Новочебоксарск', u'Нефтеюганск', u'Димитровград', u'Нефтекамск', u'Черкесск',
		  u'Орехово-Зуево', u'Дербент', u'Камышин', u'Невинномысск', u'Красногорск', u'Муром', u'Батайск',
		  u'Новошахтинск', u'СергиевПосад', u'Ноябрьск', u'Щёлково', u'Кызыл', u'Октябрьский', u'Ачинск', u'Северск',
		  u'Новокуйбышевск', u'Елец', u'Арзамас', u'Обнинск', u'НовыйУренгой', u'Каспийск', u'Элиста', u'Пушкино',
		  u'Жуковский', u'Артём', u'Междуреченск', u'Ленинск-Кузнецкий', u'Сарапул', u'Ессентуки', u'Воткинск']


from auth.models import LMUser
from lots.models import Lot
from hashlib import *
from datetime import *
import time
from helpers import *
from PIL import Image
import os
from django.core.files.base import ContentFile
from random import random, randint


def create_users(count=1):
	name = ''
	k = 0
	username_is_already_token = False
	for i in range(0, count):
		try:
			if i < len(nicks):
				LMUser.objects.get(first_name=nicks[i])
				username_is_already_token = True
			else:
				username_is_already_token = False
		except LMUser.DoesNotExist:
			username_is_already_token = False

		if i < len(nicks) and not username_is_already_token:
			name = nicks[i]
		else:
			name = 'user_'+str(time.time())

		user = LMUser.objects.create(
			first_name=name[:32],
			default_user_login=False,
			city=cities[randint(0, len(cities) - 1)],
			gender= 'male' if random() < 0.4 else 'female',
			profile_filled=True
		)
		#img_name = 'default ('+str((i + 1) % 89 if i != 88 else 89)+').jpg'
		#print img_name
		#img = Image.open('C:/LoM/media/photo/'+img_name)
		#user.photo = img
		#user.save()
		#img.save('C:/LoM/media/default/'+img_name)
		#user.photo = '/photo/'+img_name
		#user.save()
		#resizeImage(user.photo.path)


#type: 0 - all, 1 - random
def create_lots(type=0):
	uc = LMUser.objects.count()
	for i in range(1, uc+1):
		if type == 1 and random() < 0.4:
			continue
		Lot.objects.create(
			user=LMUser.objects.get(pk=i),
			date=datetime.now() + timedelta(days=int(random() * 30)+7)
		)


def tests(request, value='cu=1'):
	try:
		q = value.split('=')
		val = q[1]
		q = q[0]
		if q == 'cu':
			try:
				return create_users(int(val))
			except Exception:
				return Exception.message
		elif q == 'cl':
			if not val in ['0', '1']:
				val = 0
			return create_lots(val)
	except Exception:
		return jsonResponse(request, {'msg': u'fucking data'})

	return jsonResponse(request, {'status': '(test not found) no available link'})

