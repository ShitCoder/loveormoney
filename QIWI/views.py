#encoding=utf-8
from models import QIWI, Purse, Bill
from helpers import jsonResponse, user_authenticated, jsonResponseError
from django.views.decorators.http import require_GET
from qiwi.qiwi_status_code import *
from socket import error as SocketError
from helpers import profile_filled
from django_qiwi.soap.utils import STATUS_CODE_TEXT

SERVER_TEXT_ERROR = u'Сервис временно недоступен.'
WITHDRAW_PERCENT = 10
QIWI_PERCENT = 1

def compute_percent(sum, percent):
	return float(sum) / 100 * percent

@user_authenticated
@require_GET
@profile_filled
def bring(request):
	if not 'sum' in request.GET or not int(request.GET['sum']):
		jsonResponseError('you are hacker')

	data = {'state': True}

	sum = int(request.GET['sum'])
	try:
		itr = 0
		bill = None
		while (itr == 0 or bill.state == QIWI_STATUS_BILL_EXIST) and itr < 3:
			bill = Bill.objects.create(user=request.user, sum=sum)
			itr += 1

		if bill.state !=  QIWI_STATUS_EXPOSED:
			raise Exception()

	except Exception, e:
		data['state'] = False
		data['error'] = SERVER_TEXT_ERROR

	return jsonResponse(request, data)

@user_authenticated
@require_GET
def get_captha(request):
	qiwi = QIWI()
	data = {'state': True}

	try:
		is_auth = qiwi.is_auth()
		data['is_auth'] = is_auth

		if not is_auth:
			if not qiwi.is_upload_captcha():
				try:
					qiwi.upload_captcha()
				except Exception, e:
					data['state'] = False
					data['error'] = SERVER_TEXT_ERROR

			data['capthca_url'] = qiwi.get_captcha_url()

	except Exception, e:
		data['state'] = False
		data['error'] = SERVER_TEXT_ERROR

	return jsonResponse(request, data)

@user_authenticated
@require_GET
@profile_filled
def withdraw(request):
	is_captcha = 'captcha' in request.GET

	try:
		if not 'sum' in request.GET:
			raise Exception
		sum = float(request.GET['sum'])
	except:
		return jsonResponseError(request, 'you are hacker')

	data = {'state': True, 'is_auth': True}

	if sum > request.user.purse.available_money():
		data['state'] = False
		data['error'] = u'У вас недостаточно средств.'
	else:
		qiwi = QIWI()

		if is_captcha:
			try:
				if not qiwi.send_answer_captcha(request.GET['captcha']):
					data['state'] = False
					data['error'] = u'Введенный код неверен'
					qiwi.upload_captcha()
					data['new_captcha_url'] = qiwi.get_captcha_url()
				else:
					is_captcha = False
			except:
				data['state'] = False
				data['error'] = SERVER_TEXT_ERROR

		if not is_captcha:
			tarnslated_sum = sum - compute_percent(sum, WITHDRAW_PERCENT)
			if qiwi.transaction(request.user.telephone_number, tarnslated_sum):
				data['translated_money'] = tarnslated_sum
				user = request.user
				user.purse.sum -= int(sum)
				user.purse.save()
			else:
				data['state'] = False
				data['error'] = SERVER_TEXT_ERROR

		data['is_auth'] = not is_captcha

	return jsonResponse(request, data)

