#coding:utf-8
from django_qiwi.soap import server
from settings import QIWI_PASSWORD, QIWI_LOGIN
from threading import Thread
import SOAPpy
import qiwi.backends as qiwi
from datetime import datetime
from test.models import QIWIBill
import time

class QiwiProvider:
	def __init__(self):
		self.server = self._createSOAPServer()

	def _createSOAPServer(self):
		server = SOAPpy.SOAPServer(('127.0.0.1', 8007))

		def createBill(login, password, user, amount, comment, txn, lifetime, alarm, create):
			if (login != QIWI_LOGIN or password != QIWI_PASSWORD):
				return 150

			if len(user) != 10:
				return 150
			try:
				amount = float(amount)
			except Exception:
				return 150
			if amount > 15000:
				return 242
			if alarm not in [0, 1, 2]:
				return 150
			if create not in [0, 1]:
				return 150


			QIWIBill.objects.create(
				txn = txn,
				user = user,
				amount = amount,
				comment = comment
			)
			return 50

		def checkBill(login, password, txn):
			txn = str(txn)
			if not QIWIBill.objects.filter(txn=txn).count():
				return 210

			res = QIWIBill.objects.get(txn=txn).__dict__
			res['date'] = res['lifetime'] = '11.11.2012 23:23:23'

			return res

		server.registerFunction(createBill)
		server.registerFunction(checkBill)
		return server

	def run(self):
		self.server.serve_forever()

	def stop(self):
		self.server.shutdown()
		del self.server